package com.thegirafe.myfoot.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class RobotoBoldTextView extends AppCompatTextView {
    Context context;

    public RobotoBoldTextView(Context context) {
        super(context);
        this.context = context;
    }

    public RobotoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"Roboto-Bold.ttf");
        setTypeface(typeface);
    }
}
