package com.thegirafe.myfoot.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class RobotoItalicTextView extends AppCompatTextView {
    Context context;

    public RobotoItalicTextView(Context context) {
        super(context);
        this.context = context;
    }

    public RobotoItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"Roboto-Italic.ttf");
        setTypeface(typeface);
    }
}
