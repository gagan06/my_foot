package com.thegirafe.myfoot.CustomFonts;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class RobotoRegularEditText extends AppCompatEditText {
    Context context;

    public RobotoRegularEditText(Context context) {
        super(context);
        this.context = context;
    }

    public RobotoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"Roboto-Regular.ttf");
        setTypeface(typeface);
    }
}
