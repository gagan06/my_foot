package com.thegirafe.myfoot.Attributes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SizeResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private SizeMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SizeMessage getMessage() {
        return message;
    }

    public void setMessage(SizeMessage message) {
        this.message = message;
    }
}
