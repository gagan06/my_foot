package com.thegirafe.myfoot.Attributes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SimpleMessage {
    @SerializedName("data")
    @Expose
    private SimpleData data;

    public SimpleData getData() {
        return data;
    }

    public void setData(SimpleData data) {
        this.data = data;
    }

}
