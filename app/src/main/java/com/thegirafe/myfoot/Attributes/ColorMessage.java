package com.thegirafe.myfoot.Attributes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ColorMessage {

    @SerializedName("data")
    @Expose
    private List<ColorData> data = null;

    public List<ColorData> getData() {
        return data;
    }

    public void setData(List<ColorData> data) {
        this.data = data;
    }

}
