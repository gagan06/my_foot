package com.thegirafe.myfoot.Attributes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SimpleColorResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private SimpleMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public SimpleMessage getMessage() {
        return message;
    }

    public void setMessage(SimpleMessage message) {
        this.message = message;
    }
}
