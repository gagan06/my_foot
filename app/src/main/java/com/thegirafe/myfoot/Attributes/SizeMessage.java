package com.thegirafe.myfoot.Attributes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SizeMessage {

    @SerializedName("data")
    @Expose
    private List<SizeData> data = null;

    public List<SizeData> getData() {
        return data;
    }

    public void setData(List<SizeData> data) {
        this.data = data;
    }

}
