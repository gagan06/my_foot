package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thegirafe.myfoot.Adapter.SelectBillingAddressAdapter;
import com.thegirafe.myfoot.Adapter.SelectDeliveryAddressAdapter;
import com.thegirafe.myfoot.Address.DeleteAddressResponse;
import com.thegirafe.myfoot.Address.ShowShippingAddressData;
import com.thegirafe.myfoot.Address.ShowShippingAddressResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.AddressInterface;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.BillingInterface;
import com.thegirafe.myfoot.Models.SelectDeliveryAddressModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.AddressModel;
import com.thegirafe.myfoot.PostModel.CityModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.SATECITY.CityData;
import com.thegirafe.myfoot.SATECITY.CityResponse;
import com.thegirafe.myfoot.coupons.CheckCouponResponse;
import com.thegirafe.myfoot.coupons.CheckPincodePost;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SelectDeliveryAddress extends AppCompatActivity implements View.OnClickListener, AddressInterface, BillingInterface {
    RecyclerView addressRecycler, billingRecycler;
    Button selectBtn, addNewAddressBtn, addNewBillingBtn;
    ArrayList<SelectDeliveryAddressModel> savedAddaressData = new ArrayList<>();
    SelectDeliveryAddressAdapter adapter = null;
    SelectBillingAddressAdapter adapter1 = null;
    TextView toolbar_title;
    ImageButton backPress, cartBtn;
    SharedPreferences sharedPreferences;
    String address_id;
    String billing_id, mobile;
    View progress_dialog;
    String amount;
    Intent intent;
    String item;
    ImageView delivery_error, billing_error;
    TextView error_billing, error_delivery, cartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_delivery_address);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.backPress);

        intent = getIntent();
        item = intent.getStringExtra("Item");
        amount = intent.getStringExtra("Amount");
        Log.e(MyConstants.TAG, "amount"+amount);

        toolbar_title.setText("Delivery Address");
        delivery_error = findViewById(R.id.delivery_error);
        billing_error = findViewById(R.id.billing_error);
        progress_dialog = findViewById(R.id.progress_dialog);
        addressRecycler = findViewById(R.id.addressRecycler);
        error_delivery = findViewById(R.id.error_delivery);
        error_billing = findViewById(R.id.error_billing);
        billingRecycler = findViewById(R.id.billingRecycler);
        addressRecycler.setFocusable(false);
        billingRecycler.setFocusable(false);
        addNewAddressBtn = findViewById(R.id.addNewAddressBtn);
        addNewBillingBtn = findViewById(R.id.addNewBillingBtn);
        selectBtn = findViewById(R.id.selectBtn);
        cartCount = findViewById(R.id.cartCount);

        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
        }

        getaddress();
        cartBtn.setVisibility(View.GONE);
        getBillingaddress();


        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        addNewAddressBtn.setOnClickListener(this);
        addNewBillingBtn.setOnClickListener(this);
        selectBtn.setOnClickListener(this);
    }



    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    public void getSavedAddress() {
//        for (int i = 0; i < 3; i++) {
//            SelectDeliveryAddressModel model = new SelectDeliveryAddressModel();
//            model.setName("Gagandeep");
//            savedAddaressData.add(model);
//        }
//    }

    public void getaddress() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<ShowShippingAddressResponse> call = api.get_address(getAuthToken());
        call.enqueue(new Callback<ShowShippingAddressResponse>() {
            @Override
            public void onResponse(Call<ShowShippingAddressResponse> call, Response<ShowShippingAddressResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        final ArrayList<ShowShippingAddressData> cityData = new ArrayList<>(response.body().getMessage().getData());

                        adapter = new SelectDeliveryAddressAdapter(cityData, SelectDeliveryAddress.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(SelectDeliveryAddress.this);
                        addressRecycler.setLayoutManager(layoutManager);
                        addressRecycler.setAdapter(adapter);
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ShowShippingAddressResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getBillingaddress() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<ShowShippingAddressResponse> call = api.get_billingaddress(getAuthToken());
        call.enqueue(new Callback<ShowShippingAddressResponse>() {
            @Override
            public void onResponse(Call<ShowShippingAddressResponse> call, Response<ShowShippingAddressResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        final ArrayList<ShowShippingAddressData> cityData = new ArrayList<>(response.body().getMessage().getData());
                        adapter1 = new SelectBillingAddressAdapter(cityData, SelectDeliveryAddress.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(SelectDeliveryAddress.this);
                        billingRecycler.setLayoutManager(layoutManager);
                        billingRecycler.setAdapter(adapter1);
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ShowShippingAddressResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(SelectDeliveryAddress.this, CartProduct.class);
                startActivity(i);
                break;
            case R.id.addNewBillingBtn:
                Log.e(MyConstants.TAG, "BILL");
                i = new Intent(SelectDeliveryAddress.this, AddNewAddress.class);
                i.putExtra("Address_id", address_id);
                i.putExtra("Billing_id", billing_id);
                i.putExtra("Item", item);
                i.putExtra("Amount", amount);
                i.putExtra("address", "billing");
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                startActivity(i);
                finish();
                break;
            case R.id.addNewAddressBtn:
                i = new Intent(SelectDeliveryAddress.this, AddNewAddress.class);
                i.putExtra("Address_id", address_id);
                i.putExtra("Billing_id", billing_id);
                i.putExtra("Item", item);
                i.putExtra("Amount", amount);
                i.putExtra("address", "shipping");
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                startActivity(i);
                finish();
                break;
            case R.id.selectBtn:
                if (isValid()){
                    i = new Intent(SelectDeliveryAddress.this, PaymentScreen.class);
                    i.putExtra("Address_id", address_id);
                    i.putExtra("Billing_id", billing_id);
                    i.putExtra("Phone", mobile);
                    i.putExtra("Item", item);
                    i.putExtra("Amount", amount);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBillingaddress();
        getaddress();
    }

    public boolean isValid(){
        boolean valid = true;

        if (address_id == null){
            delivery_error.setVisibility(View.VISIBLE);
            error_delivery.setText("Select delivery Address");
            error_delivery.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            delivery_error.setVisibility(View.GONE);
            error_delivery.setVisibility(View.GONE);
        }

        if (billing_id == null){
            billing_error.setVisibility(View.VISIBLE);
            error_billing.setText("Select delivery Address");
            error_billing.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            billing_error.setVisibility(View.GONE);
            error_billing.setVisibility(View.GONE);
        }
        return valid;
    }


    @Override
    public void deleteAddress(String s) {
        address_id = s;
        delivery_error.setVisibility(View.GONE);
        error_delivery.setVisibility(View.GONE);
        billing_error.setVisibility(View.GONE);
        error_billing.setVisibility(View.GONE);
        deletecurrentAddress(address_id);
    }

    @Override
    public void selectAddress(String s,  String zip) {
        address_id =  s;
        checkpincodeADDRESS(zip);
    }

    public void deletecurrentAddress(String address){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        AddressModel addressModel = new AddressModel(address);
        Call<DeleteAddressResponse> call = api.delete_address(getAuthToken(), addressModel);
        call.enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
//                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        getResultSuccess("Address Deleted successfully");
                        getaddress();
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
//                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void deletecurrentBillingAddress(String address){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        AddressModel addressModel = new AddressModel(address);
        Call<DeleteAddressResponse> call = api.delete_billingaddress(getAuthToken(), addressModel);
        call.enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
//                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        getResultSuccess("Address Deleted successfully");
                        getBillingaddress();
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
//                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getResultSuccess(String dec) {
        TextView dialog_dec, dialogTitle, Btn;
        final Dialog successDialog = new Dialog(SelectDeliveryAddress.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialog_dec.setText(dec);
        Btn.setText("Done");
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

    @Override
    public void deleteBillingAddress(String s) {
        billing_id = s;
        deletecurrentBillingAddress(billing_id);

    }

    @Override
    public void selectBillingAddress(String s, String zip, String phone) {
        billing_id =  s;
        mobile = phone;
        checkpincode(zip);
    }

    public void checkpincode(String address){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        CheckPincodePost addressModel = new CheckPincodePost(address);
        Call<CheckCouponResponse> call = api.check_pincode(getAuthToken(), addressModel);
        call.enqueue(new Callback<CheckCouponResponse>() {
            @Override
            public void onResponse(Call<CheckCouponResponse> call, Response<CheckCouponResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        error_billing.setVisibility(View.GONE);
                        billing_error.setVisibility(View.GONE);
                        selectBtn.setVisibility(View.VISIBLE);

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        error_billing.setText("Sorry, unable to deliver at this address");
                        error_billing.setVisibility(View.VISIBLE);
                        billing_error.setVisibility(View.VISIBLE);
                        selectBtn.setVisibility(View.GONE);
                        billing_id = "";
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CheckCouponResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }


    public void checkpincodeADDRESS(String address){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        CheckPincodePost addressModel = new CheckPincodePost(address);
        Call<CheckCouponResponse> call = api.check_pincode(getAuthToken(), addressModel);
        call.enqueue(new Callback<CheckCouponResponse>() {
            @Override
            public void onResponse(Call<CheckCouponResponse> call, Response<CheckCouponResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        error_delivery.setVisibility(View.GONE);
                        delivery_error.setVisibility(View.GONE);
                        selectBtn.setVisibility(View.VISIBLE);
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        error_delivery.setText("Sorry, unable to deliver at this address");
                        error_delivery.setVisibility(View.VISIBLE);
                        delivery_error.setVisibility(View.VISIBLE);
                        selectBtn.setVisibility(View.GONE);
                        address_id = "";
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CheckCouponResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

}
