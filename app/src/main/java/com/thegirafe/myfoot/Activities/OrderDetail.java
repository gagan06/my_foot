package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.instamojo.android.models.Order;
import com.thegirafe.myfoot.Adapter.OrderProductRecyclerAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Order.CancelOrderResponse;
import com.thegirafe.myfoot.Order.OrderDetailData;
import com.thegirafe.myfoot.Order.OrderDetailProduct;
import com.thegirafe.myfoot.Order.OrderDetailResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.CancelOrderPost;
import com.thegirafe.myfoot.PostModel.OrderStatusModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.UserProfileResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderDetail extends AppCompatActivity implements View.OnClickListener {

    TextView order_date, order_number, order_total, method_name, billing_address, shipping_address, total_item
            , shipping_charges, tax_amoutn, discount, order_total_amount, toolbar_title, cartCount, order_status;
    Intent intent;
    SharedPreferences sharedPreferences;
    View progress_dialog, no_internet_layout, server_error;
    ScrollView mainView;
    RecyclerView product_recycler;
    OrderProductRecyclerAdapter orderProductRecyclerAdapter;
    ImageButton backPress, cartBtn;
    Button retryBtn;
    String order_id;
    LinearLayout cancel;
    Button cancel_order;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        intent = getIntent();
        order_id = intent.getStringExtra("Order");
        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        cancel = findViewById(R.id.cancel);
        order_status = findViewById(R.id.order_status);
        retryBtn = findViewById(R.id.retryBtn);
        toolbar_title = findViewById(R.id.toolbar_title);
        cartCount = findViewById(R.id.cartCount);
        backPress = findViewById(R.id.backPress);
        product_recycler = findViewById(R.id.product_recycler);
        mainView = findViewById(R.id.mainView);
        progress_dialog = findViewById(R.id.progress_dialog);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        server_error = findViewById(R.id.server_error);
        order_date = findViewById(R.id.order_date);
        order_total_amount = findViewById(R.id.order_total_amount);
        discount = findViewById(R.id.discount);
        tax_amoutn = findViewById(R.id.tax_amoutn);
        shipping_charges = findViewById(R.id.shipping_charges);
        total_item = findViewById(R.id.total_item);
        shipping_address = findViewById(R.id.shipping_address);
        billing_address = findViewById(R.id.billing_address);
        method_name = findViewById(R.id.method_name);
        order_total = findViewById(R.id.order_total);
        order_number = findViewById(R.id.order_number);
        order_date = findViewById(R.id.order_date);
        toolbar_title.setText("Order Detail");
        cancel_order = findViewById(R.id.cancel_order);
        cartBtn = findViewById(R.id.cartBtn);
        order_detail(order_id);
        getCartCount();
        cartBtn.setOnClickListener(this);
        backPress.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
        cancel_order.setOnClickListener(this);
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void order_detail(final String order_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        OrderStatusModel orderStatusModel  = new OrderStatusModel(order_id);
        Call<OrderDetailResponse> call = api.order_detail(getAuthToken(), orderStatusModel);
        call.enqueue(new Callback<OrderDetailResponse>() {
            @Override
            public void onResponse(Call<OrderDetailResponse> call, Response<OrderDetailResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        no_internet_layout.setVisibility(View.GONE);
                        mainView.setVisibility(View.VISIBLE);
                        OrderDetailData orderDetailData = response.body().getMessage().getData();
                        ArrayList<OrderDetailProduct> orderDetailProducts = new ArrayList<>(orderDetailData.getProduct());
                        orderProductRecyclerAdapter = new OrderProductRecyclerAdapter(orderDetailProducts, OrderDetail.this);
                        LinearLayoutManager layoutManager=new LinearLayoutManager(OrderDetail.this);
                        product_recycler.setLayoutManager(layoutManager);
                        product_recycler.setAdapter(orderProductRecyclerAdapter);
                        DividerItemDecoration itemDecorator = new DividerItemDecoration(OrderDetail.this, DividerItemDecoration.VERTICAL);
                        itemDecorator.setDrawable(ContextCompat.getDrawable(OrderDetail.this, R.drawable.divider));
                        product_recycler.addItemDecoration(itemDecorator);
                        order_date.setText(orderDetailData.getOrderDate());
                        order_number.setText(orderDetailData.getOrderId());
                        order_total.setText("₹ "+orderDetailData.getTotalPrice());
                        order_total_amount.setText("₹ "+orderDetailData.getTotalPrice());
                        method_name.setText(orderDetailData.getName());
                        billing_address.setText(orderDetailData.getBillingAddress().getName()+"\n"+orderDetailData.getBillingAddress().getAddress()+ ", "+orderDetailData.getBillingAddress().getCityName()+", "+orderDetailData.getBillingAddress().getZip());
                        shipping_address.setText(orderDetailData.getShippingAddress().getName()+"\n"+orderDetailData.getShippingAddress().getAddress()+ ", "+orderDetailData.getShippingAddress().getCityName()+", "+orderDetailData.getShippingAddress().getZip());
                        int total=0;
                        for (int i=0; i< orderDetailProducts.size(); i++){
                            total = total+Integer.parseInt(orderDetailProducts.get(i).getQuantity());
                        }
                        total_item.setText(""+total);
                        shipping_charges.setText("₹ "+orderDetailData.getShippingCharges());
                        tax_amoutn.setText("₹ "+orderDetailData.getTax());
                        order_status.setText(orderDetailData.getOrderStatus());
                        discount.setText("₹ "+orderDetailData.getDiscount());
                        if (orderDetailData.getOrderStatus().equalsIgnoreCase("Processing")){
                            cancel.setVisibility(View.VISIBLE);
                        }
                    }else
                    {

                    }
                }else{
                    //server error
                    mainView.setVisibility(View.GONE);
                    server_error.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<OrderDetailResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                mainView.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.retryBtn:
                order_detail(order_id);
                getCartCount();
                no_internet_layout.setVisibility(View.GONE);
                progress_dialog.setVisibility(View.VISIBLE);
                break;
            case R.id.cartBtn:
                Intent intent1 = new Intent(OrderDetail.this, CartProduct.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case R.id.cancel_order:
                progress_dialog.setVisibility(View.VISIBLE);
                getcancel(order_id);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getcancel(final String order_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        CancelOrderPost cancelOrderPost = new CancelOrderPost(order_id);
        Call<CancelOrderResponse> call = api.cancel_order(getAuthToken(), cancelOrderPost);
        call.enqueue(new Callback<CancelOrderResponse>() {
            @Override
            public void onResponse(Call<CancelOrderResponse> call, Response<CancelOrderResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        getResultout("Order cancelled", "Success");
                        order_detail(order_id);
                    }else {
                        getResulterror("Order cannot be cancelled", "Failed");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CancelOrderResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);

            }
        });
    }

    public void getResultout(String dec, String title) {
        TextView dialog_dec, dialogTitle;
        Button Btn;
        final Dialog successDialog = new Dialog(OrderDetail.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialog_dec.setText(dec);
        dialogTitle.setText(title);
        Btn.setText("Done");
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

    public void getResulterror(String dec, String title) {
        TextView dialog_dec, dialogTitle;
        ImageButton Btn;
        final Dialog successDialog = new Dialog(OrderDetail.this);
        successDialog.setContentView(R.layout.error_popup);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.errorDec);
        dialogTitle = successDialog.findViewById(R.id.errorTitle);
        Btn = successDialog.findViewById(R.id.cancelBtn);
        dialog_dec.setText(dec);
        dialogTitle.setText(title);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }
}
