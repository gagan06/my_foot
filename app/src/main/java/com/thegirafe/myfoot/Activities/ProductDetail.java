package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.mtp.MtpConstants;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Adapter.ProductColorRecyclerAdapter;
import com.thegirafe.myfoot.Adapter.ProductDummyImagesAdapter;
import com.thegirafe.myfoot.Adapter.ProductImagesAdapter;
import com.thegirafe.myfoot.Adapter.ProductReviewsAdapter;
import com.thegirafe.myfoot.Adapter.ProductSizeRecyclerAdapter;
import com.thegirafe.myfoot.Attributes.ColorData;
import com.thegirafe.myfoot.Attributes.ColorResponse;
import com.thegirafe.myfoot.Attributes.SimpleColorResponse;
import com.thegirafe.myfoot.Attributes.SizeData;
import com.thegirafe.myfoot.Attributes.SizeResponse;
import com.thegirafe.myfoot.Cart.AddCartResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.AddGuetsCart;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Guest.GuestUserProductDetail;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.ColorInterface;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Interfaces.SizeColorInterface;
import com.thegirafe.myfoot.Interfaces.onItemClickListener;
import com.thegirafe.myfoot.Models.AddCartModel;
import com.thegirafe.myfoot.Models.ProductReviewsModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.GuestCartModel;
import com.thegirafe.myfoot.PostModel.ProductColorChooseModel;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.Products.Color;
import com.thegirafe.myfoot.Products.LikeProductResponse;
import com.thegirafe.myfoot.Products.Product;
import com.thegirafe.myfoot.Products.ProductColor;
import com.thegirafe.myfoot.Products.ProductColorChooseData;
import com.thegirafe.myfoot.Products.ProductColorChooseResponse;
import com.thegirafe.myfoot.Products.ProductDetailResponse;
import com.thegirafe.myfoot.Products.Size;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Reviews.ReviewData;
import com.thegirafe.myfoot.Reviews.ReviewResponse;
import com.thegirafe.myfoot.Wishlist.CheckWishlistResponse;
import com.thegirafe.myfoot.Wishlist.WishlistResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductDetail extends AppCompatActivity implements View.OnClickListener, SizeColorInterface, ColorInterface, onItemClickListener {
    TextView toolbar_title, product_name, product_price, text_color, sold_by, description
            ,delivery_detail,  colors_count, likes, original_price;
    Intent intent;
    RecyclerView product_images, product_dummy_images, size_recycler, color_recycler, reviews;
    SharedPreferences sharedPreferences;
    String product_id;
    ProductImagesAdapter productImagesAdapter;
    ProductDummyImagesAdapter productDummyImagesAdapter;
    ProductSizeRecyclerAdapter productSizeRecyclerAdapter;
    ProductColorRecyclerAdapter productColorRecyclerAdapter;
    ProductReviewsAdapter productReviewRecycler;
    ImageView color_choose, wishlist_icon;
    int count = 0;
    String productid;
    String attribute_value;
    LinearLayout color_view, saveBtn, addToCartBtn;
    ArrayList<ProductColorChooseData> colorChooseData;
    ElegantNumberButton number_button;
    TextView add_cart_text;
    TextView  colorCount, priseOff, productActualPriseTV, discountPriseTV, deliveryDec, offerTV, productTitle, likeCount;
    ImageButton backPress, cartBtn, share, likeBtn;
    RecyclerView sizeRecycler, colorRecycler, reviewRecycler;
    LinearLayout  like_view;
    CircleIndicator indicator;
    Button infoBtn;
    ViewPager productSliderPager;
    ArrayList<ReviewData> reviewData;
    ProductReviewsAdapter adapter;
    String color_id, size_id;
    ImageView heart;
    Button give_review;
    ScrollView main;
    LinearLayout belLay;
    View progress_dialog;
    ImageView product_featured_image;
    TextView no_review;
    RecyclerView sizeRecyclerdialog, colorRecyclerdialog;   Button submitBtn;
    Dialog successDialog;
    TextView select_color;
    TextView cartCount;
    String product_image;
    TextView instock;
    String name_product;
    String qauntity;
    RelativeLayout sizeView, cart, colorful_view;
    RelativeLayout qaunt_layout;
    View server_error, no_internet_layout;
    ProgressBar progressbAR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_dteail);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        intent = getIntent();
        product_id = intent.getStringExtra(MyConstants.Product_id);
        Log.e(MyConstants.TAG, "Product idssss "+product_id);
        qaunt_layout = findViewById(R.id.qaunt_layout);
        heart = findViewById(R.id.heart);
        main = findViewById(R.id.main);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        belLay = findViewById(R.id.belLay);
        progressbAR = findViewById(R.id.progressbAR);
        progress_dialog = findViewById(R.id.progress_dialog);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setText("Product Detail");
        product_name = findViewById(R.id.product_name);
        product_images = findViewById(R.id.product_images);
        product_price = findViewById(R.id.product_price);
        color_choose = findViewById(R.id.color_choose);
        product_dummy_images = findViewById(R.id.product_dummy_images);
        size_recycler = findViewById(R.id.size_recycler);
        color_recycler = findViewById(R.id.color_recycler);
        color_view = findViewById(R.id.color_view);
        text_color = findViewById(R.id.text_color);
        sold_by = findViewById(R.id.sold_by);
        description = findViewById(R.id.description);
        give_review = findViewById(R.id.give_review);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        sizeRecycler = findViewById(R.id.sizeRecycler);
        colorRecycler = findViewById(R.id.colorRecycler);
        reviewRecycler = findViewById(R.id.reviewRecycler);
        like_view = findViewById(R.id.like_view);
        no_review = findViewById(R.id.no_review);
        priseOff = findViewById(R.id.priseOff);
        product_featured_image = findViewById(R.id.product_featured_image);
        productActualPriseTV = findViewById(R.id.productActualPriseTV);
        discountPriseTV = findViewById(R.id.discountPriseTV);
        deliveryDec = findViewById(R.id.deliveryDec);
        offerTV = findViewById(R.id.offerTV);
        productTitle = findViewById(R.id.productTitle);
        likeCount = findViewById(R.id.likeCount);
        share = findViewById(R.id.share);
        likeBtn = findViewById(R.id.likeBtn);
        saveBtn = findViewById(R.id.saveBtn);
        wishlist_icon = findViewById(R.id.wishlist_icon);
        colors_count = findViewById(R.id.colors_count);
        cartCount = findViewById(R.id.cartCount);
        number_button = findViewById(R.id.number_button);
        likes = findViewById(R.id.likes);
        addToCartBtn = findViewById(R.id.addToCartBtn);
        add_cart_text = findViewById(R.id.add_cart_text);
        original_price = findViewById(R.id.original_price);
        delivery_detail = findViewById(R.id.delivery_detail);
        instock = findViewById(R.id.instock);
        successDialog = new Dialog(ProductDetail.this);
        successDialog.setContentView(R.layout.dialog_choose_attribute);
        successDialog.setCancelable(true);
        sizeRecyclerdialog = successDialog.findViewById(R.id.sizeRecycler);
        colorRecyclerdialog = successDialog.findViewById(R.id.colorRecycler);
        sizeView = successDialog.findViewById(R.id.sizeView);
        select_color = successDialog.findViewById(R.id.select_color);
        submitBtn = successDialog.findViewById(R.id.submitBtn);
        colorful_view = findViewById(R.id.colorful_view);

        cart = findViewById(R.id.cart);
        Window window = successDialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.x = 0;
        layoutParams.y = 100;
        window.setAttributes(layoutParams);
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.wobble);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    successDialog.dismiss();
                    if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
                        Log.e(MyConstants.TAG, "WAUNTITYT "+qauntity);
                        if (qauntity.equalsIgnoreCase("0")){
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("OUT OF STOCK")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Item is out of stock... Sorry about that")
                                    .setNegativeBtnText("Cancel")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("OK")
                                    .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                            size_id = null;
                            color_id = null;
                        }else{
                            progress_dialog.setVisibility(View.VISIBLE);
                            addtoguestcart();
                        }
                    }
                    else{
                        if (qauntity.equalsIgnoreCase("0")){
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("OUT OF STOCK")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Item is out of stock... Sorry about that, Add to wishlist to buy it later")
                                    .setNegativeBtnText("Cancel")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("OK")
                                    .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                            size_id = null;
                            color_id = null;
                        }else{
                            progress_dialog.setVisibility(View.VISIBLE);
                            addtocart();
                        }
                    }
                }else{
                    colorRecyclerdialog.startAnimation(animation);
                    sizeRecyclerdialog.startAnimation(animation);
                }

            }
        });
        LinearLayoutManager lm = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        product_images.setLayoutManager(lm);

        LinearLayoutManager lm1 = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        product_dummy_images.setLayoutManager(lm1);

        LinearLayoutManager lm2 = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        size_recycler.setLayoutManager(lm2);

        LinearLayoutManager lm_new = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        sizeRecyclerdialog.setLayoutManager(lm_new);

        LinearLayoutManager lm3 = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        color_recycler.setLayoutManager(lm3);

        LinearLayoutManager lm_newq1 = new LinearLayoutManager( ProductDetail.this ,LinearLayoutManager.HORIZONTAL, false);
        colorRecyclerdialog.setLayoutManager(lm_newq1);

        colorChooseData = new ArrayList<>();
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){

            progress_dialog.setVisibility(View.VISIBLE);
            getProductDetailsguest(product_id);
            getcolors();
            getGuestCartCount();
        }else{
            progress_dialog.setVisibility(View.VISIBLE);
            getProductDetails(product_id);
            getcolors();
            getCartCount();
        }
        getProductReviews();
        color_choose.setOnClickListener(this);
        color_view.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        add_cart_text.setOnClickListener(this);
        addToCartBtn.setOnClickListener(this);
        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        like_view.setOnClickListener(this);
        give_review.setOnClickListener(this);
        cartBtn.setOnClickListener(this);

    }

    public void getcolors(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "erferfger"+product_id);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<ColorResponse> call = api.color(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<ColorResponse>() {
            @Override
            public void onResponse(Call<ColorResponse> call, Response<ColorResponse> response) {
                if (response.isSuccessful()){
                    ArrayList<ColorData> colorData = new ArrayList<>(response.body().getMessage().getData());
                    Log.e(MyConstants.TAG, "SIZE OF COLOR "+colorData.size());
                    getProductColor(colorData.get(0).getAttributeValueId(), colorData.get(0).getProductId());
                    getsizes(colorData.get(0).getProductId());
                    if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
//                        checkCartGuest(colorData.get(0).getProductId());
                    }else {
                        Log.e(MyConstants.TAG, "check cart color "+colorData.size());
//                        checkCart(colorData.get(0).getProductId());
                        checkWishlist(colorData.get(0).getProductId());
                    }
                    for(int i=0; i< colorData.size(); i++){
                        getProductColor1(colorData.get(i).getAttributeValueId(), colorData.get(i).getProductId());
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<ColorResponse> call, Throwable t) {

            }
        });
    }

    public void getsizes(String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+product_id);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<SizeResponse> call = api.size(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<SizeResponse>() {
            @Override
            public void onResponse(Call<SizeResponse> call, Response<SizeResponse> response) {
                if (response.isSuccessful()){
                        ArrayList<SizeData> sizeData = new ArrayList<>(response.body().getMessage().getData());
                        productSizeRecyclerAdapter = new ProductSizeRecyclerAdapter(ProductDetail.this, sizeData);
                        size_recycler.setAdapter(productSizeRecyclerAdapter);
                        sizeRecyclerdialog.setVisibility(View.VISIBLE);
                        sizeView.setVisibility(View.VISIBLE);
                        sizeRecyclerdialog.setAdapter(productSizeRecyclerAdapter);
                }else{

                }
            }

            @Override
            public void onFailure(Call<SizeResponse> call, Throwable t) {

            }
        });
    }


    public void getProductDetails(final String product_id){
        color_choose.setVisibility(View.GONE);
        colorful_view.setVisibility(View.GONE);
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+product_id);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<ProductDetailResponse> call = api.get_product_detail(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    belLay.setVisibility(View.VISIBLE);
                    main.setVisibility(View.VISIBLE);
                    Log.e(MyConstants.TAG, "success dfgdfgf"+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        if(response.body().getMessage().getData().getSubproducts().size() != 0){
                            Log.e(MyConstants.TAG, "QANTRIRY "+response.body().getMessage().getData().getSubproducts().get(0).getProductName());
//                            getProductDetails(response.body().getMessage().getData().getProductId());
                            product_name.setText(response.body().getMessage().getData().getSubproducts().get(0).getProductName());
                            delivery_detail.setText(response.body().getMessage().getData().getSubproducts().get(0).getProductDeliveryDetail());
                            sold_by.setText("Sold By: "+response.body().getMessage().getData().getSubproducts().get(0).getProductSoldBy());
                            original_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductOriginalPrice());
                            description.setText(getResources().getString(R.string.product_description)+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductDescription());
                            product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductSalePrice());
                            number_button.setRange(0, Integer.valueOf(response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity()));
                            Glide.with(ProductDetail.this)
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + response.body().getMessage().getData().getSubproducts().get(0).getProductImage()))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_featured_image);
                            productid = response.body().getMessage().getData().getSubproducts().get(0).getProductId();
                            checkWishlist(productid);
                            checkCart(productid);
                            Log.e(MyConstants.TAG, "QANTRIRY "+response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity());
                            if (response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity().equalsIgnoreCase("0")){
                                instock.setText("OUT OF STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Red));
                                qaunt_layout.setVisibility(View.GONE);
                                number_button.setRange(0, 0);
                            }else{
                                instock.setText("IN STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Green));
                            }
                            qauntity = response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity();
                            Log.e(MyConstants.TAG, "Quantity"+qauntity);
                            name_product = response.body().getMessage().getData().getSubproducts().get(0).getProductName();
                            product_image = response.body().getMessage().getData().getSubproducts().get(0).getProductImage();

                            if (response.body().getMessage().getData().getSubproducts().get(0).getProductSalePrice() == null){
                                product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductOriginalPrice().toString());
                                original_price.setVisibility(View.GONE);
                            }
                        }else{
                            Log.e(MyConstants.TAG, "QANTRIRY "+response.body().getMessage().getData().getProductName());
                            product_name.setText(response.body().getMessage().getData().getProductName());
                            delivery_detail.setText(response.body().getMessage().getData().getProductDeliveryDetail().toString());
                            sold_by.setText("Sold By: "+response.body().getMessage().getData().getProductSoldBy());
                            original_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductOriginalPrice());
                            description.setText(getResources().getString(R.string.product_description)+" "+response.body().getMessage().getData().getProductDescription());
                            product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductSalePrice());
                            number_button.setRange(0, Integer.valueOf(response.body().getMessage().getData().getProductQuantity()));
                            if (response.body().getMessage().getData().getProductQuantity().equalsIgnoreCase("0")) {
                                Log.e(MyConstants.TAG, "QANTRIRYdasff " + response.body().getMessage().getData().getProductQuantity());
                                instock.setText("OUT OF STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Red));
                                qaunt_layout.setVisibility(View.GONE);
                                number_button.setRange(0, 0);
                            }else{
                                    instock.setText("IN STOCK");
                                    instock.setTextColor(getResources().getColor(R.color.Green));
                                }

                            if (response.body().getMessage().getData().getProductSalePrice() == null){
                                product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductOriginalPrice().toString());
                                original_price.setVisibility(View.GONE);
                            }
                            qauntity = response.body().getMessage().getData().getProductQuantity();
                            Log.e(MyConstants.TAG, "Quantity"+qauntity);
                            Glide.with(ProductDetail.this)
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + response.body().getMessage().getData().getProductImage()))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_featured_image);
                            checkWishlist(product_id);
                            productid = product_id;
                            checkCart(productid);
                            name_product = response.body().getMessage().getData().getProductName();
                            product_image = response.body().getMessage().getData().getProductImage();

                        }
                        String like = response.body().getMessage().getData().getLikes();
                        if (like != null) {
                            if (like.equalsIgnoreCase("0")) {
                                likes.setText(like + " Like");
                            } else if (like.equalsIgnoreCase("1")) {
                                likes.setText(like + " Like");
                            } else {
                                likes.setText(like + " Likes");
                            }
                        }else{
                            likes.setText("0 Like");
                        }

                        int user_like = response.body().getMessage().getData().getUser_liked();
                        if (user_like == 1){
                            heart.setImageDrawable(getResources().getDrawable(R.drawable.heart_filled));
                            like_view.setBackground(getResources().getDrawable(R.drawable.curved_draw_new));
                        }else{
                            heart.setImageDrawable(getResources().getDrawable(R.drawable.heart));
                            like_view.setBackground(getResources().getDrawable(R.drawable.curved_draw));
                        }

                        String wishlist = response.body().getMessage().getData().getWishlist();
                        if (wishlist.equalsIgnoreCase("1")){
                            wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSec)));
                        }else{
                            wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.White)));
                        }
                    }else
                    {
                        new FancyAlertDialog.Builder(ProductDetail.this)
                                .setTitle("Something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        progress_dialog.setVisibility(View.VISIBLE);
                                        getProductDetails(product_id);
                                        getcolors();
                                        getCartCount();
                                        getProductReviews();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        onBackPressed();
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    progress_dialog.setVisibility(View.VISIBLE);
                                    getProductDetails(product_id);
                                    getcolors();
                                    getCartCount();
                                    getProductReviews();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    onBackPressed();
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                progress_dialog.setVisibility(View.VISIBLE);
                                getProductDetails(product_id);
                                getcolors();
                                getCartCount();
                                getProductReviews();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                onBackPressed();
                            }
                        })
                        .build();
            }
        });
    }

    public void getProductDetailsguest(final String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+product_id);
        GuestUserProductDetail productDetailModel =  new GuestUserProductDetail(sharedPreferences.getString(MyConstants.AccessToken, ""), product_id);
        Call<ProductDetailResponse> call = api.get_product_detailguest(productDetailModel);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "success guest"+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        belLay.setVisibility(View.VISIBLE);
                        main.setVisibility(View.VISIBLE);
                        if(response.body().getMessage().getData().getSubproducts().size() != 0){
                            product_name.setText(response.body().getMessage().getData().getSubproducts().get(0).getProductName());
                            delivery_detail.setText(response.body().getMessage().getData().getSubproducts().get(0).getProductDeliveryDetail());
                            sold_by.setText("Sold By: "+response.body().getMessage().getData().getSubproducts().get(0).getProductSoldBy());
                            original_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductOriginalPrice());
                            description.setText(getResources().getString(R.string.product_description)+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductDescription());
                            product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductSalePrice());
                            number_button.setRange(0, Integer.valueOf(response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity()));
                            Glide.with(ProductDetail.this)
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + response.body().getMessage().getData().getSubproducts().get(0).getProductImage()))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_featured_image);
                            productid = response.body().getMessage().getData().getSubproducts().get(0).getProductId();
                            name_product = response.body().getMessage().getData().getSubproducts().get(0).getProductName();
                            product_image = response.body().getMessage().getData().getSubproducts().get(0).getProductImage();
                            checkCartGuest(productid);
                            qauntity = response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity();

                            if (response.body().getMessage().getData().getSubproducts().get(0).getProductQuantity().equalsIgnoreCase("0")){
                                instock.setText("OUT OF STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Red));
                                number_button.setRange(0, 0);
                                qaunt_layout.setVisibility(View.GONE);
                            }else{
                                instock.setText("IN STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Green));
                            }

                            if (response.body().getMessage().getData().getSubproducts().get(0).getProductSalePrice() == null){
                                product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getSubproducts().get(0).getProductOriginalPrice().toString());
                                original_price.setVisibility(View.GONE);
                            }
                        }else{
                            product_name.setText(response.body().getMessage().getData().getProductName());
                            delivery_detail.setText(response.body().getMessage().getData().getProductDeliveryDetail().toString());
                            sold_by.setText("Sold By: "+response.body().getMessage().getData().getProductSoldBy());
                            original_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductOriginalPrice());
                            description.setText(getResources().getString(R.string.product_description)+" "+response.body().getMessage().getData().getProductDescription());
                            product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductSalePrice());
                            number_button.setRange(0, Integer.valueOf(response.body().getMessage().getData().getProductQuantity().toString()));
                            Glide.with(ProductDetail.this)
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + response.body().getMessage().getData().getProductImage()))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progressbAR.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_featured_image);
                            productid = product_id;
                            name_product = response.body().getMessage().getData().getProductName();
                            product_image = response.body().getMessage().getData().getProductImage();
                            checkCartGuest(productid);
                            qauntity = response.body().getMessage().getData().getProductQuantity();
                            if (response.body().getMessage().getData().getProductQuantity().equalsIgnoreCase("0")){
                                Log.e(MyConstants.TAG, "QANTRIRYdasff "+response.body().getMessage().getData().getProductQuantity());
                                instock.setText("OUT OF STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Red));
                                qaunt_layout.setVisibility(View.GONE);
                                number_button.setRange(0, 0);
                            }else{
                                instock.setText("IN STOCK");
                                instock.setTextColor(getResources().getColor(R.color.Green));
                            }

                            if (response.body().getMessage().getData().getProductSalePrice() == null){
                                product_price.setText("\u20B9"+" "+response.body().getMessage().getData().getProductOriginalPrice().toString());
                                original_price.setVisibility(View.GONE);
                            }
                        }

                        heart.setImageDrawable(getResources().getDrawable(R.drawable.heart));
                        String like = response.body().getMessage().getData().getLikes();
                        if (like != null) {
                            if (like.equalsIgnoreCase("0")) {
                                likes.setText(like + " Like");
                            } else if (like.equalsIgnoreCase("1")) {
                                likes.setText(like + " Like");
                            } else {
                                likes.setText(like + " Likes");
                            }
                        }else{
                            likes.setText("0 Like");
                        }
                    }else
                    {
                        new FancyAlertDialog.Builder(ProductDetail.this)
                                .setTitle("Something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        progress_dialog.setVisibility(View.VISIBLE);
                                        getProductDetailsguest(product_id);
                                        getcolors();
                                        getGuestCartCount();
                                        getProductReviews();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        onBackPressed();
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    progress_dialog.setVisibility(View.VISIBLE);
                                    getProductDetailsguest(product_id);
                                    getcolors();
                                    getGuestCartCount();
                                    getProductReviews();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    onBackPressed();
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                progress_dialog.setVisibility(View.VISIBLE);
                                getProductDetailsguest(product_id);
                                getcolors();
                                getGuestCartCount();
                                getProductReviews();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                onBackPressed();
                            }
                        })
                        .build();
            }
        });
    }


    public void getProductColor(String pos, String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        attribute_value = pos;
        ProductColorChooseModel productColorChooseModel =  new ProductColorChooseModel(product_id, pos);
        Call<ProductColorChooseResponse> call = api.get_images(getAuthToken(), productColorChooseModel);
        call.enqueue(new Callback<ProductColorChooseResponse>() {
            @Override
            public void onResponse(Call<ProductColorChooseResponse> call, Response<ProductColorChooseResponse> response) {
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "success "+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        ArrayList<ProductColorChooseData> allProductsImages = new ArrayList<>(response.body().getMessage().getData());
                        if (allProductsImages.size() == 0){
                            product_images.setVisibility(View.GONE);
                            product_dummy_images.setVisibility(View.GONE);
                            color_choose.setVisibility(View.GONE);
                            colorful_view.setVisibility(View.GONE);
                            product_featured_image.setVisibility(View.VISIBLE);
                        }else{
                            productImagesAdapter = new ProductImagesAdapter(ProductDetail.this, allProductsImages);
                            product_images.setAdapter(productImagesAdapter);
                            color_choose.setVisibility(View.VISIBLE);
                            colorful_view.setVisibility(View.VISIBLE);
                            progressbAR.setVisibility(View.GONE);
                            productDummyImagesAdapter = new ProductDummyImagesAdapter(ProductDetail.this, allProductsImages);
                            product_dummy_images.setAdapter(productDummyImagesAdapter);
                            productDummyImagesAdapter.notifyDataSetChanged();// Notify the adapter
                            product_dummy_images.setVisibility(View.VISIBLE);
                            product_dummy_images.setVisibility(View.VISIBLE);
                        }
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ProductColorChooseResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }

    public void like(final String product_id){
        Log.e(MyConstants.TAG, "product "+product_id);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<LikeProductResponse> call = api.like(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<LikeProductResponse>() {
            @Override
            public void onResponse(Call<LikeProductResponse> call, Response<LikeProductResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {

                        heart.setImageDrawable(getResources().getDrawable(R.drawable.heart_filled));
                        getProductDetails(product_id);

                    }else
                    {
                        heart.setImageDrawable(getResources().getDrawable(R.drawable.heart));
                        progress_dialog.setVisibility(View.VISIBLE);
                        getProductDetails(product_id);
                    }
                }else{
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    like(product_id);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<LikeProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                like(product_id);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    public void getProductColor1(final String pos, final String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        attribute_value = pos;
        Log.e(MyConstants.TAG, "Id image "+attribute_value);
        ProductColorChooseModel productColorChooseModel =  new ProductColorChooseModel(product_id, pos);
        Call<ProductColorChooseResponse> call = api.get_images(getAuthToken(), productColorChooseModel);
        call.enqueue(new Callback<ProductColorChooseResponse>() {
            @Override
            public void onResponse(Call<ProductColorChooseResponse> call, Response<ProductColorChooseResponse> response) {
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "color "+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        final ArrayList<ProductColorChooseData> allProductsImages = new ArrayList<>(response.body().getMessage().getData());
                        if (allProductsImages.size() == 0){
                            colorRecyclerdialog.setVisibility(View.GONE);
                            color_id = pos;
                            select_color.setVisibility(View.GONE);
                        }else{
                            colorChooseData.add(new ProductColorChooseData(allProductsImages.get(0).getProductImage(), allProductsImages.get(0).getAttribute_value_id(), allProductsImages.get(0).getProductId()));
                            productColorRecyclerAdapter = new ProductColorRecyclerAdapter(ProductDetail.this, colorChooseData, product_id);
                            color_recycler.setAdapter(productColorRecyclerAdapter);
                            colorRecyclerdialog.setAdapter(productColorRecyclerAdapter);
                            getsizes(allProductsImages.get(0).getProductId());
                            progressbAR.setVisibility(View.GONE);
                        }
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ProductColorChooseResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }


    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.color_choose:
                if (count == 0) {
                    count = 1;
                    color_view.setVisibility(View.VISIBLE);
                }else{
                    count = 0;
                    color_view.setVisibility(View.GONE);
                }
                break;

            case R.id.saveBtn:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    Intent intent = new Intent(ProductDetail.this, Register_Screen.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    wishlist();
                }
                break;

            case R.id.like_view:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    Intent intent = new Intent(ProductDetail.this, Register_Screen.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    like(product_id);
                }
                break;

            case R.id.cartBtn:
                Log.e(MyConstants.TAG, "Cart clickd");
                Intent intent1 = new Intent(ProductDetail.this, CartProduct.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.add_cart_text:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    String title = add_cart_text.getText().toString();
                    Log.e(MyConstants.TAG, "Guest "+title);
                    if (title.equalsIgnoreCase("Add to cart")) {
                        if (isValid()) {
                            progress_dialog.setVisibility(View.VISIBLE);
                            addtoguestcart();
                        }else{
                            successDialog.show();
                        }
                    } else if (title.equalsIgnoreCase("Go to Cart")) {
                        Intent intent = new Intent(ProductDetail.this, CartProduct.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }
                }else{
                    String title = add_cart_text.getText().toString();
                    Log.e(MyConstants.TAG, "USER "+title);
                    if (title.equalsIgnoreCase("Add to cart")) {
                        if (isValid()) {
                            progress_dialog.setVisibility(View.VISIBLE);
                            addtocart();
                        }else{
                            successDialog.show();
                        }
                    } else if (title.equalsIgnoreCase("Go to Cart")) {
                        Intent intent = new Intent(ProductDetail.this, CartProduct.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }
                }

                break;

            case R.id.give_review:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    Intent intent = new Intent(ProductDetail.this, Register_Screen.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Intent intent = new Intent(ProductDetail.this, ProductReview.class);
                    intent.putExtra(MyConstants.Product_id, productid);
                    intent.putExtra(MyConstants.ProductImage, product_image);
                    intent.putExtra(MyConstants.ProductName, name_product);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                break;
        }
    }

    public boolean isValid(){
        boolean valid = true;

        if (size_id == null){
            valid = false;
        }

        if (color_id == null){
            valid = false;
        }


        return valid;
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
        super.onBackPressed();
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void checkWishlist(String productid){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(productid);
        Call<CheckWishlistResponse> call = api.check_wish(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<CheckWishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<CheckWishlistResponse> call, Response<CheckWishlistResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSec)));
                    }else{
                        wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.White)));
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CheckWishlistResponse> call, Throwable t) {

            }
        });
    }

    public void checkCart(String productid){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "PRODUCT CART CHECK "+productid);
        ProductDetailModel productDetailModel =  new ProductDetailModel(productid);
        Call<CheckWishlistResponse> call = api.check_cart(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<CheckWishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<CheckWishlistResponse> call, Response<CheckWishlistResponse> response) {
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "Cart "+response.body().getMessage().getSuccess());
                    if (response.body().getSuccess()){
                        add_cart_text.setText("Go to cart");
                    }else {
                        add_cart_text.setText("Add to cart");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CheckWishlistResponse> call, Throwable t) {

            }
        });
    }

    public void checkCartGuest(String productid){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestCartModel productDetailModel =  new GuestCartModel(sharedPreferences.getString(MyConstants.AccessToken, ""), productid);
        Call<CheckWishlistResponse> call = api.check_cart_guest(productDetailModel);
        call.enqueue(new Callback<CheckWishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<CheckWishlistResponse> call, Response<CheckWishlistResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        add_cart_text.setText("Go to cart");
                    }else {
                        add_cart_text.setText("Add to cart");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CheckWishlistResponse> call, Throwable t) {

            }
        });
    }

    public void wishlist(){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(productid);
        Call<WishlistResponse> call = api.wishlist(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<WishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "color "+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(ProductDetail.this)
                                .setTitle("Product Added to wishlist successfully")
                                .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Product Added to wishlist successfully.. Explore Product")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_white, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                        wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSec)));
                    }else
                    {
                        wishlist_icon.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.White)));
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    wishlist();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                wishlist();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void addtocart() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Log.e(MyConstants.TAG, " "+number_button.getNumber()+ productid+ color_id+ size_id);

        AddCartModel addCartModel = new AddCartModel(number_button.getNumber(), productid, color_id, size_id);
        Call<AddCartResponse> call = api.add_to_cart(getAuthToken(), addCartModel);
        call.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.e(MyConstants.TAG, "color " + response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(ProductDetail.this)
                                .setTitle("Product Added to cart successfully")
                                .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Product Added to cart successfully.. Explore Product")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_white, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                        add_cart_text.setText("Go to cart");
                        size_id = null;
                        color_id = null;
                        getCartCount();
                    } else {
                        if (response.body().getMessage().getSuccess().equalsIgnoreCase("Already added to cart")){
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("Product Added to cart successfully")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                    .setMessage("Product Added to cart successfully.. Explore Product")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Explore")
                                    .setNegativeBtnText("OK")
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_white, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }else{
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("Something went wrong")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                            progress_dialog.setVisibility(View.VISIBLE);
                                            addtocart();
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                    }
                } else {
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    progress_dialog.setVisibility(View.VISIBLE);
                                    addtocart();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                progress_dialog.setVisibility(View.VISIBLE);
                                addtocart();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    public void addtoguestcart() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Log.e(MyConstants.TAG, " GUEST"+number_button.getNumber()+ productid+ color_id+ size_id);

        AddGuetsCart addCartModel = new AddGuetsCart(number_button.getNumber(), productid, color_id, size_id, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<AddCartResponse> call = api.add_to_cartguest(addCartModel);
        call.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.e(MyConstants.TAG, "color " + response.errorBody());
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(ProductDetail.this)
                                .setTitle("Product Added to cart successfully")
                                .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Product Added to cart successfully.. Explore Product")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_white, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                        add_cart_text.setText("Go to cart");
                        size_id = null;
                        color_id = null;
                        getGuestCartCount();
                    } else {
                        if (response.body().getMessage().getSuccess().equalsIgnoreCase("Already added to cart")){
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("Product Added to cart successfully")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                    .setMessage("Product Added to cart successfully.. Explore Product")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Explore")
                                    .setNegativeBtnText("OK")
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_white, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }else{
                            new FancyAlertDialog.Builder(ProductDetail.this)
                                    .setTitle("Something went wrong")
                                    .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                            progress_dialog.setVisibility(View.VISIBLE);
                                            addtoguestcart();
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }

                    }
                } else {
                    new FancyAlertDialog.Builder(ProductDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    progress_dialog.setVisibility(View.VISIBLE);
                                    addtoguestcart();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                progress_dialog.setVisibility(View.VISIBLE);
                                addtoguestcart();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    @Override
    public void select_color(String s) {
        color_id = s;
    }

    @Override
    public void select_size(String s) {
        size_id = s;
    }

    @Override
    public void colorid(String s, String s1) {
        color_id = s;
        product_id = s1;
        productid = s1;
        getProductColor(s, s1);
        size_id = null;
//        sizeRecyclerdialog.setVisibility(View.GONE);
//        sizeView.setVisibility(View.GONE);
        getsizes(s1);
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getProductDetailsguest(s1);
            checkCartGuest(productid);
        }else{
            getProductDetails(s1);
            checkCart(productid);
            checkWishlist(productid);
        }
    }

    @Override
    public void scroll(int s) {
        product_images.scrollToPosition(s);
    }

    public void getProductReviews(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+productid);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<ReviewResponse> call = api.get_reviews(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<ReviewResponse>() {
            @Override
            public void onResponse(Call<ReviewResponse> call, Response<ReviewResponse> response) {
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "success "+response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        reviewData = new ArrayList<>(response.body().getMessage().getData());
                        productReviewRecycler = new ProductReviewsAdapter( reviewData, ProductDetail.this);
                        reviewRecycler.setAdapter(productReviewRecycler);
                        reviewRecycler.setLayoutManager(new LinearLayoutManager(ProductDetail.this));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        no_review.setVisibility(View.VISIBLE);
                        reviewRecycler.setVisibility(View.GONE);
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ReviewResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }



}
