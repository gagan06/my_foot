package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

public class Splash_Screen extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.activity_splash__screen);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        Log.e(MyConstants.TAG, "aCCESS " + sharedPreferences.getString(MyConstants.AccessToken, ""));

        if (!sharedPreferences.getString(MyConstants.Email, "").equalsIgnoreCase("")) {
            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent
                                    (Splash_Screen.this, Home_Screen.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        }
                    }, 1000
            );
        } else if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {

                            Intent i = new Intent(Splash_Screen.this, Home_Screen.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        }
                    }, 1000
            );
        } else {
            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(Splash_Screen.this, Login_Screen.class);
                            startActivity(i);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        }
                    }, 1000
            );
        }


    }


}
