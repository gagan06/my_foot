package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.thegirafe.myfoot.Adapter.NotificationAdapter;
import com.thegirafe.myfoot.Models.NotificationModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class Notifications extends AppCompatActivity implements View.OnClickListener {
    RecyclerView notificationRecycler;
    NotificationAdapter adapter = null;
    ArrayList<NotificationModel> data = new ArrayList<>();
    TextView toolbar_title;
    ImageButton backPress,cartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        toolbar_title.setText("Notifications");
        notificationRecycler = findViewById(R.id.notificationRecycler);

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);

        getNotifications();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getNotifications() {
        for (int i = 0; i < 5; i++) {
            NotificationModel model = new NotificationModel();
            model.setTitle("New Offer From Brands");
            data.add(model);
            adapter = new NotificationAdapter(data, Notifications.this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(Notifications.this);
            notificationRecycler.setLayoutManager(layoutManager);
            notificationRecycler.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i=new Intent(Notifications.this, CartProduct.class);
                startActivity(i);
                break;
        }
    }
}
