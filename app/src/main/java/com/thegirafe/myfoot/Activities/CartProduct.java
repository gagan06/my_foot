package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Adapter.CartListAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Cart.MyCartResponse;
import com.thegirafe.myfoot.Cart.ProductList;
import com.thegirafe.myfoot.Cart.RemoveCartResponse;
import com.thegirafe.myfoot.Cart.UpdateCartResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.ItemClickListener;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.PostModel.UpdateCartModel;
import com.thegirafe.myfoot.PostModel.UpdateGuestCart;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Wishlist.WishlistResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CartProduct extends AppCompatActivity implements View.OnClickListener, MoveToWishlist {
    RecyclerView cartItemsRecyclerView;
    CartListAdapter cartListAdapter;
    ArrayList<ProductList> cartData = new ArrayList<>();
    Button checkOutBtn;
    TextView toolbar_title;
    ImageButton backPress;
    TextView total_price, total_item, total_discount, total_shipping, total_tax;
    SharedPreferences sharedPreferences;
    LinearLayout main;
    Button retryBtn;
    TextView cartCount;
    String amount, item;
    int disc;
    LinearLayout apply_coupon;
    TextView apply_text, code, price;
    View empty_cart, progress_dialog, no_internet_layout, server_error;
    String coupon_id, coupon_code, coupon_amount, cart_amount;
    SharedPreferences.Editor editor;
    boolean valid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_product);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        checkOutBtn = findViewById(R.id.checkOutBtn);
        cartItemsRecyclerView = findViewById(R.id.cartItemsRecyclerView);
        cartItemsRecyclerView.setFocusable(false);

        toolbar_title = findViewById(R.id.toolbar_title);
        apply_text = findViewById(R.id.apply_text);
        cartCount = findViewById(R.id.cartCount);
        main = findViewById(R.id.main);
        total_tax = findViewById(R.id.total_tax);
        empty_cart = findViewById(R.id.empty_cart);
        progress_dialog = findViewById(R.id.progress_dialog);
        total_price = findViewById(R.id.total_price);
        total_item = findViewById(R.id.total_item);
        retryBtn = findViewById(R.id.retryBtn);
        total_discount = findViewById(R.id.total_discount);
        total_shipping = findViewById(R.id.total_shipping);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        server_error = findViewById(R.id.server_error);
        backPress = findViewById(R.id.backPress);
        apply_coupon = findViewById(R.id.apply_coupon);
        price = findViewById(R.id.price);
        toolbar_title.setText("My Cart");
        code = findViewById(R.id.code);
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartDataGuest();
            getGuestCartCount();
        }else {
            progress_dialog.setVisibility(View.VISIBLE);
            getCartData();
            getCartCount();
        }

        checkOutBtn.setOnClickListener(this);
        backPress.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
        apply_coupon.setOnClickListener(this);
    }



    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartDataGuest();
            getGuestCartCount();
        }else {
            progress_dialog.setVisibility(View.VISIBLE);
            getCartData();
            getCartCount();
        }
    }

    public void getCartData() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<MyCartResponse> call = api.get_cart(getAuthToken());
        call.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                            main.setVisibility(View.VISIBLE);
                            Log.e(MyConstants.TAG, "SUccesds");
                            cartData = new ArrayList<>(response.body().getMessage().getData().getProductList());
                            cartListAdapter = new CartListAdapter(cartData, CartProduct.this);
                            cartItemsRecyclerView.setAdapter(cartListAdapter);
                            cartItemsRecyclerView.setLayoutManager(new LinearLayoutManager(CartProduct.this));
                            total_shipping.setText(response.body().getMessage().getData().getShipping().toString());
                            total_price.setText(response.body().getMessage().getData().getTotalPrice());
                            total_item.setText(response.body().getMessage().getData().getTotalItems().toString());
                            item = response.body().getMessage().getData().getTotalItems().toString();
                            amount = response.body().getMessage().getData().getTotalPrice().toString();
                            total_discount.setText(response.body().getMessage().getData().getDiscount().toString());
                            price.setText(response.body().getMessage().getData().getPrice());
                            disc = response.body().getMessage().getData().getDiscount();
                            total_tax.setText(response.body().getMessage().getData().getTax());
                            if (!sharedPreferences.getString("Code", "").equalsIgnoreCase("")){
                                coupon_id = sharedPreferences.getString("coupon_id", "");
                                coupon_code = sharedPreferences.getString("Code", "");
                                coupon_amount = sharedPreferences.getString("discount", "");
                                cart_amount = sharedPreferences.getString("cart_amount", "");
                                String amount_new = amount.replace(",", "");
                                int amount =  Math.round(Float.parseFloat(amount_new));
                                Log.e(MyConstants.TAG, "amount all "+cart_amount+"  "+amount+"   "+amount_new);
                                if (Integer.parseInt(cart_amount) > amount){
                                    code.setVisibility(View.GONE);
                                    apply_text.setText("Apply Coupon");
                                    apply_text.setTextColor(getResources().getColor(R.color.greyhigh));
                                    editor = sharedPreferences.edit();
                                    editor.putString(MyConstants.Coupon_ID, "");
                                    editor.apply();
                                }else{
                                    String discount_new = coupon_amount.replace(",", "");
                                    int discount = Integer.parseInt(discount_new);
                                    Log.e(MyConstants.TAG, "Amount "+amount+" Discount "+discount);
                                    int t_price = amount - discount;
                                    total_price.setText(String.valueOf(t_price));
                                    int t_discount = discount + disc;
                                    total_discount.setText(String.valueOf(t_discount));
                                    apply_text.setText("Coupon Applied");
                                    code.setText(coupon_code);
                                    code.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    code.setVisibility(View.VISIBLE);
                                    apply_text.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    editor = sharedPreferences.edit();
                                    editor.putString(MyConstants.Coupon_ID, coupon_id);
                                    editor.apply();
                                }

                            }
                        }else
                        {
                            empty_cart.setVisibility(View.VISIBLE);
                            main.setVisibility(View.GONE);
                            Log.e(MyConstants.TAG, "error"+response.errorBody());
                        }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }


    public void getCartDataGuest() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "SUccesds guest");

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<MyCartResponse> call = api.get_guest_cart(guestUserModel);
        call.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                            cartData = new ArrayList<>(response.body().getMessage().getData().getProductList());
                            cartListAdapter = new CartListAdapter(cartData, CartProduct.this);
                            cartItemsRecyclerView.setAdapter(cartListAdapter);
                            cartItemsRecyclerView.setLayoutManager(new LinearLayoutManager(CartProduct.this));
                            total_shipping.setText(response.body().getMessage().getData().getShipping().toString());
                            total_price.setText(response.body().getMessage().getData().getTotalPrice().toString());
                            total_item.setText(response.body().getMessage().getData().getTotalItems().toString());
                            total_tax.setText(response.body().getMessage().getData().getTax().toString());
                            total_discount.setText(response.body().getMessage().getData().getDiscount().toString());
                            apply_coupon.setVisibility(View.GONE);
                            price.setText(response.body().getMessage().getData().getPrice());

                    }else
                    {
                        empty_cart.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.checkOutBtn:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    Intent intent = new Intent(CartProduct.this, Register_Screen.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    if (valid) {
                        i = new Intent(CartProduct.this, SelectDeliveryAddress.class);
                        i.putExtra("Amount", amount);
                        i.putExtra("Item", item);
                        startActivity(i);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }else{
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("OUT OF STOCK")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Some items in your cart is out of stock, move to wishlist to buy it later")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                }
                break;

            case R.id.apply_coupon:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                }else{
                    i = new Intent(CartProduct.this, Coupons.class);
                    i.putExtra("Amount", amount);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                break;

            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.retryBtn:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    no_internet_layout.setVisibility(View.GONE);
                    getCartDataGuest();
                    getGuestCartCount();
                }else {
                    progress_dialog.setVisibility(View.VISIBLE);
                    no_internet_layout.setVisibility(View.GONE);
                    getCartData();
                    getCartCount();
                }
                break;
        }
    }

    public void wishlist(final String product_id){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<WishlistResponse> call = api.wishlist(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {
                if (response.isSuccessful()){
                    progress_dialog.setVisibility(View.GONE);
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Product Added to wishlist successfully")
                                .setBackgroundColor(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Product Added to wishlist successfully.. Explore Product")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_white, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        getCartCount();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        getCartCount();
                                    }
                                })
                                .build();
                    }else
                    {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Looks like something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        wishlist(product_id);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(CartProduct.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    wishlist(product_id);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(CartProduct.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                wishlist(product_id);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void removeCart(final String product_id){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<RemoveCartResponse> call = api.remove_from_cart(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<RemoveCartResponse>() {
            @Override
            public void onResponse(Call<RemoveCartResponse> call, Response<RemoveCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        Toast.makeText(CartProduct.this, ""+response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                        getCartData();

                        getCartCount();
                        valid = true;
                    }else {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Looks like something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        removeCart(product_id);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                        }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(CartProduct.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    removeCart(product_id);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<RemoveCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(CartProduct.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                removeCart(product_id);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void removeGuestCart(final String product_id){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        UpdateGuestCart productDetailModel =  new UpdateGuestCart(product_id, "", sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<RemoveCartResponse> call = api.remove_from_cartguest(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<RemoveCartResponse>() {
            @Override
            public void onResponse(Call<RemoveCartResponse> call, Response<RemoveCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
//                        Toast.makeText(CartProduct.this, ""+response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                        getCartDataGuest();

                        getGuestCartCount();
                    }else {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Looks like something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        removeGuestCart(product_id);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(CartProduct.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    removeGuestCart(product_id);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<RemoveCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(CartProduct.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                removeGuestCart(product_id);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    @Override
    public void movetowish(String s) {
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            Intent i = new Intent(CartProduct.this, Register_Screen.class);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }else{
            wishlist(s);
            removeCart(s);
        }
    }

    @Override
    public void removerfromcart(String s) {
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            removeGuestCart(s);
        }else{
            removeCart(s);
        }
    }

    @Override
    public void addToCart(String s, String q) {
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            addtoGuestCart(s, q);
        }else{
            addtocart(s, q);
        }

    }

    @Override
    public void sendError(String s) {
        valid = false;
    }

    @Override
    public void gotowishlist() {
        Intent intent = new Intent(CartProduct.this, Wishlist.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public void addtocart(final String s, final String q){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        UpdateCartModel productDetailModel =  new UpdateCartModel(s, q);
        Call<UpdateCartResponse> call = api.update_to_cart(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        getCartData();
                    }else {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Looks like something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        addtocart(s, q);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(CartProduct.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addtocart(s, q);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(CartProduct.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addtocart(s, q);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void addtoGuestCart(final String s, final String q) {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        UpdateGuestCart productDetailModel =  new UpdateGuestCart(s, q, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<UpdateCartResponse> call = api.update_to_cartguest(sharedPreferences.getString(MyConstants.AccessToken, ""), productDetailModel);
        call.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        getCartDataGuest();
                    }else {
                        new FancyAlertDialog.Builder(CartProduct.this)
                                .setTitle("Looks like something went wrong")
                                .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        addtoGuestCart(s, q);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(CartProduct.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(android.graphics.Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addtoGuestCart(s, q);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(CartProduct.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(android.graphics.Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(android.graphics.Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addtoGuestCart(s, q);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

}
