package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Adapter.ProductListAdapter;
import com.thegirafe.myfoot.Adapter.ProductSizeRecyclerAdapter;
import com.thegirafe.myfoot.Adapter.WishlistAdapter;
import com.thegirafe.myfoot.Attributes.ColorData;
import com.thegirafe.myfoot.Attributes.ColorResponse;
import com.thegirafe.myfoot.Attributes.SizeData;
import com.thegirafe.myfoot.Attributes.SizeResponse;
import com.thegirafe.myfoot.Cart.AddCartResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.ItemClickListener;
import com.thegirafe.myfoot.Interfaces.MoveToCart;
import com.thegirafe.myfoot.Models.AddCartModel;
import com.thegirafe.myfoot.Models.WishlistModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.PostCatgeory;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.Products.CategoryProductResponse;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Wishlist.MyWishListData;
import com.thegirafe.myfoot.Wishlist.MyWishListResponse;
import com.thegirafe.myfoot.Wishlist.WishlistResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Wishlist extends AppCompatActivity implements View.OnClickListener, MoveToCart {
    RecyclerView wishlistRecyclerView;
    WishlistAdapter adapter;
    ArrayList<MyWishListData> data = new ArrayList<>();
    TextView toolbar_title, cartCount;
    ImageButton backPress, cartBtn;
    Button shopNowBtn;
    LinearLayout listLayout, emptyLayout;
    SharedPreferences sharedPreferences;
    ScrollView main;
    String color_id;
    Button retryBtn;
    View no_internet_layout, progress_dialog, server_error, empty_wishlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        cartCount = findViewById(R.id.cartCount);
        wishlistRecyclerView = findViewById(R.id.wishlistRecyclerView);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        shopNowBtn = findViewById(R.id.shopNowBtn);
        listLayout = findViewById(R.id.listLayout);
        emptyLayout = findViewById(R.id.emptyLayout);;
        no_internet_layout = findViewById(R.id.no_internet_layout);
        progress_dialog = findViewById(R.id.progress_dialog);
        server_error = findViewById(R.id.server_error);
        empty_wishlist = findViewById(R.id.empty_wishlist);
        main = findViewById(R.id.main);
        retryBtn = findViewById(R.id.retryBtn);
        toolbar_title.setText("Wishlist");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        shopNowBtn.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
            getData();
        }else{
            getGuestCartCount();
            progress_dialog.setVisibility(View.GONE);
            empty_wishlist.setVisibility(View.VISIBLE);
        }
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(Wishlist.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.shopNowBtn:
                i = new Intent(Wishlist.this, Home_Screen.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case R.id.retryBtn:
                no_internet_layout.setVisibility(View.GONE);
                if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    progress_dialog.setVisibility(View.VISIBLE);
                    getCartCount();
                    getData();
                }else{
                    getGuestCartCount();
                    progress_dialog.setVisibility(View.GONE);
                    empty_wishlist.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    public void getData() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<MyWishListResponse> call = api.mywishlist(getAuthToken());
        call.enqueue(new Callback<MyWishListResponse>() {
            @Override
            public void onResponse(Call<MyWishListResponse> call, Response<MyWishListResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new WishlistAdapter(data, Wishlist.this);
                        wishlistRecyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        wishlistRecyclerView.setLayoutManager(new LinearLayoutManager(Wishlist.this));
                        wishlistRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        empty_wishlist.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                    }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<MyWishListResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void wishlist(final String product_id){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<WishlistResponse> call = api.wishlist(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<WishlistResponse>() {
            @Override
            public void onResponse(Call<WishlistResponse> call, Response<WishlistResponse> response) {
                Log.e(MyConstants.TAG, "Response "+response.isSuccessful());
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    progress_dialog.setVisibility(View.VISIBLE);
                    getData();
                }else{
                    new FancyAlertDialog.Builder(Wishlist.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    wishlist(product_id);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<WishlistResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Wishlist.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                wishlist(product_id);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
            getData();
        }else{
            getGuestCartCount();
            progress_dialog.setVisibility(View.GONE);
            empty_wishlist.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addToCart(String s) {
        progress_dialog.setVisibility(View.VISIBLE);
        getcolors(s);
    }

    @Override
    public void removeFromWishlist(String s) {
        wishlist(s);
    }

    @Override
    public void gotocart() {
        Intent intent = new Intent(Wishlist.this, CartProduct.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public void getcolors(String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+product_id);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<ColorResponse> call = api.color(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<ColorResponse>() {
            @Override
            public void onResponse(Call<ColorResponse> call, Response<ColorResponse> response) {
                if (response.isSuccessful()){
                    color_id = response.body().getMessage().getData().get(0).getAttributeValueId();
                    getsizes(response.body().getMessage().getData().get(0).getProductId());
                }else{

                }
            }

            @Override
            public void onFailure(Call<ColorResponse> call, Throwable t) {

            }
        });
    }

    public void getsizes(final String product_id){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Product id "+product_id);
        ProductDetailModel productDetailModel =  new ProductDetailModel(product_id);
        Call<SizeResponse> call = api.size(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<SizeResponse>() {
            @Override
            public void onResponse(Call<SizeResponse> call, Response<SizeResponse> response) {
                if (response.isSuccessful()){
                    progress_dialog.setVisibility(View.VISIBLE);
                    addtocart(product_id, response.body().getMessage().getData().get(0).getAttributeValueId());
                }else{

                }
            }

            @Override
            public void onFailure(Call<SizeResponse> call, Throwable t) {

            }
        });
    }

    public void addtocart(final String product_id, final String size) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);


        AddCartModel addCartModel = new AddCartModel("1", product_id, color_id, size);
        Call<AddCartResponse> call = api.add_to_cart(getAuthToken(), addCartModel);
        call.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.e(MyConstants.TAG, "color " + response.body().getSuccess());
                    if (response.body().getSuccess()) { new FancyAlertDialog.Builder(Wishlist.this)
                            .setTitle("Move to cart successfully")
                            .setBackgroundColor(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                            .setMessage("Move to cart successfully.. Explore Product")
                            .setPositiveBtnBackground(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Explore")
                            .setNegativeBtnText("OK")
                            .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_tag_faces_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    wishlist(product_id);
                                    getCartCount();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    wishlist(product_id);
                                    getCartCount();
                                }
                            })
                            .build();
                    } else {
                        new FancyAlertDialog.Builder(Wishlist.this)
                                .setTitle("Something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        addtocart(product_id, size);
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(Wishlist.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addtocart(product_id, size);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Wishlist.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addtocart(product_id, size);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }
}
