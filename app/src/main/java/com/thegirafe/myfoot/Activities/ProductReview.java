package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hsalf.smilerating.SmileRating;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Cart.AddCartResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.AddCartModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.AddReviewModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Reviews.AddReviewResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductReview extends AppCompatActivity implements View.OnClickListener {
    TextView toolbar_title, reviewET;
    ImageButton backPress, cartBtn;
    SmileRating smileRating;
    String product_id, product_image, product_name;
    SharedPreferences sharedPreferences;
    Button submit;
    ImageView product_image_im;
    TextView product_name_tx, ratingError;
    String rating, review;
    View progress_dialog;
    TextView cartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_review);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        product_id = getIntent().getStringExtra(MyConstants.Product_id);
        product_name = getIntent().getStringExtra(MyConstants.ProductName);
        product_image = getIntent().getStringExtra(MyConstants.ProductImage);
        progress_dialog = findViewById(R.id.progress_dialog);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        submit = findViewById(R.id.submit);
        reviewET = findViewById(R.id.reviewET);
        product_image_im = findViewById(R.id.product_image_im);
        product_name_tx = findViewById(R.id.product_name_tx);
        smileRating = (SmileRating) findViewById(R.id.smile_rating);
        cartCount = findViewById(R.id.cartCount);
        ratingError = findViewById(R.id.ratingError);
        toolbar_title.setText("Review");
        product_name_tx.setText(product_name);

        Glide.with(ProductReview.this)
                .load(Uri.parse(MyConstants.IMAGE_PATH + product_image))
                .into(product_image_im);

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        submit.setOnClickListener(this);
        smileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
//                Toast.makeText(ProductReview.this, ""+level, Toast.LENGTH_SHORT).show();
                // level is from 1 to 5 (0 when none selected)
                // reselected is false when user selects different smiley that previously selected one
                // true when the same smiley is selected.
                // Except if it first time, then the value will be false.
            }
        });

        getCartCount();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(ProductReview.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.submit:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                if (isValid()) {
                    progress_dialog.setVisibility(View.VISIBLE);
                    addrating();
                }
                break;
        }
    }

    public boolean isValid(){
        boolean valid = true;

        rating = String.valueOf(smileRating.getRating());
        if (rating.equalsIgnoreCase("0")){
            ratingError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            ratingError.setVisibility(View.GONE);
            valid = true;
        }
        review = reviewET.getText().toString();
        if (review.length() < 30){
            reviewET.setError("Review must be of minimum 30 characters");
            valid = false;
        }
        return  valid;
    }

    public void addrating() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        AddReviewModel addReviewModel = new AddReviewModel(product_id, review, rating);
        Call<AddReviewResponse> call = api.add_review(getAuthToken(), addReviewModel);
        call.enqueue(new Callback<AddReviewResponse>() {
            @Override
            public void onResponse(Call<AddReviewResponse> call, Response<AddReviewResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(ProductReview.this)
                                .setTitle("Review Added successfully")
                                .setBackgroundColor(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Review Added successfully.. Explore Product")
                                .setPositiveBtnBackground(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_tag_faces_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        onBackPressed();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();

                    } else {
                    }
                } else {
                    new FancyAlertDialog.Builder(ProductReview.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addrating();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<AddReviewResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ProductReview.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addrating();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


}
