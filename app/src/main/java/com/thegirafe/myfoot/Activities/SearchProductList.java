package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thegirafe.myfoot.Adapter.ProductListAdapter;
import com.thegirafe.myfoot.Adapter.SearchProductListAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.PostCatgeory;
import com.thegirafe.myfoot.PostModel.ProductDearchPostGuest;
import com.thegirafe.myfoot.PostModel.ProductSearchPost;
import com.thegirafe.myfoot.Products.CategoryProductResponse;
import com.thegirafe.myfoot.Products.ProductSearchData;
import com.thegirafe.myfoot.Products.ProductSearchResponse;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchProductList extends AppCompatActivity implements View.OnClickListener {

    RecyclerView productRecycler;
    ImageButton backPress, cartBtn;
    TextView toolbar_title, cartCount;
    View no_search_found, progress_dialog, no_internet_layout, server_error;
    Intent intent;
    String name;
    ArrayList<ProductSearchData> productData;
    RelativeLayout main;
    SearchProductListAdapter adapter;
    Button retryBtn, homeBtn;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product_list);
        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();
        name = intent.getStringExtra("name");


        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
        }

        main = findViewById(R.id.main);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        progress_dialog = findViewById(R.id.progress_dialog);
        no_search_found = findViewById(R.id.no_search_found);
        cartCount = findViewById(R.id.cartCount);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        productRecycler = findViewById(R.id.productRecycler);
        retryBtn = findViewById(R.id.retryBtn);
        homeBtn = findViewById(R.id.homeBtn);
        toolbar_title.setText("Products");
        cartBtn = findViewById(R.id.cartBtn);
        cartBtn.setOnClickListener(this);
        backPress.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
        homeBtn.setOnClickListener(this);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getData();
            getCartCount();
        }else{
            getDataGuest();
            getGuestCartCount();
        }

    }

    public void getData() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "category_id "+name);

        ProductSearchPost postCatgeory = new ProductSearchPost(name);
        Call<ProductSearchResponse> call = api.get_search(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<ProductSearchResponse>() {
            @Override
            public void onResponse(Call<ProductSearchResponse> call, Response<ProductSearchResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapter = new SearchProductListAdapter(productData, SearchProductList.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(SearchProductList.this, 2));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+new Gson().toJson(response));
                        no_search_found.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);

                    }
                } else {
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<ProductSearchResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }

    public void getDataGuest() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "category_id "+name);

        ProductDearchPostGuest postCatgeory = new ProductDearchPostGuest(name, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<ProductSearchResponse> call = api.get_search(postCatgeory);
        call.enqueue(new Callback<ProductSearchResponse>() {
            @Override
            public void onResponse(Call<ProductSearchResponse> call, Response<ProductSearchResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapter = new SearchProductListAdapter(productData, SearchProductList.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(SearchProductList.this, 2));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+new Gson().toJson(response));
                        no_search_found.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);

                    }
                } else {
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<ProductSearchResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.homeBtn:
                onBackPressed();
                break;

            case R.id.retryBtn:
                no_internet_layout.setVisibility(View.GONE);
                progress_dialog.setVisibility(View.VISIBLE);
                getData();
                break;

            case R.id.addToCartBtn:
                Intent intent1 = new Intent(SearchProductList.this, CartProduct.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

        }
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }
}
