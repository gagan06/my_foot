package com.thegirafe.myfoot.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thegirafe.myfoot.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReturnProduct extends AppCompatActivity implements View.OnClickListener {
    TextView orderDate, nameError, emailError, phoneError, orderIdError, orderDateError, productNameError, quantityError, otherDetailError;
    EditText nameET, emailET, phoneNumberET, orderIdET, productNameET, quantityET, otherET;
    RadioButton radioDeadOnArrival, radioFaulty, radioOrderError, radioReceivedWrongItem, radioOther;
    Button submitBtn;
    ImageButton ordeDateBtn;
    TextView toolbar_title;
    ImageButton backPress, cartBtn;
    String dateStr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_product);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        orderDate = findViewById(R.id.orderDate);
        ordeDateBtn = findViewById(R.id.ordeDateBtn);
        submitBtn = findViewById(R.id.submitBtn);

        //Error text views
        nameError = findViewById(R.id.nameError);
        emailError = findViewById(R.id.emailError);
        phoneError = findViewById(R.id.phoneError);
        orderIdError = findViewById(R.id.orderIdError);
        orderDateError = findViewById(R.id.orderDateError);
        productNameError = findViewById(R.id.productNameError);
        quantityError = findViewById(R.id.quantityError);
        otherDetailError = findViewById(R.id.otherDetailError);

        //Edit Texts
        nameET = findViewById(R.id.nameET);
        emailET = findViewById(R.id.emailET);
        phoneNumberET = findViewById(R.id.phoneNumberET);
        orderIdET = findViewById(R.id.orderIdET);
        productNameET = findViewById(R.id.productNameET);
        quantityET = findViewById(R.id.quantityET);
        otherET = findViewById(R.id.otherET);

        //Radio Buttons
        //, , , ,
        radioDeadOnArrival = findViewById(R.id.radioDeadOnArrival);
        radioFaulty = findViewById(R.id.radioFaulty);
        radioOrderError = findViewById(R.id.radioOrderError);
        radioReceivedWrongItem = findViewById(R.id.radioReceivedWrongItem);
        radioOther = findViewById(R.id.radioOther);


        toolbar_title.setText("Return Product");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        ordeDateBtn.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(ReturnProduct.this, CartProduct.class);
                startActivity(i);
                break;
            case R.id.ordeDateBtn:
                datePicker();
                break;
        }
    }

    private void datePicker() {
        Log.v("DATES", "call");
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dateStr = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1)
                        + "-" + String.valueOf(year);

                try {
                    SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateValue = input.parse(dateStr);
                    dateStr = input.format(dateValue);
                    orderDate.setText(dateStr);
                    Log.v("DATES", dateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }
}
