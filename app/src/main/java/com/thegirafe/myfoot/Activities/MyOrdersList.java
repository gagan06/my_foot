package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.thegirafe.myfoot.Adapter.MyOrdersListAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.MyOrdersListModel;
import com.thegirafe.myfoot.Order.MyOrderData;
import com.thegirafe.myfoot.Order.MyOrderResponse;
import com.thegirafe.myfoot.Order.OrderResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.OrderModel;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyOrdersList extends AppCompatActivity implements View.OnClickListener {
RecyclerView orderListRecyclerView;
    MyOrdersListAdapter adapter;
    ArrayList<MyOrderData> data = new ArrayList<>();
    TextView toolbar_title, cartCount;
    Button retryBtn;
    ImageButton backPress,cartBtn;
    Button showNow;
    LinearLayout listLayout, emptyLayout;
    SharedPreferences sharedPreferences;
    ScrollView main;
    View no_order_to_purchase, progress_dialog, server_error, no_internet_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders_list);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        retryBtn=findViewById(R.id.retryBtn);
        orderListRecyclerView=findViewById(R.id.orderListRecyclerView);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        showNow = findViewById(R.id.showNow);
        listLayout = findViewById(R.id.listLayout);
        emptyLayout = findViewById(R.id.emptyLayout);
        main = findViewById(R.id.main);
        no_order_to_purchase = findViewById(R.id.no_order_to_purchase);
        progress_dialog = findViewById(R.id.progress_dialog);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        cartCount = findViewById(R.id.cartCount);
        toolbar_title.setText("My Orders");

        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
            getOrder();
        }else{
            getGuestCartCount();
            progress_dialog.setVisibility(View.GONE);
            no_order_to_purchase.setVisibility(View.VISIBLE);
        }

        backPress.setOnClickListener(this);
        cartBtn = findViewById(R.id.cartBtn);
        cartBtn.setOnClickListener(this);
        showNow.setOnClickListener(this);
        progress_dialog.setVisibility(View.VISIBLE);
        retryBtn.setOnClickListener(this);
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i=new Intent(MyOrdersList.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.showNow:
                i = new Intent(MyOrdersList.this, Home_Screen.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.addToCartBtn:
                Intent intent1 = new Intent(MyOrdersList.this, CartProduct.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.retryBtn:
                if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    getCartCount();
                    getOrder();
                }else{
                    getGuestCartCount();
                    progress_dialog.setVisibility(View.GONE);
                    no_order_to_purchase.setVisibility(View.VISIBLE);
                }
                break;


        }
    }

    public void getOrder(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<MyOrderResponse> call = api.my_orders(getAuthToken());
        call.enqueue(new Callback<MyOrderResponse>() {
            @Override
            public void onResponse(Call<MyOrderResponse> call, Response<MyOrderResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter=new MyOrdersListAdapter(data,MyOrdersList.this);
                        LinearLayoutManager layoutManager=new LinearLayoutManager(MyOrdersList.this);
                        orderListRecyclerView.setLayoutManager(layoutManager);
                        orderListRecyclerView.setAdapter(adapter);
                    } else {
                        no_order_to_purchase.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                    }
                } else {
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<MyOrderResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
            }
        });
        }
    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }
}
