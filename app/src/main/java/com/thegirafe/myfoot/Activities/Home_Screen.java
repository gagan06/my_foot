package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.thegirafe.myfoot.Adapter.ExpandableListAdapter;
import com.thegirafe.myfoot.Adapter.ImagePagerAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Categories.BrandsList;
import com.thegirafe.myfoot.Categories.BrandsResponse;
import com.thegirafe.myfoot.Categories.CategoryResponse;
import com.thegirafe.myfoot.Categories.CatgeoryList;
import com.thegirafe.myfoot.FeaturedProducts.FeaturedProductData;
import com.thegirafe.myfoot.FeaturedProducts.FeaturedProductResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Offers.OffersSliderData;
import com.thegirafe.myfoot.Offers.OffersSliderResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.StateModel;
import com.thegirafe.myfoot.Products.LatestProductData;
import com.thegirafe.myfoot.Products.LatestProductResponse;
import com.thegirafe.myfoot.Products.ProductNameData;
import com.thegirafe.myfoot.Products.ProductNameResponse;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.UserProfileResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Home_Screen extends AppCompatActivity
        implements  View.OnClickListener, ExpandableListView.OnGroupClickListener, GoogleApiClient.OnConnectionFailedListener {

    ImageButton search_icon, cartBtn;
    DrawerLayout drawer;
    LinearLayout menuBtn;
    TextView cartCount;
    ViewPager viewPager;
    ArrayList<FeaturedProductData> featuredProductData;
    ArrayList<LatestProductData> latestProductData;
    int currentPage = 0;
    int NUM_PAGES = 0;
    final long DELAY_MS = 500;
    Timer masterTimer;
    ArrayList<CatgeoryList> allCategories;
    ArrayList<BrandsList> allBrands;
    ExpandableListView expListView;
    RequestOptions requestOptions;
    Button retryBtn;
    ArrayList<OffersSliderData> SliderData = new ArrayList<>();
    Intent new_intent;
    ExpandableListAdapter listAdapter = null;
    List<String> category;
    LinearLayout featured_title, latest_title;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Intent intent = null;
    SharedPreferences sharedPreferences;
    TextView text1, text2, text3, text4, txt, latest_text1,  latest_text2,  latest_text3,  latest_text4,  latest_text5;
    ImageView product1, product2, product3, product4, product_im, latest_im1,  latest_im2,  latest_im3,  latest_im4,  latest_im5;
    View progress_dialog, server_error, no_internet_layout;
    TextView user_name, user_designation;
    ImageView profile_image;
    SharedPreferences.Editor  editor;
    GoogleApiClient mGoogleSignInClient;
    ScrollView home;
    RelativeLayout singleItem, moreItem, more_item2, more_item3, more_item1, singleItem1;
    AsymmetricGridView asymmetricGridView;
    AutoCompleteTextView search_product;
    ArrayList<ProductNameData> productNameData;
    ProgressBar progress, progress1, progress2, progress3, pr, progress_latest1, progress_latest2, progress_latest3, progress_latest4, progress_latest5;
    RelativeLayout men_grid, women_grid, kids_grid;
    LinearLayout category_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home__screen);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        menuBtn = findViewById(R.id.menuBtn);
        men_grid = findViewById(R.id.men_grid);
        women_grid = findViewById(R.id.women_grid);
        kids_grid = findViewById(R.id.kids_grid);
        more_item1 = findViewById(R.id.more_item1);
        singleItem1 = findViewById(R.id.single_item1);
        progress_latest1 = findViewById(R.id.progress_latest1);
        progress_latest2 = findViewById(R.id.progress_latest2);
        progress_latest3 = findViewById(R.id.progress_latest3);
        progress_latest4 = findViewById(R.id.progress_latest4);
        progress_latest5 = findViewById(R.id.progress_latest5);
        latest_title = findViewById(R.id.latest_title);
        latest_text1 = findViewById(R.id.latest_text1);
        latest_text2 = findViewById(R.id.latest_text2);
        latest_text3 = findViewById(R.id.latest_text3);
        latest_text4 = findViewById(R.id.latest_text4);
        latest_text5 = findViewById(R.id.latest_text5);
        latest_im1 = findViewById(R.id.latest_im1);
        latest_im2 = findViewById(R.id.latest_im2);
        latest_im3 = findViewById(R.id.latest_im3);
        latest_im4 = findViewById(R.id.latest_im4);
        latest_im5 = findViewById(R.id.latest_im5);
        category_view = findViewById(R.id.category_view);
        pr = findViewById(R.id.pr);
        product_im = findViewById(R.id.product_im);
        txt = findViewById(R.id.txt);
        singleItem = findViewById(R.id.single_item);
        moreItem = findViewById(R.id.more_item);
        more_item3 = findViewById(R.id.more_item3);
        more_item2 = findViewById(R.id.more_item2);
        user_name = findViewById(R.id.user_name);
        profile_image = findViewById(R.id.profile_image);
        user_designation = findViewById(R.id.user_designation);
        search_icon = findViewById(R.id.search_icon);
        cartBtn = findViewById(R.id.cartBtn);
        cartCount = findViewById(R.id.cartcount);
        viewPager = findViewById(R.id.pager);
        product1 = findViewById(R.id.product_image1);
        product2 = findViewById(R.id.product_image2);
        product3 = findViewById(R.id.product_image3);
        product4 = findViewById(R.id.product_image4);
        progress = findViewById(R.id.progress);
        progress1 = findViewById(R.id.progress1);
        progress2 = findViewById(R.id.progress2);
        progress3 = findViewById(R.id.progress3);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        server_error = findViewById(R.id.server_error);
        home = findViewById(R.id.home);
        progress_dialog = findViewById(R.id.progress_dialog);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        featured_title = findViewById(R.id.featured_title);
        search_product = findViewById(R.id.search_product);

        // get the listview
        expListView = findViewById(R.id.list_slidermenu);
        retryBtn = findViewById(R.id.retryBtn);
        getOffers();

        category_view.setVisibility(View.GONE);
        progress_dialog.setVisibility(View.VISIBLE);
        getCategories();

        Log.e(MyConstants.TAG, "Guest ir nit "+sharedPreferences.getString(MyConstants.AccessToken, ""));

        featured_title.setVisibility(View.GONE);
        latest_title.setVisibility(View.GONE);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getFeatured_products();
            getCartCount();
        }else{
            getFeatured_productsguest();
            getGuestCartCount();
        }
        getlatest_products();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        search_icon.setOnClickListener(this);
        menuBtn.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        expListView.setOnGroupClickListener(this);
        retryBtn.setOnClickListener(this);

        //For Master Slider
        /*After setting the adapter use the timer */
        final Handler masterHandler = new Handler();
        final Runnable UpdateMaster = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            currentPage = 0;
                        }
                    }, 3000);

                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };

        masterTimer = new Timer(); // This will create a new Thread
        masterTimer.schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                masterHandler.post(UpdateMaster);
            }
        }, DELAY_MS, 3000);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (groupPosition == 1) {
                    new_intent = new Intent(Home_Screen.this, Products_List.class);
                    new_intent.putExtra(MyConstants.GroupPosition, String.valueOf(groupPosition));
                    new_intent.putExtra(MyConstants.Category_Id, allCategories.get(childPosition).getId().toString());
                    startActivity(new_intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    drawer.closeDrawers();
                    parent.collapseGroup(groupPosition);
                }

                return false;
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        user_profile();
        saveddData();
        getallproducts_names();

        men_grid.setOnClickListener(this);
        women_grid.setOnClickListener(this);
        kids_grid.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getFeatured_products();
            getCartCount();
        }else{
            getFeatured_productsguest();
            getGuestCartCount();
        }
        getOffers();
        getlatest_products();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.menuBtn:
                drawer.openDrawer(GravityCompat.START);
                break;

            case R.id.search_icon:
                Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show();
                break;

            case R.id.cartBtn:
                i=new Intent(Home_Screen.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.women_grid:
                int id = 2;
                if (category.contains("WOMEN")){
                    id = category.indexOf("WOMEN");
                }
                new_intent = new Intent(Home_Screen.this, Products_List.class);
                new_intent.putExtra(MyConstants.Category_Id, allCategories.get(id).getId().toString());
                startActivity(new_intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.men_grid:
                int id1 = 1;
                if (category.contains("MEN")){
                    id1 = category.indexOf("MEN");
                }
                new_intent = new Intent(Home_Screen.this, Products_List.class);
                new_intent.putExtra(MyConstants.Category_Id, allCategories.get(id1).getId().toString());
                startActivity(new_intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.kids_grid:
                int id2 = 3;
                if (category.contains("KIDS")){
                    id2 = category.indexOf("KIDS");
                }
                new_intent = new Intent(Home_Screen.this, Products_List.class);
                new_intent.putExtra(MyConstants.Category_Id, allCategories.get(id2).getId().toString());
                startActivity(new_intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.retryBtn:
                no_internet_layout.setVisibility(View.GONE);
                menuBtn.setClickable(true);
                if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    getFeatured_products();
                    getCategories();
                    getCartCount();
                }else{
                    getFeatured_productsguest();
                    getCategories();
                    getGuestCartCount();
                }

                getlatest_products();
                getallproducts_names();
                getOffers();
                break;
        }
    }

    @Override
    public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
        switch (groupPosition) {
            case 0:
                intent = new Intent(Home_Screen.this, Home_Screen.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                drawer.closeDrawers();
                break;

            case 1:
                break;

            case 2:

                intent = new Intent(Home_Screen.this, CartProduct.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                drawer.closeDrawers();
                break;

            case 3:

                intent = new Intent(Home_Screen.this, Wishlist.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                drawer.closeDrawers();
                break;

            case 4:

                intent = new Intent(Home_Screen.this, MyOrdersList.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                drawer.closeDrawers();
                break;

            case 5:

                intent = new Intent(Home_Screen.this, MyAccount.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                drawer.closeDrawers();
                break;

            case 6:
                String name = expandableListView.getAdapter().getItem(groupPosition).toString();
                Log.e(MyConstants.TAG, ""+name);
                if (name.equalsIgnoreCase("login")) {
                    intent = new Intent(Home_Screen.this, Login_Screen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                    drawer.closeDrawers();
                }else{
                    editor = sharedPreferences.edit();
                    editor.clear().apply();
                    intent = new Intent(Home_Screen.this, Login_Screen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                    if (mGoogleSignInClient != null && mGoogleSignInClient.isConnected()) {
                        Auth.GoogleSignInApi.signOut(mGoogleSignInClient);
                    }

                    if (AccessToken.getCurrentAccessToken() != null && com.facebook.Profile.getCurrentProfile() != null){
                        //Logged in so show the login button
                        //log out
                        LoginManager.getInstance().logOut();
                    }
                }
                break;
        }

        return false;
    }

    public void getMenu() {
        Log.e(MyConstants.TAG, "Array menu " + allCategories.size());
        category = new ArrayList<>();
        for (int i = 0; i < allCategories.size(); i++) {
            category.add(allCategories.get(i).getName());
        }

        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            listDataHeader.add(getResources().getString(R.string.nav_home));
            listDataHeader.add(getResources().getString(R.string.nav_category));
            listDataHeader.add(getResources().getString(R.string.nav_cart));
            listDataHeader.add(getResources().getString(R.string.nav_wishlist));
            listDataHeader.add(getResources().getString(R.string.nav_myorders));
            listDataHeader.add(getResources().getString(R.string.nav_myaccount));
            listDataHeader.add(getResources().getString(R.string.nav_logout));
        }else{
            listDataHeader.add(getResources().getString(R.string.nav_home));
            listDataHeader.add(getResources().getString(R.string.nav_category));
            listDataHeader.add(getResources().getString(R.string.nav_cart));
            listDataHeader.add(getResources().getString(R.string.nav_wishlist));
            listDataHeader.add(getResources().getString(R.string.nav_myorders));
            listDataHeader.add(getResources().getString(R.string.nav_myaccount));
            listDataHeader.add(getResources().getString(R.string.nav_loginbtn));
        }

        Log.e(MyConstants.TAG, "aCCESS "+sharedPreferences.getString(MyConstants.AccessToken, ""));


        Log.e(MyConstants.TAG, "Size " + category.size());
        //Category Child Data
        listDataChild.put(listDataHeader.get(0), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(1), category);
        listDataChild.put(listDataHeader.get(2), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(3), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(4), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(5), new ArrayList<String>());
        listDataChild.put(listDataHeader.get(6), new ArrayList<String>());

        listAdapter = new ExpandableListAdapter(Home_Screen.this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        menuBtn.setClickable(true);
        menuBtn.setEnabled(true);
    }

    public void getCategories(){

        menuBtn.setClickable(false);
        menuBtn.setEnabled(false);
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<CategoryResponse> call = api.get_categories(getAuthToken());
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        allCategories = new ArrayList<>(response.body().getMessage().getData());
                        category_view.setVisibility(View.VISIBLE);
                        getMenu();
//                        getBrands();

                        Log.e(MyConstants.TAG, "Size "+allCategories.size());
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getBrands(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<BrandsResponse> call = api.get_brands(getAuthToken());
        call.enqueue(new Callback<BrandsResponse>() {
            @Override
            public void onResponse(Call<BrandsResponse> call, Response<BrandsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        allBrands = new ArrayList<>(response.body().getMessage().getData());

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<BrandsResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getFeatured_products(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<FeaturedProductResponse> call = api.getFeaturedProducts(getAuthToken());
        call.enqueue(new Callback<FeaturedProductResponse>() {
            @Override
            public void onResponse(Call<FeaturedProductResponse> call, Response<FeaturedProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        featured_title.setVisibility(View.VISIBLE);
                        requestOptions = new RequestOptions();
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                        home.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        featuredProductData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, featuredProductData.size()+"");
                        ArrayList<FeaturedProductData> featuredProductData1 = new ArrayList<>();

                        if (featuredProductData.size() == 0){
                            more_item2.setVisibility(View.GONE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.GONE);
                            featured_title.setVisibility(View.GONE);
                        }else if(featuredProductData.size() == 1){
                            more_item2.setVisibility(View.GONE);
                            singleItem.setVisibility(View.VISIBLE);
                            moreItem.setVisibility(View.GONE);
                            featuredProductData1.add(new FeaturedProductData(featuredProductData.get(0).getProductId(), featuredProductData.get(0).getProductName(), featuredProductData.get(0).getFeaturedImage()));
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_im);

                            txt.setText(featuredProductData.get(0).getProductName());
                            product_im.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Home_Screen.this, ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if(featuredProductData.size() == 2){
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.GONE);
                            for (int i=0; i< 2; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Home_Screen.this, ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Home_Screen.this, ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if (featuredProductData.size() == 3){
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.VISIBLE);
                            moreItem.setVisibility(View.GONE);
                            for (int i=0; i< 3; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Home_Screen.this, ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(2).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_im);

                            txt.setText(featuredProductData.get(2).getProductName());
                            product_im.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                        }else{
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.VISIBLE);
                            for (int i=0; i< 4; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }



//
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(2).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress2.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress2.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product3);
                            text3.setText(featuredProductData.get(2).getProductName());
                            product3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(3).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress3.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress3.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product4);
                            text4.setText(featuredProductData.get(3).getProductName());
                            product4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(3).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());

                        featured_title.setVisibility(View.GONE);
                    }
                }else{
                    //server errorserver_error
                    server_error.setVisibility(View.VISIBLE);
                    home.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<FeaturedProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                menuBtn.setClickable(false);
                no_internet_layout.setVisibility(View.VISIBLE);
                home.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getlatest_products(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<LatestProductResponse> call = api.get_all_products(getAuthToken());
        call.enqueue(new Callback<LatestProductResponse>() {
            @Override
            public void onResponse(Call<LatestProductResponse> call, Response<LatestProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        latest_title.setVisibility(View.VISIBLE);
                        requestOptions = new RequestOptions();
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                        home.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        latestProductData = new ArrayList<>(response.body().getMessage().getData());
//                        Log.e(MyConstants.TAG, featuredProductData.size()+"");
                        ArrayList<LatestProductData> featuredProductData1 = new ArrayList<>();

                        if (latestProductData.size() == 0){
                            more_item3.setVisibility(View.GONE);
                            singleItem1.setVisibility(View.GONE);
                            more_item1.setVisibility(View.GONE);
                            latest_title.setVisibility(View.GONE);
                        }else if(latestProductData.size() == 1){
                            more_item3.setVisibility(View.GONE);
                            singleItem1.setVisibility(View.VISIBLE);
                            more_item1.setVisibility(View.GONE);
                            featuredProductData1.add(new LatestProductData(latestProductData.get(0).getProductId(), latestProductData.get(0).getProductName(), latestProductData.get(0).getProductImage()));
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(0).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest3.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest3.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im3);

                            latest_text3.setText(latestProductData.get(0).getProductName());
                            latest_im3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if(latestProductData.size() == 2){
                            more_item3.setVisibility(View.VISIBLE);
                            singleItem1.setVisibility(View.GONE);
                            more_item1.setVisibility(View.GONE);
                            for (int i=0; i< 2; i++){
                                featuredProductData1.add(new LatestProductData(latestProductData.get(i).getProductId(), latestProductData.get(i).getProductName(), latestProductData.get(i).getProductImage()));
                            }
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(0).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im1);

                            latest_text1.setText(latestProductData.get(0).getProductName());
                            latest_im1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(1).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im2);
                            latest_text2.setText(latestProductData.get(1).getProductName());
                            latest_im2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if (latestProductData.size() == 3){
                            more_item3.setVisibility(View.VISIBLE);
                            singleItem1.setVisibility(View.VISIBLE);
                            more_item1.setVisibility(View.GONE);
                            for (int i=0; i< 3; i++){
                                featuredProductData1.add(new LatestProductData(latestProductData.get(i).getProductId(), latestProductData.get(i).getProductName(), latestProductData.get(i).getProductImage()));
                            }

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(0).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im1);

                            latest_text1.setText(latestProductData.get(0).getProductName());
                            latest_im1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(0).getProductId());
                                    startActivity(intent);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(1).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im2);
                            latest_text2.setText(latestProductData.get(1).getProductName());
                            latest_im2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(2).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest3.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest3.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im3);

                            latest_text3.setText(latestProductData.get(2).getProductName());
                            latest_im3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                        }else{
                            more_item3.setVisibility(View.VISIBLE);
                            singleItem1.setVisibility(View.GONE);
                            more_item1.setVisibility(View.VISIBLE);
                            for (int i=0; i< 4; i++){
                                featuredProductData1.add(new LatestProductData(latestProductData.get(i).getProductId(), latestProductData.get(i).getProductName(), latestProductData.get(i).getProductImage()));
                            }

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(0).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im1);

                            latest_text1.setText(latestProductData.get(0).getProductName());
                            latest_im1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(1).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest2.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im2);
                            latest_text2.setText(latestProductData.get(1).getProductName());
                            latest_im2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(2).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest4.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest4.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im4);
                            latest_text4.setText(latestProductData.get(2).getProductName());
                            latest_im4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + latestProductData.get(3).getProductImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress_latest5.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress_latest5.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(latest_im5);
                            latest_text5.setText(latestProductData.get(3).getProductName());
                            latest_im5.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, latestProductData.get(3).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());

                        latest_title.setVisibility(View.GONE);
                    }
                }else{
                    //server errorserver_error
                    server_error.setVisibility(View.VISIBLE);
                    home.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<LatestProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                menuBtn.setClickable(false);
                no_internet_layout.setVisibility(View.VISIBLE);
                home.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void user_profile(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<UserProfileResponse> call = api.user_profile(getAuthToken());
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        user_name.setText(response.body().getMessage().getData().getName());
                        user_designation.setText(response.body().getMessage().getData().getEmail());
                        if (response.body().getMessage().getData().getProfileImage() != null){
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.Profile_Image + response.body().getMessage().getData().getProfileImage()))
                                    .into(profile_image);
                            if (response.body().getMessage().getData().getProvider() == null){
                            }else if(response.body().getMessage().getData().getProvider().equalsIgnoreCase("google")){
                                Glide.with(getApplicationContext())
                                        .load(response.body().getMessage().getData().getProfileImage())
                                        .into(profile_image);
                            }else if(response.body().getMessage().getData().getProvider().equalsIgnoreCase("facebook")){
                                Glide.with(getApplicationContext())
                                        .load(response.body().getMessage().getData().getProfileImage())
                                        .into(profile_image);
                            }else{
                                Glide.with(getApplicationContext())
                                        .load(Uri.parse(MyConstants.Profile_Image + response.body().getMessage().getData().getProfileImage()))
                                        .into(profile_image);
                            }
                        }
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                                        server_error.setVisibility(View.VISIBLE);
                                        home.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void saveddData(){
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
            if (sharedPreferences.getString(MyConstants.Provider, "").equals("")) {
            } else if(sharedPreferences.getString(MyConstants.Provider, "").equals("normal")){
                Glide.with(getApplicationContext())
                        .load(Uri.parse(MyConstants.Profile_Image + sharedPreferences.getString(MyConstants.ProfileImage, "")))
                        .into(profile_image);
            }else {
                Glide.with(getApplicationContext())
                        .load(sharedPreferences.getString(MyConstants.ProfileImage, ""))
                        .into(profile_image);
            }

            user_name.setText(sharedPreferences.getString(MyConstants.Username, ""));
            user_designation.setText(sharedPreferences.getString(MyConstants.Email, ""));
        }
    }

    public void getFeatured_productsguest(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<FeaturedProductResponse> call = api.getFeaturedProductsGuest(guestUserModel);
        call.enqueue(new Callback<FeaturedProductResponse>() {
            @Override
            public void onResponse(Call<FeaturedProductResponse> call, Response<FeaturedProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {

                        featured_title.setVisibility(View.VISIBLE);
                        home.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds guest");

                        requestOptions = new RequestOptions();
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
                        featuredProductData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, featuredProductData.size()+"");
                        ArrayList<FeaturedProductData> featuredProductData1 = new ArrayList<>();
                        if (featuredProductData.size() == 0){
                            more_item2.setVisibility(View.GONE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.GONE);
                            featured_title.setVisibility(View.GONE);
                        }else if(featuredProductData.size() == 1){
                            more_item2.setVisibility(View.GONE);
                            singleItem.setVisibility(View.VISIBLE);
                            moreItem.setVisibility(View.GONE);
                            featuredProductData1.add(new FeaturedProductData(featuredProductData.get(0).getProductId(), featuredProductData.get(0).getProductName(), featuredProductData.get(0).getFeaturedImage()));
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_im);

                            txt.setText(featuredProductData.get(0).getProductName());
                            product_im.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if(featuredProductData.size() == 2){
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.GONE);
                            for (int i=0; i< 2; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }else if (featuredProductData.size() == 3){
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.VISIBLE);
                            moreItem.setVisibility(View.GONE);
                            for (int i=0; i< 3; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(2).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            pr.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product_im);

                            txt.setText(featuredProductData.get(2).getProductName());
                            product_im.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                        }else{
                            more_item2.setVisibility(View.VISIBLE);
                            singleItem.setVisibility(View.GONE);
                            moreItem.setVisibility(View.VISIBLE);
                            for (int i=0; i< 4; i++){
                                featuredProductData1.add(new FeaturedProductData(featuredProductData.get(i).getProductId(), featuredProductData.get(i).getProductName(), featuredProductData.get(i).getFeaturedImage()));
                            }

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(0).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product1);

                            text1.setText(featuredProductData.get(0).getProductName());
                            product1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(0).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(1).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress1.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product2);
                            text2.setText(featuredProductData.get(1).getProductName());
                            product2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(1).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(2).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress2.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress2.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product3);
                            text3.setText(featuredProductData.get(2).getProductName());
                            product3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(2).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });

                            Glide.with(getApplicationContext())
                                    .load(Uri.parse(MyConstants.IMAGE_PATH + featuredProductData.get(3).getFeaturedImage()))
                                    .apply(requestOptions
                                            .skipMemoryCache(true)
                                            .placeholder(R.color.Black))
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            progress3.setVisibility(View.GONE);
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            progress3.setVisibility(View.GONE);
                                            return false;
                                        }
                                    })
                                    .into(product4);
                            text4.setText(featuredProductData.get(3).getProductName());
                            product4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), ProductDetail.class);
                                    intent.putExtra(MyConstants.Product_id, featuredProductData.get(3).getProductId());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                        }
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        featured_title.setVisibility(View.GONE);
                    }
                }else{
                    //server error

                    server_error.setVisibility(View.VISIBLE);
                    home.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<FeaturedProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                home.setVisibility(View.GONE);
                menuBtn.setClickable(false);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getallproducts_names() {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        StateModel stateModel = new StateModel("101");

        Call<ProductNameResponse> call = api.get_names();
        call.enqueue(new Callback<ProductNameResponse>() {
            @Override
            public void onResponse(Call<ProductNameResponse> call, Response<ProductNameResponse> response) {
//                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        productNameData = new ArrayList<>(response.body().getMessage().getData());
                        ArrayList<String> names = new ArrayList<>();
                        for (int i=0; i<productNameData.size(); i++){
                            names.add(productNameData.get(i).getProductName());
                        }
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext() ,R.layout.simple_list_item,names);
                        search_product.setAdapter(adapter);
                        search_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                String itemName = adapter.getItem(position);

                                for (ProductNameData pojo : productNameData) {
                                    if (pojo.getProductName().equals(itemName)) {
                                        Intent intent = new Intent(Home_Screen.this, SearchProductList.class);
                                        intent.putExtra("name", itemName);
                                        startActivity(intent);
                                        search_product.setText("");
//                                        String state_id = pojo.getId().toString(); // This is the correct ID
//                                        Log.e(MyConstants.TAG, "State "+state_id);
                                        break; // No need to keep looping once you found it.
                                    }
                                }


                            }
                        });

                        search_product.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                    Intent intent = new Intent(Home_Screen.this, SearchProductList.class);
                                    intent.putExtra("name", search_product.getText().toString());
                                    startActivity(intent);
                                    return true;
                                }
                                return false;
                            }
                        });


                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        search_product.setEnabled(false);
                        search_product.setClickable(false);
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<ProductNameResponse> call, Throwable t) {
//                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getOffers() {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);



        Call<OffersSliderResponse> call = api.get_offers();
        call.enqueue(new Callback<OffersSliderResponse>() {
            @Override
            public void onResponse(Call<OffersSliderResponse> call, Response<OffersSliderResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        OffersSliderData mData = new OffersSliderData();

                        for (int i = 0; i < response.body().getMessage().getData().size(); i++) {


                            mData  = response.body().getMessage().getData().get(i);

                            SliderData.add(mData);
                            ImagePagerAdapter adapter = new ImagePagerAdapter(Home_Screen.this, SliderData);
                            viewPager.setAdapter(adapter);
                            //indicator.setViewPager(viewPager);
                            currentPage = adapter.getItemPosition(adapter);
                            NUM_PAGES = adapter.getCount();

                        }

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    home.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<OffersSliderResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                home.setVisibility(View.GONE);
                menuBtn.setClickable(false);
            }
        });
    }
}
