package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ForgetPasswordModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.ForgetPasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Forgot_Password extends AppCompatActivity implements View.OnClickListener {
    TextInputEditText PhoneET;
    Button submitBtn;
    ImageButton backPress;
    TextView toolbar_title;
    String MobileNumber;
    View progress_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__password);

        PhoneET = findViewById(R.id.mobileForgotPass);
        submitBtn = findViewById(R.id.submitBtn);
        backPress = findViewById(R.id.backPress);
        toolbar_title = findViewById(R.id.toolbar_title);
        progress_dialog = findViewById(R.id.progress_dialog);

        toolbar_title.setText("Forgot Password");

        submitBtn.setOnClickListener(this);
        backPress.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }

    @Override
    public void onClick(View view) {
        Intent i=null;
        switch (view.getId()) {
            case R.id.submitBtn:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                if (isValid()) {
                    forget_password();
                }
                break;
            case R.id.backPress:
                onBackPressed();
                break;
        }
    }

    public boolean isValid() {
        Boolean valid = true;

        MobileNumber = PhoneET.getText().toString().trim();
        if (MobileNumber.isEmpty() && MobileNumber.length() > 10 && MobileNumber.length() < 10) {
            PhoneET.setError("Invalid mobile number");
            valid = false;
        }
        if (MobileNumber.length() > 10) {
            PhoneET.setError("Invalid mobile number");
            valid = false;
        }

        if (MobileNumber.startsWith("0")) {
            PhoneET.setError("Invalid mobile number");
            valid = false;
        }


        if (MobileNumber.length() < 10) {
            PhoneET.setError("Invalid mobile number");
            valid = false;
        }
        return valid;
    }

    public void forget_password(){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        ForgetPasswordModel forgetPasswordModel = new ForgetPasswordModel(MobileNumber);
        Call<ForgetPasswordResponse> call = api.forget_password(forgetPasswordModel);
        call.enqueue(new Callback<ForgetPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Intent intent = new Intent(Forgot_Password.this, OTP_Screen.class);
                        intent.putExtra(MyConstants.Phone, MobileNumber);
                        intent.putExtra("type","reset");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }else
                    {
                        if (response.body().getMessage().getSuccess() != null){
                                    new FancyAlertDialog.Builder(Forgot_Password.this)
                                        .setTitle("Something went wrong")
                                        .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                        .setMessage("Mobile number does not exist in our records")
                                        .setNegativeBtnText("OK")
                                        .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                        .setPositiveBtnText("Try Again")
                                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                        .setAnimation(Animation.POP)
                                        .isCancellable(true)
                                        .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                        .OnPositiveClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                            }
                                        })
                                        .OnNegativeClicked(new FancyAlertDialogListener() {
                                            @Override
                                            public void OnClick() {
                                            }
                                        })
                                        .build();
                        }

                    }
                }else{
                    new FancyAlertDialog.Builder(Forgot_Password.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    forget_password();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Forgot_Password.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                forget_password();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }

}
