
package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

public class OrderFailure extends AppCompatActivity {

    TextView dialogTitle, description;
    Intent intent;
    String title;
    Button viewOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_failure);

        dialogTitle = findViewById(R.id.dialogTitle);
        viewOrders = findViewById(R.id.viewOrders);
        description = findViewById(R.id.description);
        intent = getIntent();
        title = intent.getStringExtra("order");
        Log.e(MyConstants.TAG, "SUCCESS order "+title);
        description.setText(title);
        dialogTitle.setText("Order Failed");
        viewOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderFailure.this, MyOrdersList.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }
}
