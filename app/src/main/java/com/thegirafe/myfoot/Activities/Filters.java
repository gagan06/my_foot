package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thegirafe.myfoot.Adapter.BrandFilterAdapter;
import com.thegirafe.myfoot.Adapter.ColorFilterAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Cart.ProductList;
import com.thegirafe.myfoot.Categories.BrandsList;
import com.thegirafe.myfoot.Categories.BrandsResponse;
import com.thegirafe.myfoot.Filters.FilterColorData;
import com.thegirafe.myfoot.Filters.FilterColorResponse;
import com.thegirafe.myfoot.Fragments.BrandFilter;
import com.thegirafe.myfoot.Fragments.ColorFilter;
import com.thegirafe.myfoot.Fragments.PriceFilter;
import com.thegirafe.myfoot.Fragments.SizeFilter;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.BrandFilterInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.BrandModel;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Filters extends AppCompatActivity implements View.OnClickListener, BrandFilterInterface {
    TextView brandTV, priceTV, colorTV, sizeTV;
    int sdk = 0;
    TextView toolbar_title, cartCount;
    ImageButton backPress,cartBtn;
    RecyclerView brands, colors;
    ArrayList<BrandsList> data = new ArrayList<>();
    ArrayList<FilterColorData> dataColor=new ArrayList<>();
    ColorFilterAdapter adapterColor;
    BrandFilterAdapter adapter;
    SharedPreferences sharedPreferences;
    Button applyBrand;
    ArrayList<String> brandsl = new ArrayList<>();
    ArrayList<String> colorl = new ArrayList<>();
    String category_id;
    LinearLayout main;
    View progress_dialog;

    //filterContainer
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        category_id = getIntent().getStringExtra(MyConstants.Category_Id);
        sdk = android.os.Build.VERSION.SDK_INT;

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);

        toolbar_title.setText("Filter");
        cartCount = findViewById(R.id.cartCount);
        brandTV = findViewById(R.id.brandTV);
        main = findViewById(R.id.main);
        progress_dialog = findViewById(R.id.progress_dialog);
        priceTV = findViewById(R.id.priceTV);
        colorTV = findViewById(R.id.colorTV);
        brands = findViewById(R.id.brands);
        colors = findViewById(R.id.colors);
        sizeTV = findViewById(R.id.sizeTV);
        applyBrand = findViewById(R.id.applyBrand);
        brandTV.setOnClickListener(this);
        priceTV.setOnClickListener(this);
        colorTV.setOnClickListener(this);
        sizeTV.setOnClickListener(this);

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        applyBrand.setOnClickListener(this);

        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
        }


        getBrands();
        getColors();

    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        brandTV.setTextColor(this.getResources().getColor(R.color.Black));
        priceTV.setTextColor(this.getResources().getColor(R.color.listback));
        colorTV.setTextColor(this.getResources().getColor(R.color.listback));
        sizeTV.setTextColor(this.getResources().getColor(R.color.listback));
//        getSupportFragmentManager().beginTransaction().add(R.id.filterContainer, new BrandFilter()).commit();
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            brandTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
            colorTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
            sizeTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
        } else {
            brandTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
            colorTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
            sizeTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i=null;
        switch (view.getId()) {
            case R.id.brandTV:
                brandTV.setTextColor(this.getResources().getColor(R.color.Black));
                priceTV.setTextColor(this.getResources().getColor(R.color.listback));
                colorTV.setTextColor(this.getResources().getColor(R.color.listback));
                sizeTV.setTextColor(this.getResources().getColor(R.color.listback));
                brands.setVisibility(View.VISIBLE);
                colors.setVisibility(View.GONE);
//                getSupportFragmentManager().beginTransaction().add(R.id.filterContainer, new BrandFilter()).commit();
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    brandTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    priceTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                } else {
                    brandTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    priceTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                }

                break;

            case R.id.priceTV:
                brandTV.setTextColor(this.getResources().getColor(R.color.listback));
                priceTV.setTextColor(this.getResources().getColor(R.color.Black));
                colorTV.setTextColor(this.getResources().getColor(R.color.listback));
                sizeTV.setTextColor(this.getResources().getColor(R.color.listback));
                getSupportFragmentManager().beginTransaction().add(R.id.filterContainer, new PriceFilter()).commit();
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    brandTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    colorTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                } else {
                    brandTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    colorTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                }
                break;

            case R.id.colorTV:
                brandTV.setTextColor(this.getResources().getColor(R.color.listback));
                priceTV.setTextColor(this.getResources().getColor(R.color.listback));
                colorTV.setTextColor(this.getResources().getColor(R.color.Black));
                sizeTV.setTextColor(this.getResources().getColor(R.color.listback));
                brands.setVisibility(View.GONE);
                colors.setVisibility(View.VISIBLE);
//                getSupportFragmentManager().beginTransaction().add(R.id.filterContainer, new ColorFilter()).commit();
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    brandTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    sizeTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                } else {
                    brandTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                    sizeTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                }

                break;

            case R.id.sizeTV:
                brandTV.setTextColor(this.getResources().getColor(R.color.listback));
                priceTV.setTextColor(this.getResources().getColor(R.color.listback));
                colorTV.setTextColor(this.getResources().getColor(R.color.listback));
                sizeTV.setTextColor(this.getResources().getColor(R.color.Black));
                getSupportFragmentManager().beginTransaction().add(R.id.filterContainer, new SizeFilter()).commit();
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    brandTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                } else {
                    brandTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    priceTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    colorTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.app_color_white_bottom));
                    sizeTV.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.white_back));
                }

                break;

            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.cartBtn:
                i=new Intent(Filters.this, CartProduct.class);
                startActivity(i);
                break;

            case R.id.applyBrand:
                i=new Intent(Filters.this, Products_List.class);
                Log.e(MyConstants.TAG, "Brands "+brandsl);
                Log.e(MyConstants.TAG, "Colors "+colorl);
                i.putStringArrayListExtra("Brands", brandsl);
                i.putStringArrayListExtra("Colors", colorl);
                Log.e(MyConstants.TAG, "catgeory "+category_id);
                i.putExtra(MyConstants.Category_Id, category_id);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
        }
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getBrands(){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        BrandModel brandModel = new BrandModel(category_id);

        Call<BrandsResponse> call = api.get_brandscategory(getAuthToken(), brandModel);
        call.enqueue(new Callback<BrandsResponse>() {
            @Override
            public void onResponse(Call<BrandsResponse> call, Response<BrandsResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new BrandFilterAdapter(data, Filters.this);
                        brands.setAdapter(adapter);
                        brands.setLayoutManager(new LinearLayoutManager(Filters.this));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<BrandsResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getColors(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);


        BrandModel brandModel = new BrandModel(category_id);
        Call<FilterColorResponse> call = api.get_colorscategory(getAuthToken(), brandModel);
        call.enqueue(new Callback<FilterColorResponse>() {
            @Override
            public void onResponse(Call<FilterColorResponse> call, Response<FilterColorResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        dataColor = new ArrayList<>(response.body().getMessage().getData());
                        adapterColor = new ColorFilterAdapter(dataColor, Filters.this);
                        colors.setAdapter(adapterColor);
                        colors.setLayoutManager(new LinearLayoutManager(Filters.this));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<FilterColorResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    @Override
    public void getBrandsList(ArrayList<String> s) {
        brandsl = s;
    }

    @Override
    public void getColorsList(ArrayList<String> s) {
        colorl = s;
    }
}
