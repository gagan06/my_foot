package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.UpdateProfile;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.UserProfileResponse;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyProfileDetail extends AppCompatActivity implements View.OnClickListener {
    TextView toolbar_title, cartCount;
    ImageButton backPress, cartBtn;
    TextView nameError, emailError, phoneError, addressError, pincodeError, stateError, cityError;
    EditText nameET, emailET, phoneNumberET, addressET, pincodeET, stateET, cityET;
    Button saveBtn;
    View progress_dialog;
    String name, email, mobile_no;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_detail);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        nameError = findViewById(R.id.nameError);
        emailError = findViewById(R.id.emailError);
        phoneError = findViewById(R.id.phoneError);
        addressError = findViewById(R.id.addressError);
        pincodeError = findViewById(R.id.pincodeError);
        stateError = findViewById(R.id.stateError);
        cityError = findViewById(R.id.cityError);
        progress_dialog = findViewById(R.id.progress_dialog);
        nameET = findViewById(R.id.nameET);
        emailET = findViewById(R.id.emailET);
        phoneNumberET = findViewById(R.id.phoneNumberET);
        addressET = findViewById(R.id.addressET);
        pincodeET = findViewById(R.id.pincodeET);
        stateET = findViewById(R.id.stateET);
        cityET = findViewById(R.id.cityET);
        cartCount = findViewById(R.id.cartCount);

        saveBtn = findViewById(R.id.saveBtn);

        toolbar_title.setText("Account Detail");
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
            progress_dialog.setVisibility(View.GONE);
        }
        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        progress_dialog.setVisibility(View.VISIBLE);
        user_profile();
        saveddData();
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MyProfileDetail.this, Home_Screen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public void saveddData(){
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
            nameET.setText(sharedPreferences.getString(MyConstants.Username, ""));
            emailET.setText(sharedPreferences.getString(MyConstants.Email, ""));
            phoneNumberET.setText(sharedPreferences.getString(MyConstants.Phone, ""));
        }else{

        }
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(MyProfileDetail.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.saveBtn:
                if (isvalid()){
                    progress_dialog.setVisibility(View.VISIBLE);
                    update_user();
                }
                break;
        }
    }

    public boolean isvalid(){
        Boolean valid = true;

        name  = nameET.getText().toString();
        if (name.length() < 3){
            nameError.setText("Enter valid name");
            nameError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            nameError.setVisibility(View.GONE);
            valid = true;
        }

        email  = emailET.getText().toString();
        if (email.isEmpty()) {
            emailError.setText("Email is required");
            emailError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            emailError.setVisibility(View.GONE);
            valid = true;
        }

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!TextUtils.isEmpty(email)) {
            if (!email.matches(emailPattern)) {
                emailError.setText("Invalid email");
                emailError.setVisibility(View.VISIBLE);
                valid = false;
            }else{
                emailError.setVisibility(View.GONE);
                valid = true;
            }
        }

        mobile_no  = phoneNumberET.getText().toString();
        if (mobile_no.length() < 10){
            phoneError.setText("Enter valid phone number");
            phoneError.setVisibility(View.VISIBLE);
            valid = false;
        }else{

            if (mobile_no.substring(0, 1).matches("0")){
                phoneError.setText("Enter valid phone number");
                phoneError.setVisibility(View.VISIBLE);
                valid = false;
            }else{
                phoneError.setVisibility(View.GONE);
                valid = true;
            }
        }
        return  valid;
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void user_profile(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<UserProfileResponse> call = api.user_profile(getAuthToken());
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        nameET.setText(response.body().getMessage().getData().getName());
                        emailET.setText(response.body().getMessage().getData().getEmail());
                        phoneNumberET.setText(response.body().getMessage().getData().getMobileNo());
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.Email, response.body().getMessage().getData().getEmail());
                        editor.putString(MyConstants.Phone, response.body().getMessage().getData().getMobileNo());
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.apply();
                        if (!response.body().getMessage().getData().getProvider().equalsIgnoreCase("normal")){
                            emailET.setEnabled(false);
                            emailET.setClickable(false);
                        }
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        saveBtn.setClickable(false);
                    }
                }else{
                    //server error
                    saveBtn.setClickable(false);
                }

            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void update_user(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        UpdateProfile updateProfile = new UpdateProfile(name, email, mobile_no);
        Call<UserProfileResponse> call = api.update_user_profile(getAuthToken(), updateProfile);
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        nameET.setText(response.body().getMessage().getData().getName());
                        emailET.setText(response.body().getMessage().getData().getEmail());
                        phoneNumberET.setText(response.body().getMessage().getData().getMobileNo());
                        onBackPressed();

                    }else
                    {
                        if (response.body().getMessage().getErrors().getEmail() != null){
                            new FancyAlertDialog.Builder(MyProfileDetail.this)
                                    .setTitle("Registration Failed")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Email is already registered with us")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                        if (response.body().getMessage().getErrors().getMobileNo() != null){
                            new FancyAlertDialog.Builder(MyProfileDetail.this)
                                    .setTitle("Registration Failed")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Mobile number is already registered with us")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                    }
                }else{
                    //server error
                    new FancyAlertDialog.Builder(MyProfileDetail.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    update_user();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(MyProfileDetail.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                update_user();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }


}
