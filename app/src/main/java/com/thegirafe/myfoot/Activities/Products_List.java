package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chrisplus.rootmanager.RootManager;
import com.google.gson.Gson;
import com.thegirafe.myfoot.Adapter.ProductFilterListAdapter;
import com.thegirafe.myfoot.Adapter.ProductGridAdapter;
import com.thegirafe.myfoot.Adapter.ProductListAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Cart.ProductList;
import com.thegirafe.myfoot.Filters.FiltersData;
import com.thegirafe.myfoot.Filters.FiltersResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Guest.GuestUserModelCategory;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.FilterModel;
import com.thegirafe.myfoot.PostModel.FilterModelGuest;
import com.thegirafe.myfoot.PostModel.PopularityModelGuest;
import com.thegirafe.myfoot.PostModel.PostCatgeory;
import com.thegirafe.myfoot.PostModel.PriceModel;
import com.thegirafe.myfoot.PostModel.PriceModelGuest;
import com.thegirafe.myfoot.Products.CategoryProductData;
import com.thegirafe.myfoot.Products.CategoryProductResponse;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.SATECITY.StateData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Products_List extends AppCompatActivity implements View.OnClickListener {
    RecyclerView productRecycler, productInListRecycler;
    ImageButton backPress, cartBtn, listLayoutBtn, gridLayoutBtn;
    TextView toolbar_title;
    String sampleUrl = "http://www.dressterra.uk/images/offer/uk/111/3/19274706433/19274706433-thumb.jpg";
    ArrayList<CategoryProductData> productData = new ArrayList<>();
    ArrayList<FiltersData> productDataFilters = new ArrayList<>();
    ProductListAdapter adapter;
    LinearLayout sortItemBtn, filterItemBtn;
    Dialog sortItemDialog = null;
    View progress_dialog;
    String category_id;
    ArrayList<String> brand_id, color_id;
    Intent new_intent;
    SharedPreferences sharedPreferences;
    RelativeLayout main;
    View server_error, no_internet_layout;
    Button retryBtn;
    ProductFilterListAdapter adapterFilter;
    TextView cartCount;
    View no_search_found;
    Button homeBtn;
    TextView txtdesc;
    ArrayList<CategoryProductData> contactListFiltered;
    ArrayList<String> names = new ArrayList<>();
    EditText search_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_products__list);

        RootManager.getInstance().isProcessRunning("screen record");

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        new_intent = getIntent();
        search_product = findViewById(R.id.search_product);
        productRecycler = findViewById(R.id.productRecycler);
        productInListRecycler = findViewById(R.id.productInListRecycler);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        toolbar_title = findViewById(R.id.toolbar_title);
        sortItemBtn = findViewById(R.id.sortItemBtn);
        filterItemBtn = findViewById(R.id.filterItemBtn);
        progress_dialog = findViewById(R.id.progress_dialog);
        listLayoutBtn = findViewById(R.id.listLayoutBtn);
        gridLayoutBtn = findViewById(R.id.gridLayoutBtn);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        no_search_found = findViewById(R.id.no_search_found);
        homeBtn = findViewById(R.id.homeBtn);
        txtdesc = findViewById(R.id.txtdesc);
        retryBtn = findViewById(R.id.retryBtn);
        main = findViewById(R.id.main);
        cartCount = findViewById(R.id.cartCount);
        sortItemDialog = new Dialog(this);

        toolbar_title.setText("Product List");
        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        sortItemBtn.setOnClickListener(this);
        filterItemBtn.setOnClickListener(this);
        listLayoutBtn.setOnClickListener(this);
        gridLayoutBtn.setOnClickListener(this);
        retryBtn.setOnClickListener(this);



        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            if (new_intent.getStringArrayListExtra("Brands") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilterGuest();
                getGuestCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else if(new_intent.getStringArrayListExtra("Colors") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilterGuest();
                getGuestCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else{
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                progress_dialog.setVisibility(View.VISIBLE);

                getDataGuest();
                getGuestCartCount();

            }
        }else{
            if (new_intent.getStringArrayListExtra("Brands") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilter();
                getCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else if(new_intent.getStringArrayListExtra("Colors") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilter();
                getCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else{
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                progress_dialog.setVisibility(View.VISIBLE);

                getData();
                getCartCount();

            }
        }

        search_product.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e(MyConstants.TAG, "Text " + s);
                if (new_intent.getStringArrayListExtra("Brands") != null) {
                    adapterFilter.getFilter().filter(s);
                }else if(new_intent.getStringArrayListExtra("Colors") != null){
                    adapterFilter.getFilter().filter(s);
                }else{
                    adapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }



    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void getData() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "category_id "+category_id);

        PostCatgeory postCatgeory = new PostCatgeory(category_id);
        Call<CategoryProductResponse> call = api.getcategoryproduct(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        search_product.setClickable(true);
                        search_product.setEnabled(false);
                        main.setVisibility(View.VISIBLE);
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        search_product.setClickable(true);
                        search_product.setEnabled(true);
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                    }else
                    {
                        no_search_found.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                        txtdesc.setText("We can't find any item matching your category");
                        homeBtn.setText("GO BACK TO HOME");
                        homeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                onBackPressed();
                                finish();
                            }
                        });
                        search_product.setClickable(false);
                        search_product.setEnabled(false);
                        Log.e(MyConstants.TAG, "error"+new Gson().toJson(response));

                    }
                } else {
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                    search_product.setClickable(true);
                    search_product.setEnabled(false);
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
                search_product.setClickable(true);
                search_product.setEnabled(false);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            if (new_intent.getStringArrayListExtra("Brands") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilterGuest();
                getGuestCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else if(new_intent.getStringArrayListExtra("Colors") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilterGuest();
                getGuestCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else{
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                progress_dialog.setVisibility(View.VISIBLE);

                getDataGuest();
                getGuestCartCount();

            }
        }else{
            if (new_intent.getStringArrayListExtra("Brands") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilter();
                getCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else if(new_intent.getStringArrayListExtra("Colors") != null){
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                brand_id = new_intent.getStringArrayListExtra("Brands");
                color_id = new_intent.getStringArrayListExtra("Colors");
                progress_dialog.setVisibility(View.VISIBLE);
                getDataFilter();
                getCartCount();
                Log.e(MyConstants.TAG, "Brands "+brand_id);
            }else{
                category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                progress_dialog.setVisibility(View.VISIBLE);

                getData();
                getCartCount();

            }
        }

    }

    public void getDataLinear() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        PostCatgeory postCatgeory = new PostCatgeory(category_id);
        Call<CategoryProductResponse> call = api.getcategoryproduct(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new LinearLayoutManager(Products_List.this));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
//                        productRecycler.setAdapter(adapter);
//                        ProductGridAdapter adapter = new ProductGridAdapter(productData, Products_List.this);
//                        productInListRecycler.setLayoutManager(new LinearLayoutManager(Products_List.this));
//                        productInListRecycler.setAdapter(adapter);

                    }
                } else {
                    //server error

                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(MyConstants.Email, "") + ":" + sharedPreferences.getString(MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;

            case R.id.cartBtn:
                i = new Intent(Products_List.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.listLayoutBtn:
                listLayoutBtn.setVisibility(View.GONE);
                gridLayoutBtn.setVisibility(View.VISIBLE);
                getDataLinear();
                break;

            case R.id.gridLayoutBtn:
                productRecycler.setVisibility(View.VISIBLE);
                productInListRecycler.setVisibility(View.GONE);
                listLayoutBtn.setVisibility(View.VISIBLE);
                gridLayoutBtn.setVisibility(View.GONE);
                getData();
                break;


            case R.id.sortItemBtn:
                sortItems();
                break;

            case R.id.filterItemBtn:
                i = new Intent(getApplicationContext(), Filters.class);
                i.putExtra(MyConstants.Category_Id, category_id);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.sortByWhatsNew:
                closeDialog();
                break;

            case R.id.sortByHighToLow:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    hightolowGuest();
                }else{
                    hightolow();
                }
                closeDialog();
                break;

            case R.id.sortByLowToHigh:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    lowtohighGuest();
                }else{
                    lowtohigh();
                }
                closeDialog();
                break;

            case R.id.sortByPopularity:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    popularityGuest();
                }else{
                    popularity();
                }
                closeDialog();
                break;

            case R.id.cancleDialogLayout:
                closeDialog();
                break;

            case R.id.retryBtn:
                no_internet_layout.setVisibility(View.GONE);
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    if (new_intent.getStringArrayListExtra("Brands") != null){
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                        brand_id = new_intent.getStringArrayListExtra("Brands");
                        color_id = new_intent.getStringArrayListExtra("Colors");
                        progress_dialog.setVisibility(View.VISIBLE);
                        getDataFilterGuest();
                        getGuestCartCount();
                        Log.e(MyConstants.TAG, "Brands "+brand_id);
                    }else if(new_intent.getStringArrayListExtra("Colors") != null){
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                        brand_id = new_intent.getStringArrayListExtra("Brands");
                        color_id = new_intent.getStringArrayListExtra("Colors");
                        progress_dialog.setVisibility(View.VISIBLE);
                        getDataFilterGuest();
                        getGuestCartCount();
                        Log.e(MyConstants.TAG, "Brands "+brand_id);
                    }else{
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                        Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                        progress_dialog.setVisibility(View.VISIBLE);

                        getDataGuest();
                        getGuestCartCount();

                    }
                }else{
                    if (new_intent.getStringArrayListExtra("Brands") != null){
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                        brand_id = new_intent.getStringArrayListExtra("Brands");
                        color_id = new_intent.getStringArrayListExtra("Colors");
                        progress_dialog.setVisibility(View.VISIBLE);
                        getDataFilter();
                        getCartCount();
                        Log.e(MyConstants.TAG, "Brands "+brand_id);
                    }else if(new_intent.getStringArrayListExtra("Colors") != null){
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);
                        brand_id = new_intent.getStringArrayListExtra("Brands");
                        color_id = new_intent.getStringArrayListExtra("Colors");
                        progress_dialog.setVisibility(View.VISIBLE);
                        getDataFilter();
                        getCartCount();
                        Log.e(MyConstants.TAG, "Brands "+brand_id);
                    }else{
                        category_id = new_intent.getStringExtra(MyConstants.Category_Id);

                        Log.e(MyConstants.TAG, "Brands "+new_intent.getStringArrayListExtra("Brands"));
                        progress_dialog.setVisibility(View.VISIBLE);

                        getData();
                        getCartCount();

                    }
                }
                break;
        }
    }

    public void sortItems() {
        TextView sortByWhatsNew, sortByHighToLow, sortByLowToHigh, sortByPopularity, sortByDiscount, sortByDelTime;
        RelativeLayout cancleDialogLayout;

        sortItemDialog.setCancelable(true);
        sortItemDialog.setContentView(R.layout.dialog_item_sort_list);

        sortByHighToLow = sortItemDialog.findViewById(R.id.sortByHighToLow);
        sortByLowToHigh = sortItemDialog.findViewById(R.id.sortByLowToHigh);
        sortByPopularity = sortItemDialog.findViewById(R.id.sortByPopularity);
        cancleDialogLayout = sortItemDialog.findViewById(R.id.cancleDialogLayout);

        sortByHighToLow.setOnClickListener(this);
        sortByLowToHigh.setOnClickListener(this);
        sortByPopularity.setOnClickListener(this);
        cancleDialogLayout.setOnClickListener(this);


        Window window = sortItemDialog.getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
//        layoutParams.gravity = Gravity.CENTER;
        layoutParams.x = 0;
        layoutParams.y = 100;
        window.setAttributes(layoutParams);

//        fullImageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sortItemDialog.show();
    }

    public void closeDialog() {
        if (sortItemDialog.isShowing()) {
            sortItemDialog.dismiss();
        }
    }

    public void lowtohigh(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        PriceModel postCatgeory = new PriceModel(category_id, "0");
        Call<CategoryProductResponse> call = api.price(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "SUccesds"+"low");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void lowtohighGuest(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        PriceModelGuest postCatgeory = new PriceModelGuest(category_id, "0", sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CategoryProductResponse> call = api.priceGuest(postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "SUccesds");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void hightolow(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        PriceModel postCatgeory = new PriceModel(category_id, "1");
        Call<CategoryProductResponse> call = api.price(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "SUccesds"+"low");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void hightolowGuest(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "guyest"+category_id);

        PriceModelGuest postCatgeory = new PriceModelGuest(category_id, "1", sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CategoryProductResponse> call = api.priceGuest(postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void popularity() {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        PostCatgeory postCatgeory = new PostCatgeory(category_id);
        Call<CategoryProductResponse> call = api.popularity(getAuthToken(), postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "SUccesds");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void popularityGuest() {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        PopularityModelGuest postCatgeory = new PopularityModelGuest(category_id, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CategoryProductResponse> call = api.popularity_guest(postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "SUccesds");
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public void getResultSuccess(String dec, String title, final String buttonTxt) {
        TextView dialog_dec, dialogTitle, Btn;
        final Dialog successDialog = new Dialog(Products_List.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialogTitle.setText(title);
        dialog_dec.setText(dec);
        Btn.setText(buttonTxt);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        successDialog.show();

    }

    public void getDataFilter() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Gson gson = new Gson();
        String brands_list = null;
        String color_list = null;
        if (brand_id.isEmpty()){
            brands_list = "";
        }else{
            brands_list =  gson.toJson(brand_id);
        }
        if (color_id.isEmpty()){
            color_list = "";
        }else{
            color_list =  gson.toJson(color_id);
        }
        Log.e(MyConstants.TAG, "Response post "+brands_list+" color "+color_list);
        FilterModel filterModel = new FilterModel(category_id, brand_id, color_id);
        Call<FiltersResponse> call = api.filters(getAuthToken(), filterModel);
        call.enqueue(new Callback<FiltersResponse>() {
            @Override
            public void onResponse(Call<FiltersResponse> call, Response<FiltersResponse> response) {
                progress_dialog.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        search_product.setClickable(true);
                        search_product.setEnabled(true);
                        main.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        productDataFilters = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapterFilter = new ProductFilterListAdapter(productDataFilters, Products_List.this);
                        productRecycler.setAdapter(adapterFilter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                        search_product.setClickable(true);
                        search_product.setEnabled(true);

                    }else
                    {
                        no_search_found.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                        txtdesc.setText("We can't find any item matching your filters");
                        homeBtn.setText("RESET FILTERS");
                        homeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                no_search_found.setVisibility(View.GONE);
                                getData();
                            }
                        });
                        search_product.setClickable(false);
                        search_product.setEnabled(false);

                    }
                } else {
                    //server error

                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                    search_product.setClickable(false);
                    search_product.setEnabled(false);
                }
            }

            @Override
            public void onFailure(Call<FiltersResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
                search_product.setClickable(false);
                search_product.setEnabled(false);

            }
        });
    }

    public void getDataFilterGuest() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Gson gson = new Gson();
        String brands_list = null;
        String color_list = null;
        if (brand_id.isEmpty()){
            brands_list = "";
        }else{
            brands_list =  gson.toJson(brand_id);
        }
        if (color_id.isEmpty()){
            color_list = "";
        }else{
            color_list =  gson.toJson(color_id);
        }
        Log.e(MyConstants.TAG, "Response post "+brands_list+" color "+color_list);
        FilterModelGuest filterModel = new FilterModelGuest(category_id, brand_id, color_id, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<FiltersResponse> call = api.filters_guest(getAuthToken(), filterModel);
        call.enqueue(new Callback<FiltersResponse>() {
            @Override
            public void onResponse(Call<FiltersResponse> call, Response<FiltersResponse> response) {
                progress_dialog.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "SUccesds");
                        productDataFilters = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapterFilter = new ProductFilterListAdapter(productDataFilters, Products_List.this);
                        productRecycler.setAdapter(adapterFilter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                        search_product.setClickable(true);
                        search_product.setEnabled(true);

                    }else
                    {
                        no_search_found.setVisibility(View.VISIBLE);
                        main.setVisibility(View.GONE);
                        txtdesc.setText("We can't find any item matching your filters");
                        homeBtn.setText("RESET FILTERS");
                        homeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                no_search_found.setVisibility(View.GONE);
                                getDataGuest();
                            }
                        });

                    }
                } else {
                    //server error

                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FiltersResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, "response "+t.getMessage());
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);

            }
        });
    }

    public void getDataGuest() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "category_id "+sharedPreferences.getString(MyConstants.AccessToken, ""));

        GuestUserModelCategory postCatgeory = new GuestUserModelCategory(sharedPreferences.getString(MyConstants.AccessToken, ""), category_id);
        Call<CategoryProductResponse> call = api.getcategoryproductguest(postCatgeory);
        call.enqueue(new Callback<CategoryProductResponse>() {
            @Override
            public void onResponse(Call<CategoryProductResponse> call, Response<CategoryProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        main.setVisibility(View.VISIBLE);
                        productData = new ArrayList<>(response.body().getMessage().getData());
                        Log.e(MyConstants.TAG, "SUccesds"+productData.size());
                        adapter = new ProductListAdapter(productData, Products_List.this);
                        productRecycler.setAdapter(adapter);
                        productRecycler.setLayoutManager(new GridLayoutManager(Products_List.this, 2));
                        for (int i= 0; i< productData.size(); i++){
                            names.add(productData.get(i).getProductName());
                        }
                        search_product.setClickable(true);
                        search_product.setEnabled(true);
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
//                        productRecycler.setAdapter(adapter);
//                        ProductGridAdapter adapter = new ProductGridAdapter(productData, Products_List.this);
//                        productInListRecycler.setLayoutManager(new LinearLayoutManager(Products_List.this));
//                        productInListRecycler.setAdapter(adapter);

                    }
                } else {
                    //server error

                    server_error.setVisibility(View.VISIBLE);
                    main.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<CategoryProductResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                main.setVisibility(View.GONE);
            }
        });
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


}

