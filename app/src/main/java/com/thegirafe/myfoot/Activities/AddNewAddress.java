package com.thegirafe.myfoot.Activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Adapter.CartListAdapter;
import com.thegirafe.myfoot.Address.StoreShippingAddressMessage;
import com.thegirafe.myfoot.Address.StoreShippingAddressResponse;
import com.thegirafe.myfoot.Cart.AddCartResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Cart.MyCartResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.AddCartModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.AddAddressModel;
import com.thegirafe.myfoot.PostModel.CityModel;
import com.thegirafe.myfoot.PostModel.StateModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.SATECITY.CityData;
import com.thegirafe.myfoot.SATECITY.CityResponse;
import com.thegirafe.myfoot.SATECITY.StateData;
import com.thegirafe.myfoot.SATECITY.StateResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddNewAddress extends AppCompatActivity implements View.OnClickListener {
    TextView toolbar_title, cartCount;
    ImageButton backPress,cartBtn;
    AutoCompleteTextView autoCompleteTextView1, DistrictET;
    SharedPreferences sharedPreferences;
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> city = new ArrayList<>();
    ArrayList<StateData> stateData;
    String address, cit, state, pincode;
    EditText address1ET, address2ET, pincodeET, userNameET, phoneET, landMarkET;
    Button submit;
    String name, phone = null, landmark;
    Intent intent;
    String type, address_id, billing_id, item, amount;
    RelativeLayout cart;
    View progress_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();

        type = intent.getStringExtra("address");
        address_id = intent.getStringExtra("Address_id");
        billing_id = intent.getStringExtra("Billing_id");
        item = intent.getStringExtra("Item");
        amount = intent.getStringExtra("Amount");
        progress_dialog = findViewById(R.id.progress_dialog);
        toolbar_title = findViewById(R.id.toolbar_title);
        cart = findViewById(R.id.cart);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        cartCount = findViewById(R.id.cartCount);
        autoCompleteTextView1 = (AutoCompleteTextView) findViewById(R.id.stateET);
        DistrictET = (AutoCompleteTextView) findViewById(R.id.DistrictET);
        address1ET = (EditText) findViewById(R.id.address1ET);
        address2ET = (EditText) findViewById(R.id.address2ET);
        pincodeET = (EditText) findViewById(R.id.pincodeET);
        userNameET = (EditText) findViewById(R.id.userNameET);
        landMarkET = (EditText) findViewById(R.id.landMarkET);
        phoneET = (EditText) findViewById(R.id.phoneET);
        submit = (Button) findViewById(R.id.submit);
        getstates();
        cart.setVisibility(View.GONE);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
        }

        if (type.equalsIgnoreCase("billing")){
            toolbar_title.setText("Billing Address");
        }else{
            toolbar_title.setText("Delivery Address");
        }



        DistrictET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (state == null){
                    DistrictET.setError("Select state first");
                    DistrictET.setEnabled(false);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        toolbar_title.setText("Delivery Address");
        submit.setOnClickListener(this);
        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


    public void getstates() {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        StateModel stateModel = new StateModel("101");

        Call<StateResponse> call = api.get_states(getAuthToken(), stateModel);
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        stateData = new ArrayList<>(response.body().getMessage().getData());
                        for (int i=0; i<stateData.size(); i++){
                            states.add(stateData.get(i).getName());
                        }
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddNewAddress.this ,R.layout.simple_list_item,states);
                        autoCompleteTextView1.setAdapter(adapter);
                        autoCompleteTextView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                String itemName = adapter.getItem(position);

                                for (StateData pojo : stateData) {
                                    if (pojo.getName().equals(itemName)) {
                                        String state_id = pojo.getId().toString(); // This is the correct ID
                                        Log.e(MyConstants.TAG, "State "+state_id);
                                        state = state_id;
                                        DistrictET.setEnabled(true);
                                        getcities(state_id);
                                        break; // No need to keep looping once you found it.
                                    }
                                }
                            }
                        });


                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    new FancyAlertDialog.Builder(AddNewAddress.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    getstates();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(AddNewAddress.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                getstates();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void getResultSuccess(String dec, String title, String btn) {
        final TextView dialog_dec, dialogTitle;
        ImageButton Btn;
        final  ImageButton cancelBtn;
        final Dialog successDialog = new Dialog(AddNewAddress.this);
        successDialog.setContentView(R.layout.error_popup);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.errorDec);
        dialogTitle = successDialog.findViewById(R.id.errorTitle);
        Btn = successDialog.findViewById(R.id.cancelBtn);
        dialogTitle.setText(title);
        dialog_dec.setText(dec);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });

        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

    public void getcities(String id) {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        CityModel cityModel = new CityModel(id);

        Call<CityResponse> call = api.get_city(getAuthToken(), cityModel);
        call.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        final ArrayList<CityData> cityData = new ArrayList<>(response.body().getMessage().getData());
                        for (int i=0; i<cityData.size(); i++){
                            city.add(cityData.get(i).getCityName());
                        }
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddNewAddress.this ,R.layout.simple_list_item,city);
                        DistrictET.setAdapter(adapter);
                        DistrictET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                String itemName = adapter.getItem(position);

                                for (CityData pojo : cityData) {
                                    if (pojo.getCityName().equals(itemName)) {
                                        String city = pojo.getId().toString(); // This is the correct ID
                                        Log.e(MyConstants.TAG, "city "+city);
                                        cit = city;
                                        break; // No need to keep looping once you found it.
                                    }else{
                                        DistrictET.setError("We are not available in this city");
                                    }
                                }
                            }
                        });

                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                    getResultSuccess("Something went wrong at our end.. Please be patient...", "OOPS", "OK");
                }

            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                getResultSuccess("No Internet Connection", "Connectivity Loss", "OK");
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    @Override
    public void onBackPressed() {
        Intent i =new Intent(AddNewAddress.this, SelectDeliveryAddress.class);
        i.putExtra("Address_id", address_id);
        i.putExtra("Billing_id", billing_id);
        i.putExtra("Item", item);
        i.putExtra("Amount", amount);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i=null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.submit:
                if (isValid()){
                    if (type.equalsIgnoreCase("billing")){
                        progress_dialog.setVisibility(View.VISIBLE);
                        addBillingAddress();
                    }else{
                        progress_dialog.setVisibility(View.VISIBLE);
                        addAddress();
                    }
                }
                break;
            case R.id.cartBtn:
                onBackPressed();
                finish();
                break;
        }
    }

    public boolean isValid(){
        boolean valid= true;

        address = address1ET.getText().toString();
        if (address.length() == 0){
            address1ET.setError("Address is required");
            valid = false;
        }else if(address.length() <5){
            address1ET.setError("Invalid Address");
            valid = false;
        }

        pincode = pincodeET.getText().toString();
        if (pincode.length() < 6) {
            pincodeET.setError("Valid pincode is required");
            valid = false;
        }

        name = userNameET.getText().toString();
        if (name.length() < 3) {
            userNameET.setError("Username length should be minimum 3");
            valid = false;
        }

        if (state == null){
            autoCompleteTextView1.setError("State is required");
            valid = false;
        }

        if (cit == null){
            DistrictET.setError("City is required");
            valid = false;
        }

        landmark = landMarkET.getText().toString();
        if (!landmark.isEmpty()){
            if (landmark.length() < 3){
                landMarkET.setError("Enter valid landmark");
                valid = false;
            }else{
                valid = true;
            }
        }

        phone = phoneET.getText().toString();
        if (phone.isEmpty()){
            phoneET.setError("Invalid mobile number");
            valid = false;
        }else if (phone.length() < 10){
            phoneET.setError("Invalid mobile number");
            valid = false;
        }

        return valid;
    }

    public void addAddress(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "color " +phone);

        AddAddressModel addCartModel = new AddAddressModel(address, state, cit, pincode, name, phone, landmark);
        Call<StoreShippingAddressResponse> call = api.store_shippingaddress(getAuthToken(), addCartModel);
        call.enqueue(new Callback<StoreShippingAddressResponse>() {
            @Override
            public void onResponse(Call<StoreShippingAddressResponse> call, Response<StoreShippingAddressResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(AddNewAddress.this)
                                .setTitle("Address Added successfully")
                                .setBackgroundColor(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Address Added successfully.. Order Product")
                                .setPositiveBtnBackground(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_tag_faces_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        Intent i =new Intent(AddNewAddress.this, SelectDeliveryAddress.class);
                                        i.putExtra("Address_id", address_id);
                                        i.putExtra("Billing_id", billing_id);
                                        i.putExtra("Item", item);
                                        i.putExtra("Amount", amount);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        finish();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        Intent i =new Intent(AddNewAddress.this, SelectDeliveryAddress.class);
                                        i.putExtra("Address_id", address_id);
                                        i.putExtra("Billing_id", billing_id);
                                        i.putExtra("Item", item);
                                        i.putExtra("Amount", amount);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        startActivity(i);
                                        finish();
                                    }
                                })
                                .build();
                    } else {
                        new FancyAlertDialog.Builder(AddNewAddress.this)
                                .setTitle("Something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        addAddress();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(AddNewAddress.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addAddress();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<StoreShippingAddressResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
                new FancyAlertDialog.Builder(AddNewAddress.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addAddress();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }

    public void addBillingAddress(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "color " +phone);

        AddAddressModel addCartModel = new AddAddressModel(address, state, cit, pincode, name, phone, landmark);
        Call<StoreShippingAddressResponse> call = api.store_billingaddress(getAuthToken(), addCartModel);
        call.enqueue(new Callback<StoreShippingAddressResponse>() {
            @Override
            public void onResponse(Call<StoreShippingAddressResponse> call, Response<StoreShippingAddressResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        new FancyAlertDialog.Builder(AddNewAddress.this)
                                .setTitle("Address Added successfully")
                                .setBackgroundColor(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Address Added successfully.. Order Product")
                                .setPositiveBtnBackground(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_tag_faces_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        Intent i =new Intent(AddNewAddress.this, SelectDeliveryAddress.class);
                                        i.putExtra("Address_id", address_id);
                                        i.putExtra("Billing_id", billing_id);
                                        i.putExtra("Item", item);
                                        i.putExtra("Amount", amount);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        finish();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        Intent i =new Intent(AddNewAddress.this, SelectDeliveryAddress.class);
                                        i.putExtra("Address_id", address_id);
                                        i.putExtra("Billing_id", billing_id);
                                        i.putExtra("Item", item);
                                        i.putExtra("Amount", amount);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        startActivity(i);
                                        finish();
                                    }
                                })
                                .build();
                    } else {
                        new FancyAlertDialog.Builder(AddNewAddress.this)
                                .setTitle("Something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        addBillingAddress();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(AddNewAddress.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    addBillingAddress();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<StoreShippingAddressResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
                new FancyAlertDialog.Builder(AddNewAddress.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                addBillingAddress();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    public void getResultSuccess(String dec) {
        TextView dialog_dec, dialogTitle, Btn;
        final Dialog successDialog = new Dialog(AddNewAddress.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialog_dec.setText(dec);
        Btn.setText("Done");
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();


            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }


}
