package com.thegirafe.myfoot.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.ProfileImageResponse;
import com.thegirafe.myfoot.User.UserProfileResponse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAccount extends AppCompatActivity implements View.OnClickListener {
    TextView toolbar_title;
    ImageButton backPress, cartBtn;
    TextView nameTV, emailTV, cartCount;
    ImageButton editAccount;
    ImageView profileImage;
    TextView emailNotificationSwitch;
    LinearLayout logoutLayout, termsLayout, feedbackLayout, aboutLayout;
    SharedPreferences sharedPreferences;
    View progress_dialog;
    Dialog dialog;
    static final int CAPTURE_IMAGE_REQUEST = 3;
    private static final int SELECT_PHOTO = 100;
    String imageName, mCurrentPhotoPath, mStoragePath;
    String filePath = null;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        cartCount = findViewById(R.id.cartCount);
        nameTV = findViewById(R.id.nameTV);
        emailTV = findViewById(R.id.emailTV);
        editAccount = findViewById(R.id.editAccount);
        profileImage = findViewById(R.id.profileImage);
        emailNotificationSwitch = findViewById(R.id.emailNotificationSwitch);
        termsLayout = findViewById(R.id.termsLayout);
        feedbackLayout = findViewById(R.id.feedbackLayout);
        logoutLayout = findViewById(R.id.logoutLayout);
        progress_dialog = findViewById(R.id.progress_dialog);
        aboutLayout = findViewById(R.id.aboutLayout);
        toolbar_title.setText("My Account");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        editAccount.setOnClickListener(this);
        termsLayout.setOnClickListener(this);
        feedbackLayout.setOnClickListener(this);
        logoutLayout.setOnClickListener(this);
        aboutLayout.setOnClickListener(this);
        progress_dialog.setVisibility(View.VISIBLE);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
            user_profile();
            saveddData();
        }else{
            progress_dialog.setVisibility(View.GONE);
            getGuestCartCount();
        }

    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void saveddData(){
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
            if (sharedPreferences.getString(MyConstants.Provider, "").equals("")) {
            } else if(sharedPreferences.getString(MyConstants.Provider, "").equals("normal")){
                Glide.with(MyAccount.this)
                        .load(Uri.parse(MyConstants.Profile_Image + sharedPreferences.getString(MyConstants.ProfileImage, "")))
                        .into(profileImage);
            } else {
                Glide.with(MyAccount.this)
                        .load(sharedPreferences.getString(MyConstants.ProfileImage, ""))
                        .into(profileImage);
            }

            nameTV.setText(sharedPreferences.getString(MyConstants.Username, ""));
            emailTV.setText(sharedPreferences.getString(MyConstants.Email, ""));
        }else{

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                finish();
                break;
            case R.id.cartBtn:
                i = new Intent(MyAccount.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case R.id.editAccount:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    i = new Intent(MyAccount.this, Register_Screen.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    i = new Intent(MyAccount.this, MyProfileDetail.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
                break;
            case R.id.termsLayout:
                i = new Intent(MyAccount.this, TermsCondition.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.aboutLayout:
                i = new Intent(MyAccount.this, AboutUs.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.logoutLayout:
                i = new Intent(MyAccount.this, Login_Screen.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case R.id.feedbackLayout:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    i = new Intent(MyAccount.this, Register_Screen.class);
                    startActivity(i);
                }else{
                    i = new Intent(MyAccount.this, ResetPassword.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
                break;
            case R.id.profileImage:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    i = new Intent(MyAccount.this, Register_Screen.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }else{
                    Permissions.check(this, new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        "Camera and storage permissions are required to set your profile image", new Permissions.Options()
                                .setRationaleDialogTitle("Info"),
                        new PermissionHandler() {
                            @Override
                            public void onGranted() {
                                pickImage();
                            }

                            @Override
                            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
//                                Toast.makeText(context, "Camera+Storage Denied:\n" + Arrays.toString(deniedPermissions.toArray()),
//                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public boolean onBlocked(Context context, ArrayList<String> blockedList) {
//                                Toast.makeText(context, "Camera+Storage blocked:\n" + Arrays.toString(blockedList.toArray()),
//                                        Toast.LENGTH_SHORT).show();
                                return false;
                            }

                            @Override
                            public void onJustBlocked(Context context, ArrayList<String> justBlockedList,
                                                      ArrayList<String> deniedPermissions) {
//                                Toast.makeText(context, "Camera+Storage just blocked:\n" + Arrays.toString(deniedPermissions.toArray()),
//                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                }

                break;
        }
    }

    public void pickImage() {
        RelativeLayout gallery_select, camera_select;
        dialog = new Dialog(MyAccount.this);
        dialog.setContentView(R.layout.dialog_select_image);
//        cancel = ImageSelectDialog.findViewById(R.id.cancel);
        gallery_select = dialog.findViewById(R.id.selectGallery);
        camera_select = dialog.findViewById(R.id.selectCamera);

        camera_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera();
                dialog.dismiss();
            }
        });
        gallery_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    public void takePhotoFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(MyAccount.this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                imageName = photoFile.getName();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(getApplicationContext(), "com.thegirafe.myfoot.provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_REQUEST);
            }
        }

    }

    private void openGallery(){

        Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, SELECT_PHOTO);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */

        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    Log.e(MyConstants.TAG, "lENGHT "+data.getData().getPath().getBytes().toString());

                    if(selectedImage !=null){
                        filePath = getPath(selectedImage);

                        File f = new File(filePath);

                        imageName = f.getName();
//                        Log.e(MyConstants.TAG, "Path "+f.getAbsolutePath()+" name "+imageName+"Compressed Image "+compressImage(filePath)+f.length());
//                        String file = compressImage(filePath);
                        Log.e(MyConstants.TAG, "Compressed lntgh "+selectedImage.getPath().length() );
                        imageUpload(f);
                    }
                }
                break;

            case CAPTURE_IMAGE_REQUEST:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options o2 = new BitmapFactory.Options();
                    o2.inSampleSize = 8;
                    // Get the original bitmap from the filepath to which you want to change orientation
                    // fileName ist the filepath of the image
                    Bitmap cachedImage = BitmapFactory.decodeFile(mStoragePath, o2);
                    int rotate = getCameraPhotoOrientation(mStoragePath);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotate);
                    // Here you will get the image bitmap which has changed orientation
                    cachedImage = Bitmap.createBitmap(cachedImage, 0, 0, cachedImage.getWidth(), cachedImage.getHeight(), matrix, true);
//                        updateuserimage.setImageBitmap(cachedImage);

                    filePath = mStoragePath;

                    if (filePath != null) {

                        File imgFile = new File(filePath);
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());


                        String img = compressImage(filePath);
                        //Uploading to server
                        imageUpload(imgFile);
                    }
                }
                break;



        }
    }

    public static int getCameraPhotoOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }



    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        Log.e(MyConstants.TAG, "Size "+calculateInSampleSize(options, actualWidth, actualHeight));

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private String getPath(Uri contentUri) {
        Log.v("picUriGET", String.valueOf(contentUri));
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public String getAuthTokenImage() {
        byte[] data = new byte[0];
        try {
            data = ("gagan@gmail.com" + ":" + "pppppp").getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }



    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void user_profile(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<UserProfileResponse> call = api.user_profile(getAuthToken());
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        nameTV.setText(response.body().getMessage().getData().getName());
                        emailTV.setText(response.body().getMessage().getData().getEmail());
                        if (response.body().getMessage().getData().getProfileImage() != null) {
                            if (response.body().getMessage().getData().getProvider() == null){
                                Glide.with(MyAccount.this)
                                        .load(Uri.parse(MyConstants.Profile_Image + response.body().getMessage().getData().getProfileImage()))
                                        .into(profileImage);
                            }else if(response.body().getMessage().getData().getProvider().equalsIgnoreCase("google")){
                                Glide.with(MyAccount.this)
                                        .load(response.body().getMessage().getData().getProfileImage())
                                        .into(profileImage);
                                feedbackLayout.setVisibility(View.GONE);
                            }else if(response.body().getMessage().getData().getProvider().equalsIgnoreCase("facebook")){
                                Glide.with(MyAccount.this)
                                        .load(response.body().getMessage().getData().getProfileImage())
                                        .into(profileImage);
                                feedbackLayout.setVisibility(View.GONE);
                            }else{
                                Glide.with(MyAccount.this)
                                        .load(Uri.parse(MyConstants.Profile_Image + response.body().getMessage().getData().getProfileImage()))
                                        .into(profileImage);
                            }
                        }


                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.Email, response.body().getMessage().getData().getEmail());
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.putString(MyConstants.Phone, response.body().getMessage().getData().getMobileNo());
                        if (response.body().getMessage().getData().getProfileImage() != null){
                            editor.putString(MyConstants.ProfileImage, response.body().getMessage().getData().getProfileImage());
                        }
                        editor.apply();
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    new FancyAlertDialog.Builder(MyAccount.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    onBackPressed();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();

                }

            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(MyAccount.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                onBackPressed();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void imageUpload(final File image_path){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface= retrofit.create(ApiInterface.class);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), image_path);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("profile_image", image_path.getName(), requestFile);

        Call<ProfileImageResponse> call = apiInterface.update_profile_image(getAuthToken(), body);
        call.enqueue(new Callback<ProfileImageResponse>() {
            @Override
            public void onResponse(Call<ProfileImageResponse> call, Response<ProfileImageResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        Log.e(MyConstants.TAG, "Message "+response.body().getMessage().getSuccess());
                        user_profile();
                    }else {
                        if (response.body().getMessage().getErrors().getProfileImage() != null){
                            new FancyAlertDialog.Builder(MyAccount.this)
                                    .setTitle("Upload Failed")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage(response.body().getMessage().getErrors().getProfileImage())
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                    }
                }else {
                    new FancyAlertDialog.Builder(MyAccount.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    imageUpload(image_path);
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }
            }
            @Override
            public void onFailure(Call<ProfileImageResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(MyAccount.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                imageUpload(image_path);
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
