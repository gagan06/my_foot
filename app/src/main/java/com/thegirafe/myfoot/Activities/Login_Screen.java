package com.thegirafe.myfoot.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Guest.GuestUserResponse;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.FacebookModel;
import com.thegirafe.myfoot.PostModel.LoginModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.SocialResponse.FbResponse;
import com.thegirafe.myfoot.User.LoginResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login_Screen extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    public TextView forgot_password, signup, passwordError, emailError, guest;
    public LinearLayout facebook_login, google_login;
    private Button signin_button;
    public TextInputEditText signin_email, signin_password;
    public TextInputLayout email_layout, password__layout;
    private String Email, Password;
    View progress_dialog;
    SharedPreferences sharedPreferences, guestPref;
    SharedPreferences.Editor editor, guesteditor;
    String device_token, imageUrl;
    Intent intent;
    CallbackManager callbackManager;
    String fb_id;
    AccessToken accessToken;
    String accesstoken, access;
    String fb_name, fb_email, fb_image, fb_token;
    GoogleApiClient mGoogleSignInClient;
    private static final int REQ_CODE = 9001;

    Intent intent1;
    String google_name, google_email, google_image, google_token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        setContentView(R.layout.activity_login__screen);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        guestPref = getSharedPreferences(MyConstants.GuestPref, MODE_PRIVATE);

        facebook_login = findViewById(R.id.facebook_login);
        guest = findViewById(R.id.guest);
        google_login = findViewById(R.id.google_login);
        signup = findViewById(R.id.signup);
        forgot_password = findViewById(R.id.forgot_password);
        signin_button = findViewById(R.id.signin_button);
        signin_email = findViewById(R.id.signin_email);
        signin_password = findViewById(R.id.signin_password);
        email_layout = findViewById(R.id.email_layout);
        password__layout = findViewById(R.id.password__layout);
        progress_dialog = findViewById(R.id.progress_dialog);
        passwordError = findViewById(R.id.passwordError);
        emailError = findViewById(R.id.emailError);
        printHashKey();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signup.setOnClickListener(this);
        signin_button.setOnClickListener(this);
        facebook_login.setOnClickListener(this);
        google_login.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
        guest.setOnClickListener(this);

        checkForPermission();
    }

    //Hash Key for the app
    public void printHashKey() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.thegirafe.salonkey", PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.e(MyConstants.TAG, "KeyHash : " + Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.signin_button:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if (isValid()) {
                    progress_dialog.setVisibility(View.VISIBLE);
                    login();
                }
                break;

            case R.id.signup:
                i = new Intent(Login_Screen.this, Register_Screen.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
                break;

            case R.id.forgot_password:
                i = new Intent(Login_Screen.this, Forgot_Password.class);
                startActivity(i);
                overridePendingTransition(0, 0);
                break;

            case R.id.facebook_login:
                facebookLogin();
                break;

            case R.id.google_login:
                signIn(view);
                break;

            case R.id.guest:
                progress_dialog.setVisibility(View.VISIBLE);
                guestsignin();
                break;
        }
    }

    //Facebook Login
    public void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(Login_Screen.this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(MyConstants.TAG, "@@@onSuccess");
                accessToken = AccessToken.getCurrentAccessToken();
                Log.e(MyConstants.TAG, "Access Token: " + loginResult.getAccessToken().getToken());
                fb_token = loginResult.getAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e(MyConstants.TAG, "@@@response: " + accessToken);
                                try {
                                    Email = object.getString("email");
                                    fb_id = object.getString("id");
                                    imageUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    fb_name = object.getString("name");
                                    fb_email = object.getString("email");
                                    fb_image = object.getJSONObject("picture").getJSONObject("data").getString("url");
//                                    Log.e(MyConstants.TAG, ""+email+imageUrl+object.getString("name"));
                                    editor = sharedPreferences.edit();
                                    editor.putString("Email", Email);
                                    editor.putString("id", fb_id);
                                    editor.putString(MyConstants.ProfileImage, imageUrl);
                                    editor.commit();

                                    fblogin();

//                                    intent = new Intent(Sign_In_Screen.this, HomeActivity.class);
//                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e(MyConstants.TAG, "" + e.getMessage());
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name,email,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e(MyConstants.TAG, "@@@onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(MyConstants.TAG, "Facebook Login");
                Log.e(MyConstants.TAG, "@@@onError: " + error.getMessage());
            }
        });
    }

    public void fblogin() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Facebook " + fb_name + " " + fb_email + " " + fb_image + " " + fb_token);
        FacebookModel facebookModel = new FacebookModel(fb_name, fb_email, fb_image, fb_token);
        Call<FbResponse> call = api.fblogin(facebookModel);
        call.enqueue(new Callback<FbResponse>() {
            @Override
            public void onResponse(Call<FbResponse> call, Response<FbResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "success " + response.body().getMessage().getSuccess());
                        login_social(fb_email, fb_token);
                    } else {
                        Log.e(MyConstants.TAG, "Fb " + response.message());
                        if (response.body().getMessage().getSuccess().equalsIgnoreCase("Email Id already Exist")) {
                            new FancyAlertDialog.Builder(Login_Screen.this)
                                    .setTitle("Sorry, something went wrong")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("You are already signed in using this email id")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        } else {
                            new FancyAlertDialog.Builder(Login_Screen.this)
                                    .setTitle("Sorry, something went wrong")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                    }
                } else {
                    new FancyAlertDialog.Builder(Login_Screen.this)
                            .setTitle("Sorry, something went wrong")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<FbResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Login_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                guestsignin();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }


    public void login_social(final String em, String tk) {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface salonKeyApi = retrofit.create(ApiInterface.class);


        Log.e(MyConstants.TAG, "email and token" + em + "  " + tk);
        LoginModel signin = new LoginModel("gcm", device_token, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<LoginResponse> call = salonKeyApi.login(getSocialAuthToken(em, em), signin);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.e(MyConstants.TAG, "success " + response.message());
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "pASS " + "salonkey");
                        String pass = em;
                        editor.putString(MyConstants.Password, pass);
                        editor.putString(MyConstants.AccessToken, "");
                        editor.putString(MyConstants.ProfileImage, response.body().getMessage().getData().getProfileImage());
                        editor.putString(MyConstants.Id, response.body().getMessage().getData().getId().toString());
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.putString(MyConstants.Phone, response.body().getMessage().getData().getMobileNo());
                        editor.putString(MyConstants.Email, response.body().getMessage().getData().getEmail());
                        editor.putString(MyConstants.Provider, response.body().getMessage().getData().getProvider());
                        editor.apply();
                        Log.e(MyConstants.TAG, response.body().getMessage().getData().getId() + "  " + sharedPreferences.getString(MyConstants.Password, ""));
                        Intent intent = new Intent(Login_Screen.this, Home_Screen.class);
                        ;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else {
                        new FancyAlertDialog.Builder(Login_Screen.this)
                                .setTitle("Sorry, something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(Login_Screen.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, "Response salon " + t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Login_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public String getSocialAuthToken(String email, String password) {
        byte[] data = new byte[0];
        try {
            data = (email + ":" + password).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    //Sign In to Google+
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Network Error / Connection failure", Toast.LENGTH_SHORT).show();
    }

    private void signIn(View v) {
        Intent i = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
        startActivityForResult(i, REQ_CODE);
    }

    //Handle SignIn Result
    private void handleSignInResult(GoogleSignInResult result) {
        Log.v("GOOGLE", String.valueOf(result));
        progress_dialog.setVisibility(View.VISIBLE);
        Log.d(MyConstants.TAG, "handleSignInResult:" + result.isSuccess());
        if (mGoogleSignInClient != null && mGoogleSignInClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleSignInClient);
        }
        if (result.isSuccess()) {
            final GoogleSignInAccount account = result.getSignInAccount();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        String scope = "oauth2:" + Scopes.EMAIL + " " + Scopes.PROFILE;
                        accesstoken = GoogleAuthUtil.getToken(getApplicationContext(), account.getAccount(), scope, new Bundle());
                        Log.d(MyConstants.TAG, "accessToken:" + accesstoken); //accessToken:ya29.Gl...
                        google_token = accesstoken;
                        google_name = account.getGivenName();
                        google_email = account.getEmail();
                        google_image = String.valueOf(account.getPhotoUrl());
                        googlelogin();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (GoogleAuthException e) {
                        e.printStackTrace();
                    }
                }
            };
            AsyncTask.execute(runnable);


            editor = sharedPreferences.edit();
            editor.putString(MyConstants.Email, google_email);
            editor.putString(MyConstants.ProfileImage, imageUrl);
            editor.commit();
//
//            intent = new Intent(Sign_In_Screen.this, HomeActivity.class);
//            startActivity(intent);

        } else {
            // Signed out, show unauthenticated UI
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            progress_dialog.setVisibility(View.GONE);
        }
    }

    public void googlelogin() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "Google " + google_name + " " + google_email + " " + google_image + " " + google_token);
        FacebookModel facebookModel = new FacebookModel(google_name, google_email, google_image, google_token);
        Call<FbResponse> call = api.googlelogin(facebookModel);
        call.enqueue(new Callback<FbResponse>() {
            @Override
            public void onResponse(Call<FbResponse> call, Response<FbResponse> response) {

                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, "Staus " + response.body().getMessage().getSuccess());
                        login_social(google_email, google_token);
                    } else {
                        if (mGoogleSignInClient != null && mGoogleSignInClient.isConnected()) {
                            Auth.GoogleSignInApi.signOut(mGoogleSignInClient);
                        }
                        new FancyAlertDialog.Builder(Login_Screen.this)
                                .setTitle("Sorry, something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Email already registered with us")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        login();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    if (mGoogleSignInClient != null && mGoogleSignInClient.isConnected()) {
                        Auth.GoogleSignInApi.signOut(mGoogleSignInClient);
                    }
                    new FancyAlertDialog.Builder(Login_Screen.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    login();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }
            }

            @Override
            public void onFailure(Call<FbResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Login_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                googlelogin();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public boolean isValid() {
        Boolean valid = true;
        Email = signin_email.getText().toString().trim();
        Password = signin_password.getText().toString().trim();

        if (Email.isEmpty()) {
            emailError.setVisibility(View.VISIBLE);
            emailError.setText("Email is required");
            valid = false;
        } else {
            emailError.setVisibility(View.GONE);
        }
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!TextUtils.isEmpty(Email)) {
            if (!Email.matches(emailPattern)) {
                emailError.setVisibility(View.VISIBLE);
                emailError.setText("Enter valid email");
                valid = false;
            } else {
                emailError.setVisibility(View.GONE);
            }
        }
        if (!TextUtils.isEmpty(Password)) {
            if (Password.length() < 6) {
                passwordError.setVisibility(View.VISIBLE);
                passwordError.setText("Password must be of minimum 6 characters");
                valid = false;
            }
        }

        if (Password.length() < 6) {
            passwordError.setVisibility(View.VISIBLE);
            passwordError.setText("Password length should be minimum 6");
            valid = false;
        } else {
            passwordError.setVisibility(View.GONE);
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void login() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        LoginModel loginModel = new LoginModel("gcm", device_token, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<LoginResponse> call = api.login(getAuthToken(), loginModel);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        guesteditor = guestPref.edit();
                        guesteditor.putString(MyConstants.Id, response.body().getMessage().getData().getId().toString());
                        guesteditor.apply();
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.AccessToken, "");
                        editor.putString(MyConstants.Password, Password);
                        editor.putString(MyConstants.ProfileImage, response.body().getMessage().getData().getProfileImage());
                        editor.putString(MyConstants.Id, response.body().getMessage().getData().getId().toString());
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.putString(MyConstants.Phone, response.body().getMessage().getData().getMobileNo());
                        editor.putString(MyConstants.Email, response.body().getMessage().getData().getEmail());
                        editor.apply();
                        intent = new Intent(Login_Screen.this, Home_Screen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        new FancyAlertDialog.Builder(Login_Screen.this)
                                .setTitle("Login Failed")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Invalid Login Credentials")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(Login_Screen.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    login();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, "Response salon " + t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Login_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                login();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }

    public void checkForPermission() {
        Permissions.check(this, new String[]{android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.RECEIVE_SMS,
                        android.Manifest.permission.READ_SMS},
                "This app needs to read sms in order to get your mobile number verified.", new Permissions.Options()
                        .setRationaleDialogTitle("Info"),
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
//                        Toast.makeText(Sign_In_Screen.this , "Permission Granted",
//                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                    }

                    @Override
                    public boolean onBlocked(Context context, ArrayList<String> blockedList) {
                        return false;
                    }

                    @Override
                    public void onJustBlocked(Context context, ArrayList<String> justBlockedList,
                                              ArrayList<String> deniedPermissions) {
                        Toast.makeText(context, "Read SMS permission is bloacked you have to go to app settings in order to read your OTP" + Arrays.toString(deniedPermissions.toArray()),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (Email + ":" + Password).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }
    public void guestsignin() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equals("")) {
            Log.e(MyConstants.TAG, "Access " + GenerateRandomString.randomString(20));
            access = GenerateRandomString.randomString(20);
        } else {
            access = guestPref.getString(MyConstants.Id, "");
        }

        GuestUserModel loginModel = new GuestUserModel(access);
        Call<GuestUserResponse> call = api.register_guest(loginModel);
        call.enqueue(new Callback<GuestUserResponse>() {
            @Override
            public void onResponse(Call<GuestUserResponse> call, retrofit2.Response<GuestUserResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.AccessToken, access);
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.apply();
                        intent = new Intent(Login_Screen.this, Home_Screen.class);
                        ;
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        new FancyAlertDialog.Builder(Login_Screen.this)
                                .setTitle("Sorry, something went wrong")
                                .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                                .setNegativeBtnText("OK")
                                .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Try Again")
                                .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                .setAnimation(Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        login();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                    }
                                })
                                .build();
                    }
                } else {
                    new FancyAlertDialog.Builder(Login_Screen.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    login();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<GuestUserResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, "Response salon " + t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Login_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                guestsignin();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public static class GenerateRandomString {

        public static final String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static Random RANDOM = new Random();

        public static String randomString(int len) {
            StringBuilder sb = new StringBuilder(len);

            for (int i = 0; i < len; i++) {
                sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
            }

            return sb.toString();
        }

    }


}
