package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ResetPassModel;
import com.thegirafe.myfoot.PostModel.UpdateProfile;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.ResetPassResponse;
import com.thegirafe.myfoot.User.UserProfileResponse;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResetPassword extends AppCompatActivity implements View.OnClickListener {
    TextView toolbar_title, confirmPasswordError, newPasswordError, oldPasswordError, cartCount;
    ImageButton backPress,cartBtn;
    SharedPreferences sharedPreferences;
    String old, new_pass, confirm;
    Button submitBtn;
    View progress_dialog;
    SharedPreferences.Editor editor;
    EditText oldPasswordET, newPasswordET, confirmPasswordET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        oldPasswordET = findViewById(R.id.oldPasswordET);
        newPasswordET = findViewById(R.id.newPasswordET);
        confirmPasswordET = findViewById(R.id.confirmPasswordET);
        confirmPasswordError = findViewById(R.id.confirmPasswordError);
        newPasswordError = findViewById(R.id.newPasswordError);
        oldPasswordError = findViewById(R.id.oldPasswordError);
        progress_dialog = findViewById(R.id.progress_dialog);
        submitBtn = findViewById(R.id.submitBtn);
        cartCount = findViewById(R.id.cartCount);
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getCartCount();
        }else{
            getGuestCartCount();
        }

        toolbar_title.setText("Reset Password");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ResetPassword.this, Home_Screen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public boolean isValid(){
        Boolean valid = true;

        old = oldPasswordET.getText().toString();
        if (old.length() < 6){
            oldPasswordError.setText("Password length should be minimum 6");
            oldPasswordError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            oldPasswordError.setVisibility(View.GONE);
            valid = true;
        }

        new_pass = newPasswordET.getText().toString();
        if (new_pass.length() < 6){
            newPasswordError.setText("Password length should be minimum 6");
            newPasswordError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            newPasswordError.setVisibility(View.GONE);
            valid = true;
        }

        confirm = confirmPasswordET.getText().toString();
        if (confirm.length() < 6){
            confirmPasswordError.setText("Password length should be minimum 6");
            confirmPasswordError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            confirmPasswordError.setVisibility(View.GONE);
            valid = true;
        }

        if (!new_pass.equalsIgnoreCase(confirm)){
            confirmPasswordError.setText("new password and confirm password not matching");
            confirmPasswordError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            confirmPasswordError.setVisibility(View.GONE);
            valid = true;
        }

        return valid;
    }
    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i=new Intent(ResetPassword.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.submitBtn:
                if (isValid()){
                    progress_dialog.setVisibility(View.VISIBLE);
                    update_pass();
                }
                break;
        }
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


    public void update_pass(){
        oldPasswordError.setVisibility(View.GONE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ResetPassModel updateProfile = new ResetPassModel(old, confirm);
        Call<ResetPassResponse> call = api.change(getAuthToken(), updateProfile);
        call.enqueue(new Callback<ResetPassResponse>() {
            @Override
            public void onResponse(Call<ResetPassResponse> call, Response<ResetPassResponse> response) {

                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        progress_dialog.setVisibility(View.GONE);
                        new FancyAlertDialog.Builder(ResetPassword.this)
                                .setTitle("Password Updated successfully")
                                .setBackgroundColor(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setMessage("Password Updated successfully.. Explore Product")
                                .setPositiveBtnBackground(Color.parseColor("#4bae50"))  //Don't pass R.color.colorvalue
                                .setPositiveBtnText("Explore")
                                .setNegativeBtnText("OK")
                                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                                .isCancellable(true)
                                .setIcon(R.drawable.ic_tag_faces_black_24dp, Icon.Visible)
                                .OnPositiveClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        editor = sharedPreferences.edit();
                                        editor.putString(MyConstants.Password, confirm);
                                        editor.apply();
                                        onBackPressed();
                                    }
                                })
                                .OnNegativeClicked(new FancyAlertDialogListener() {
                                    @Override
                                    public void OnClick() {
                                        editor = sharedPreferences.edit();
                                        editor.putString(MyConstants.Password, confirm);
                                        editor.apply();
                                        onBackPressed();
                                    }
                                })
                                .build();
                    }else
                    {
                        progress_dialog.setVisibility(View.GONE);
                        oldPasswordError.setText(response.body().getMessage().getSuccess());
                        oldPasswordError.setVisibility(View.VISIBLE);
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    progress_dialog.setVisibility(View.GONE);
                    new FancyAlertDialog.Builder(ResetPassword.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    update_pass();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<ResetPassResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(ResetPassword.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                update_pass();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();
            }
        });
    }

    public void getResultSuccess1(String dec, String title, String btn) {
        final TextView dialog_dec, dialogTitle, Btn;
        final Dialog successDialog = new Dialog(ResetPassword.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialogTitle.setText(title);
        dialog_dec.setText(dec);
        Btn.setText(btn);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });

        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

}
