package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thegirafe.myfoot.Adapter.CouponAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.CouponInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Payment;
import com.thegirafe.myfoot.PaymentMethods.PaymentMethodResponse;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.coupons.CheckCouponPost;
import com.thegirafe.myfoot.coupons.CheckCouponResponse;
import com.thegirafe.myfoot.coupons.CouponsData;
import com.thegirafe.myfoot.coupons.CouponsResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Coupons extends AppCompatActivity implements View.OnClickListener, CouponInterface {

    RecyclerView coupons_recycler;
    CouponAdapter couponAdapter;
    SharedPreferences sharedPreferences;
    View progress_dialog;
    Intent intent;
    String amount;
    ArrayList<CouponsData> couponsData;
    TextView toolbar_title, cartCount;
    ImageButton backPress;
    SharedPreferences.Editor editor;
    Button apply;
    TextView coupon_used;
    RelativeLayout cart;
    Button homeBtn, retryBtn;
    View no_coupons, server_error, no_internet_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();
        amount = intent.getStringExtra("Amount");

        coupons_recycler = findViewById(R.id.coupons_recycler);
        homeBtn = findViewById(R.id.homeBtn);
        retryBtn = findViewById(R.id.retryBtn);
        no_coupons = findViewById(R.id.no_coupon);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        progress_dialog = findViewById(R.id.progress_dialog);
        cartCount = findViewById(R.id.cartCount);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        apply = findViewById(R.id.apply);
        toolbar_title.setText("Coupons");
        coupon_used = findViewById(R.id.coupon_used);
        cart = findViewById(R.id.cart);
        apply.setVisibility(View.GONE);

        backPress.setOnClickListener(this);
        cartCount.setOnClickListener(this);
        apply.setOnClickListener(this);
        homeBtn.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
        getCartCount();

        getcoupons();
    }

    public void getcoupons() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<CouponsResponse> call = api.get_coupons(getAuthToken());
        call.enqueue(new Callback<CouponsResponse>() {
            @Override
            public void onResponse(Call<CouponsResponse> call, Response<CouponsResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        apply.setVisibility(View.VISIBLE);
                        couponsData = new ArrayList<>(response.body().getMessage().getData());
                        couponAdapter = new CouponAdapter(couponsData, Coupons.this, amount);

                        coupons_recycler.setAdapter(couponAdapter);

                        LinearLayoutManager lm_new = new LinearLayoutManager( Coupons.this ,LinearLayoutManager.VERTICAL, false);
                        coupons_recycler.setLayoutManager(lm_new);
                        DividerItemDecoration itemDecorator = new DividerItemDecoration(Coupons.this, DividerItemDecoration.VERTICAL);
                        itemDecorator.setDrawable(ContextCompat.getDrawable(Coupons.this, R.drawable.divider));

                        coupons_recycler.addItemDecoration(itemDecorator);

                    } else {
                        Log.e(MyConstants.TAG, "error" + response.errorBody());
                        no_coupons.setVisibility(View.VISIBLE);
                        apply.setVisibility(View.GONE);
                    }
                } else {
                    //server error
                    server_error.setVisibility(View.VISIBLE);
                    apply.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<CouponsResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                no_internet_layout.setVisibility(View.VISIBLE);
                apply.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cartCount:
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.backPress:
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.apply:
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.homeBtn:
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.retryBtn:
                apply.setVisibility(View.VISIBLE);
                getCartCount();
                getcoupons();
                break;
        }
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{
                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void checkCoupon(final String coupon_id, final String code, final String discount, final String cart_amount){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        CheckCouponPost checkCouponPost = new CheckCouponPost(coupon_id);
        Call<CheckCouponResponse> call = api.check_coupon(getAuthToken(), checkCouponPost);
        call.enqueue(new Callback<CheckCouponResponse>() {
            @Override
            public void onResponse(Call<CheckCouponResponse> call, Response<CheckCouponResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        editor = sharedPreferences.edit();
                        editor.putString("Code", code);
                        editor.putString("coupon_id", coupon_id);
                        editor.putString("discount", discount);
                        editor.putString("cart_amount", cart_amount);
                        editor.apply();
                        apply.setVisibility(View.VISIBLE);
                        coupon_used.setVisibility(View.GONE);
                    }else {
                        apply.setVisibility(View.GONE);
                        coupon_used.setText("Coupon already used");
                        coupon_used.setVisibility(View.VISIBLE);
                    }
                }else{
                    server_error.setVisibility(View.VISIBLE);
                    apply.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<CheckCouponResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
                apply.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void couponselectd(String id, String code, String discount, String cart_amount) {
        progress_dialog.setVisibility(View.VISIBLE);
       checkCoupon(id, code, discount, cart_amount);
    }
}
