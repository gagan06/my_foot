package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Payment;
import com.thegirafe.myfoot.PostModel.RegisterModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.RegisterError;
import com.thegirafe.myfoot.User.RegsiterResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Register_Screen extends AppCompatActivity implements View.OnClickListener {
    private TextInputLayout username_layout, mobile_layout, email_layout, password__layout;
    private TextInputEditText signup_username, signup_mobile, signup_email, signup_password;
    private TextView login;
    private Button signup_button;
    public String Name, MobileNumber, Email, Password;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextView nameError, mobileError, emailError, passwordError;
    View progress_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__screen);

        username_layout = findViewById(R.id.username_layout);
        mobile_layout = findViewById(R.id.mobile_layout);
        email_layout = findViewById(R.id.email_layout);
        password__layout = findViewById(R.id.password__layout);
        signup_username = findViewById(R.id.signup_username);
        signup_mobile = findViewById(R.id.signup_mobile);
        signup_email = findViewById(R.id.signup_email);
        signup_password = findViewById(R.id.signup_password);
        signup_button = findViewById(R.id.signup_button);
        login = findViewById(R.id.login);
        progress_dialog = findViewById(R.id.progress_dialog);
        passwordError = findViewById(R.id.passwordError);
        nameError = findViewById(R.id.nameError);
        mobileError = findViewById(R.id.mobileError);
        emailError = findViewById(R.id.emailError);

        signup_button.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.signup_button:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                if (isValid()) {
                    progress_dialog.setVisibility(View.VISIBLE);
                    register();
                }
                break;

            case R.id.login:
                i = new Intent(Register_Screen.this, Login_Screen.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

        }
    }

    public boolean isValid() {
        Boolean valid = true;
        Email = signup_email.getText().toString().trim();
        if (Email.isEmpty()) {
            emailError.setVisibility(View.VISIBLE);
            emailError.setText("Email is required");
            valid = false;
        }else{
            emailError.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(Email)) {
            if (!Email.matches(emailPattern)) {
                emailError.setVisibility(View.VISIBLE);
                emailError.setText("Enter valid Email");
                valid = false;
            }else{
                emailError.setVisibility(View.GONE);
            }
        }

        Name = signup_username.getText().toString().trim();
        if (Name.length() < 3) {
            nameError.setVisibility(View.VISIBLE);
            nameError.setText("Name should of minimum 3 characters");
            valid = false;
        }else{
            nameError.setVisibility(View.GONE);
        }

        Password = signup_password.getText().toString().trim();
        if (!TextUtils.isEmpty(Password)) {
            if (Password.length() < 6) {
                passwordError.setVisibility(View.VISIBLE);
                passwordError.setText("Password must be Min 6 characters Max 30");
                valid = false;
            }
        }
        if (Password.length() < 6) {
            passwordError.setVisibility(View.VISIBLE);
            passwordError.setText("Password length should be minimum 6");
        }else{
            passwordError.setVisibility(View.GONE);
        }

        MobileNumber = signup_mobile.getText().toString().trim();
        if (MobileNumber.length() == 0){
            mobileError.setText("Mobile number is required");
            mobileError.setVisibility(View.VISIBLE);
            valid = false;
        }else if(MobileNumber.length() < 10){
            mobileError.setText("Invalid mobile number");
            mobileError.setVisibility(View.VISIBLE);
            valid = false;
        }else{
            if (MobileNumber.substring(0, 1).matches("0")){
                mobileError.setText("Invalid mobile number");
                mobileError.setVisibility(View.VISIBLE);
                valid = false;
            }else{
                mobileError.setVisibility(View.GONE);
            }
        }


        return valid;
    }

    public void register(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        final RegisterModel register = new RegisterModel(Name, Email, MobileNumber, Password);
        Call<RegsiterResponse> call = api.register(register);
        call.enqueue(new Callback<RegsiterResponse>() {
            @Override
            public void onResponse(Call<RegsiterResponse> call, Response<RegsiterResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Intent intent = new Intent(Register_Screen.this, OTP_Screen.class);
                        intent.putExtra(MyConstants.Phone, MobileNumber);
                        intent.putExtra(MyConstants.Password, Password);
                        intent.putExtra("type","signup");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }else
                    {
                        if (response.body().getMessage().getErrors().getEmail() != null){
                            new FancyAlertDialog.Builder(Register_Screen.this)
                                    .setTitle("Registration Failed")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Email is already registered with us")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                        if (response.body().getMessage().getErrors().getMobileNo() != null){
                            new FancyAlertDialog.Builder(Register_Screen.this)
                                    .setTitle("Registration Failed")
                                    .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setMessage("Mobile number is already registered with us")
                                    .setNegativeBtnText("OK")
                                    .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                                    .setPositiveBtnText("Try Again")
                                    .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                                    .setAnimation(Animation.POP)
                                    .isCancellable(true)
                                    .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                                    .OnPositiveClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .OnNegativeClicked(new FancyAlertDialogListener() {
                                        @Override
                                        public void OnClick() {
                                        }
                                    })
                                    .build();
                        }
                    }
                }else{
                    new FancyAlertDialog.Builder(Register_Screen.this)
                            .setTitle("Server Error")
                            .setBackgroundColor(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setMessage("Something went wrong at our end, Don't worry it's not you. It's us. Sorry about that")
                            .setNegativeBtnText("OK")
                            .setPositiveBtnBackground(Color.parseColor("#d32f2f"))  //Don't pass R.color.colorvalue
                            .setPositiveBtnText("Try Again")
                            .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                            .setAnimation(Animation.POP)
                            .isCancellable(true)
                            .setIcon(R.drawable.ic_warning_black_24dp, Icon.Visible)
                            .OnPositiveClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                    register();
                                }
                            })
                            .OnNegativeClicked(new FancyAlertDialogListener() {
                                @Override
                                public void OnClick() {
                                }
                            })
                            .build();
                }

            }

            @Override
            public void onFailure(Call<RegsiterResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                new FancyAlertDialog.Builder(Register_Screen.this)
                        .setTitle("No Internet Connection")
                        .setBackgroundColor(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setMessage("OOPS, Internet Connection is Lost, Try Again")
                        .setNegativeBtnText("OK")
                        .setPositiveBtnBackground(Color.parseColor("#30409f"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Try Again")
                        .setNegativeBtnBackground(Color.parseColor("#626262"))  //Don't pass R.color.colorvalue
                        .setAnimation(Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.ic_signal_wifi_off_black_24dp, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                register();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                            }
                        })
                        .build();

            }
        });
    }

}
