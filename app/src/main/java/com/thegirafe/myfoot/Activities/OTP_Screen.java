package com.thegirafe.myfoot.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.OTP.SmsReceiver;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ActivateUserModel;
import com.thegirafe.myfoot.PostModel.ResendOtpModel;
import com.thegirafe.myfoot.PostModel.ResetPasswordModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.User.ActivateUserResponse;
import com.thegirafe.myfoot.User.ResendOtpResponse;
import com.thegirafe.myfoot.User.ResetPasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OTP_Screen extends AppCompatActivity implements View.OnClickListener, SmsReceiver.UiUpdater, View.OnFocusChangeListener {
    Button resendOTP, confirmBtn;
    EditText digit1, digit2, digit3, digit4, digit5, digit6;
    TextView timmerTV;
    LinearLayout timmerLayout;
    String verify_message;
    SmsReceiver smsReceiver;

    TextView toolbar_title;
    ImageButton backPress;
    View progress_dialog;
    String type, mobile, verify_otp;
    Intent intent;
    String password;
    public boolean isValid = false;
    SharedPreferences sharedPreferences, guestPref;
    SharedPreferences.Editor editor, guesteditor;
    String access_id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp__screen);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);
        guestPref = getSharedPreferences(MyConstants.GuestPref, MODE_PRIVATE);

        intent = getIntent();
        if (intent != null){
            type = intent.getStringExtra("type");
            if (type.equalsIgnoreCase("signup")) {
                mobile = intent.getStringExtra(MyConstants.Phone);
                password = intent.getStringExtra(MyConstants.Password);
            }else if(type.equalsIgnoreCase("reset")){
                mobile = intent.getStringExtra(MyConstants.Phone);
            }
        }


        backPress = findViewById(R.id.backPress);
        toolbar_title = findViewById(R.id.toolbar_title);
        resendOTP = findViewById(R.id.resendOTP);
        confirmBtn = findViewById(R.id.confirmBtn);
        digit1 = findViewById(R.id.digit1);
        digit2 = findViewById(R.id.digit2);
        digit3 = findViewById(R.id.digit3);
        digit4 = findViewById(R.id.digit4);
        digit5 = findViewById(R.id.digit5);
        digit6 = findViewById(R.id.digit6);
        timmerTV = findViewById(R.id.timmerTV);
        timmerLayout = findViewById(R.id.timmerLayout);
        progress_dialog = findViewById(R.id.progress_dialog);

        digit1.setOnFocusChangeListener(this);
        digit2.setOnFocusChangeListener(this);
        digit3.setOnFocusChangeListener(this);
        digit4.setOnFocusChangeListener(this);
        digit5.setOnFocusChangeListener(this);
        digit6.setOnFocusChangeListener(this);


        toolbar_title.setText("Verify OTP");

        backPress.setOnClickListener(this);
        resendOTP.setClickable(false);
        startTimer();

        digit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit1.getText().length() == 1) {
                    digit1.clearFocus();
                    digit2.requestFocus();
                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        digit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit2.getText().length() == 1) {
                    digit2.clearFocus();
                    digit3.requestFocus();
                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        digit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit3.getText().length() == 1) {
                    digit3.clearFocus();
                    digit4.requestFocus();
                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        digit4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit4.getText().length() == 1) {
                    digit4.clearFocus();
                    digit5.requestFocus();
                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        digit5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit5.getText().length() == 1) {
                    digit5.clearFocus();
                    digit6.requestFocus();
                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        digit6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (digit6.getText().length() == 1) {
                    digit6.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    digit6.setFocusable(false);
                    digit6.setFocusableInTouchMode(true);

                    if (digit1.length() == 1 && digit2.length() == 1 && digit3.length() == 1 && digit4.length() == 1 && digit5.length() == 1 && digit6.length() == 1) {
                        confirmBtn.setVisibility(View.VISIBLE);
                        isValid = true;
                    }
                } else {
                    confirmBtn.setVisibility(View.GONE);
                    isValid = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        confirmBtn.setOnClickListener(this);
        resendOTP.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
    }



    @Override
    public void setTextView (String message){
        Log.e(MyConstants.TAG, "message " + message);
        verify_message = message;
        verify_otp = message;
        String first = verify_message.substring(0, 1);
        String second = verify_message.substring(1, 2);
        String third = verify_message.substring(2, 3);
        String fourth = verify_message.substring(3, 4);
        String fifth = verify_message.substring(4, 5);
        String sixth = verify_message.substring(5, 6);
        Log.e(MyConstants.TAG, "First : " + first + " Second : " + second + " Third : " + third + " Fourth : " + fourth + " fifth : " + fifth + " sixth : " + sixth);
        digit1.setText(first);
        digit2.setText(second);
        digit3.setText(third);
        digit4.setText(fourth);
        digit5.setText(fifth);
        digit6.setText(sixth);
        if (type.equalsIgnoreCase("signup")) {
            progress_dialog.setVisibility(View.VISIBLE);
            activate();
        }else if (type.equalsIgnoreCase("reset" )){
            progress_dialog.setVisibility(View.VISIBLE);
            reset();
        }
    }

    private void startTimer() {
        resendOTP.setClickable(false);
        resendOTP.setTextColor(ContextCompat.getColor(OTP_Screen.this, R.color.Black));
        new CountDownTimer(60000, 1000) {
            int secondsLeft = 0;

            public void onTick(long ms) {
                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    timmerTV.setText(secondsLeft +" sec");
                }
            }

            public void onFinish() {
                timmerLayout.setVisibility(View.GONE);
                resendOTP.setEnabled(true);
                resendOTP.setVisibility(View.VISIBLE);
                resendOTP.setClickable(true);
                resendOTP.setText("Resend OTP");
                resendOTP.setTextColor(ContextCompat.getColor(OTP_Screen.this, R.color.greyhigh));
            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmBtn:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                verify_otp = digit1.getText().toString()+digit2.getText().toString()+digit3.getText().toString()+digit4.getText().toString()+digit5.getText().toString()+digit6.getText().toString();

                    if (type.equalsIgnoreCase("signup")) {
                        Log.e(MyConstants.TAG, "signup");
                        activate();
                        progress_dialog.setVisibility(View.VISIBLE);
                    }else if(type.equalsIgnoreCase("reset")){
                        Log.e(MyConstants.TAG, "reset");
                        progress_dialog.setVisibility(View.VISIBLE);
                        reset();
                    }

                break;

            case R.id.resendOTP:
//                progress_dialog.setVisibility(View.VISIBLE);
                resend();
                break;

            case R.id.backPress:
                onBackPressed();
                break;
        }
    }

    public void activate(){
        Log.e(MyConstants.TAG, "orp "+verify_otp);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        final ActivateUserModel activateUserModel = new ActivateUserModel(verify_otp, sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<ActivateUserResponse> call = api.activateUser(activateUserModel);
        call.enqueue(new Callback<ActivateUserResponse>() {
            @Override
            public void onResponse(Call<ActivateUserResponse> call, Response<ActivateUserResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        guesteditor = guestPref.edit();
                        guesteditor.putString(MyConstants.Id, response.body().getMessage().getData().getId().toString());
                        guesteditor.apply();
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.Password, password);
                        editor.putString(MyConstants.Id,response.body().getMessage().getData().getId().toString());
                        editor.putString(MyConstants.Username, response.body().getMessage().getData().getName());
                        editor.putString(MyConstants.Phone, response.body().getMessage().getData().getMobileNo());
                        editor.putString(MyConstants.Email, response.body().getMessage().getData().getEmail());
                        editor.putString(MyConstants.AccessToken, "");
                        editor.apply();
                        Intent intent = new Intent(OTP_Screen.this, Home_Screen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.message());
                        if (response.body().getMessage().getErrors().getOtp() != null){
                            getResultSuccess(response.body().getMessage().getErrors().getOtp(), "OTP Mismatch", "Try Again");
                        }
                    }
                }else{
                    Log.e(MyConstants.TAG, "errpr"+response.errorBody());
                    getResultSuccess("Something went wrong", "Try After sometime", "OK");

                }
            }

            @Override
            public void onFailure(Call<ActivateUserResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                getResultSuccess("Check you internet connection", "No Internet Connection", "Try Again");
            }
        });
    }

    public void getResultSuccess(String dec, String title, String btn) {
        final TextView dialog_dec, dialogTitle, Btn;
        final Dialog successDialog = new Dialog(OTP_Screen.this);
        successDialog.setContentView(R.layout.dialog_for_success);
        successDialog.setCancelable(false);
        dialog_dec = successDialog.findViewById(R.id.dialogDec);
        dialogTitle = successDialog.findViewById(R.id.dialogTitle);
        Btn = successDialog.findViewById(R.id.done);
        dialogTitle.setText(title);
        dialog_dec.setText(dec);
        Btn.setText(btn);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();

            }
        });

        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

    public void resend(){
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        final ResendOtpModel resendOtpModel = new ResendOtpModel(mobile);
        Call<ResendOtpResponse> call = api.resendotp(resendOtpModel);
        call.enqueue(new Callback<ResendOtpResponse>() {
            @Override
            public void onResponse(Call<ResendOtpResponse> call, Response<ResendOtpResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Log.e(MyConstants.TAG, " "+ response.body().getSuccess());
                        timmerLayout.setVisibility(View.VISIBLE);
                        resendOTP.setVisibility(View.GONE);
                        startTimer();
                        Toast.makeText(OTP_Screen.this, response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                    }else
                    {
                        if (response.body().getMessage().getErrors().getMobileNo() != null){
                            getResultSuccess(response.body().getMessage().getErrors().getMobileNo(), "OOPS!!!", "Try Again");
                        }
                    }
                }else{
                    getResultSuccess("Something went wrong", "Try After sometime", "OK");

                }
            }

            @Override
            public void onFailure(Call<ResendOtpResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                getResultSuccess("Check you internet connection", "No Internet Connection", "Try Again");
            }
        });
    }


    public void reset(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        final ResetPasswordModel resetPasswordModel = new ResetPasswordModel(mobile, verify_otp);
        Call<ResetPasswordResponse> call = api.reset_password(resetPasswordModel);
        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        Intent intent = new Intent(OTP_Screen.this, Login_Screen.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    }else
                    {
                        if (response.body().getMessage().getErrors().getOtp() != null){
                            getResultSuccess(response.body().getMessage().getErrors().getOtp(), "OTP Mismatch", "Try Again");
                        }

                    }
                }else{
                    getResultSuccess("Something went wrong", "Try After sometime", "OK");

                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                getResultSuccess("Check you internet connection", "No Internet Connection", "Try Again");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(smsReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        smsReceiver = new SmsReceiver(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, intentFilter);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

    }
}
