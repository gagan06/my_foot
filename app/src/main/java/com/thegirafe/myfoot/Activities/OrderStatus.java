package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.thegirafe.myfoot.Adapter.MyOrdersListAdapter;
import com.thegirafe.myfoot.Adapter.OrderStatusAdapter;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Order.MyOrderResponse;
import com.thegirafe.myfoot.Order.OrderStatusData;
import com.thegirafe.myfoot.Order.OrderStatusResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.OrderStatusModel;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderStatus extends AppCompatActivity implements View.OnClickListener {
    RecyclerView statusRecycler;
    TextView toolbar_title;
    ImageButton backPress, cartBtn;
    OrderStatusAdapter adapter;
    ArrayList<OrderStatusData> data = new ArrayList<>();
    Intent intent;
    String order_id;
    String order;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();
        order_id = intent.getStringExtra("Order");
        Log.e(MyConstants.TAG, "id "+order_id);

        statusRecycler = findViewById(R.id.statusRecycler);
        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);

        toolbar_title.setText("Order Status");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);

        getOrder();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(OrderStatus.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
        }
    }

    public void getOrder(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        OrderStatusModel orderStatusModel = new OrderStatusModel(order_id);
        Call<OrderStatusResponse> call = api.order_status(getAuthToken(), orderStatusModel);
        call.enqueue(new Callback<OrderStatusResponse>() {
            @Override
            public void onResponse(Call<OrderStatusResponse> call, Response<OrderStatusResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new OrderStatusAdapter(data,OrderStatus.this);
                        LinearLayoutManager layoutManager=new LinearLayoutManager(OrderStatus.this);
                        statusRecycler.setLayoutManager(layoutManager);
                        statusRecycler.setAdapter(adapter);
//                        add_cart_text.setText("Go to cart");
//                        Toast.makeText(ProductDetail.this, response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(MyOrdersList.this, response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //server error
                }

            }

            @Override
            public void onFailure(Call<OrderStatusResponse> call, Throwable t) {
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }
    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }
}
