package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.thegirafe.myfoot.R;

public class ContactUs extends AppCompatActivity implements View.OnClickListener {
TextView callNumber,addressTV;
    TextView toolbar_title;
    ImageButton backPress,cartBtn;
    TextView nameError,emailError,enquirylError;
    EditText nameET,emailET,enquiryET;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        toolbar_title = findViewById(R.id.toolbar_title);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        cartBtn = findViewById(R.id.cartBtn);
        nameError = findViewById(R.id.nameError);
        emailError = findViewById(R.id.emailError);
        enquirylError = findViewById(R.id.enquirylError);
        nameET = findViewById(R.id.nameET);
        emailET = findViewById(R.id.emailET);
        enquiryET = findViewById(R.id.enquiryET);

        toolbar_title.setText("Account Detail");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,0);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i=new Intent(ContactUs.this, CartProduct.class);
                startActivity(i);
                break;
        }
    }
}
