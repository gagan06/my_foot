package com.thegirafe.myfoot.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.thegirafe.myfoot.Adapter.ImagePagerAdapter;
import com.thegirafe.myfoot.Adapter.MyOffersAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.MyOffersModel;
import com.thegirafe.myfoot.Offers.OfferPostData;
import com.thegirafe.myfoot.Offers.OfferProductData;
import com.thegirafe.myfoot.Offers.OfferProductResponse;
import com.thegirafe.myfoot.Offers.OffersSliderData;
import com.thegirafe.myfoot.Offers.OffersSliderResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyOffers extends AppCompatActivity implements View.OnClickListener {
    TextView offerTitle;
    RecyclerView offersRecycler;
    MyOffersAdapter adapter;
    ArrayList<OfferProductData>data=new ArrayList<>();
    TextView toolbar_title;
    ImageButton backPress,cartBtn;
    String Offer_id, discount;
    TextView txtdesc, cartCount;
    Button homeBtn;
    Intent intent;
    SharedPreferences sharedPreferences;
    Button retryBtn;
    View progress_dialog, server_error, no_internet_layout, search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_offers);

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();
        Offer_id = intent.getStringExtra("Offer_id");
        discount = intent.getStringExtra("discount");
        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
            getOffers(Offer_id);
        }else{
            getOffersGuest(Offer_id);
        }
        toolbar_title = findViewById(R.id.toolbar_title);
        cartCount = findViewById(R.id.cartCount);
        txtdesc = findViewById(R.id.txtdesc);
        homeBtn = findViewById(R.id.homeBtn);
        progress_dialog = findViewById(R.id.progress_dialog);
        server_error = findViewById(R.id.server_error);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        search = findViewById(R.id.search);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        retryBtn = findViewById(R.id.retryBtn);
        offerTitle=findViewById(R.id.offerTitle);
        offersRecycler=findViewById(R.id.offersRecycler);

        toolbar_title.setText("My Offers");

        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        retryBtn.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,0);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i=new Intent(MyOffers.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.retryBtn:
                if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")){
                    getOffers(Offer_id);
                }else{
                    getOffersGuest(Offer_id);
                }

                break;
        }
    }

    public void getOffersGuest(String Offer_id) {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);


        OfferPostData offerPostData = new OfferPostData(Offer_id);
        Call<OfferProductResponse> call = api.get_offersproducts(offerPostData);
        call.enqueue(new Callback<OfferProductResponse>() {
            @Override
            public void onResponse(Call<OfferProductResponse> call, Response<OfferProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new MyOffersAdapter(data, MyOffers.this, discount);
                        offersRecycler.setAdapter(adapter);
                        offersRecycler.setLayoutManager(new GridLayoutManager(MyOffers.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        search.setVisibility(View.VISIBLE);
                        txtdesc.setText("We can't find any item for this offer");
                        homeBtn.setText("home");
                        homeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                search.setVisibility(View.GONE);
                                onBackPressed();
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });

                    }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<OfferProductResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getOffers(String Offer_id) {
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);


        OfferPostData offerPostData = new OfferPostData(Offer_id);
        Call<OfferProductResponse> call = api.get_offersproducts(getAuthToken(), offerPostData);
        call.enqueue(new Callback<OfferProductResponse>() {
            @Override
            public void onResponse(Call<OfferProductResponse> call, Response<OfferProductResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new MyOffersAdapter(data, MyOffers.this, discount);
                        offersRecycler.setAdapter(adapter);
                        offersRecycler.setLayoutManager(new GridLayoutManager(MyOffers.this, 2));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                        search.setVisibility(View.VISIBLE);
                        txtdesc.setText("We can't find any item for this offer");
                        homeBtn.setText("home");
                        homeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                search.setVisibility(View.GONE);
                                onBackPressed();
                                finish();
                            }
                        });

                    }
                }else{
                    //server error
                    server_error.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onFailure(Call<OfferProductResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
                progress_dialog.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(MyConstants.Email, "") + ":" + sharedPreferences.getString(MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getGuestCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success "+response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getCartCount(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        cartCount.setText(response.body().getMessage().getSuccess());
                    }else {
                        cartCount.setText("0");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }



}
