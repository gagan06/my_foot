package com.thegirafe.myfoot.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.instamojo.android.Instamojo;
import com.thegirafe.myfoot.Adapter.PaymentMethodAdapter;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.Method_interface;
import com.thegirafe.myfoot.Order.OrderResponse;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PaymentMethods.PaymentMethodData;
import com.thegirafe.myfoot.PaymentMethods.PaymentMethodResponse;
import com.thegirafe.myfoot.PostModel.OrderModel;
import com.thegirafe.myfoot.PostModel.OrderOnlineModel;
import com.thegirafe.myfoot.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaymentScreen extends AppCompatActivity implements View.OnClickListener, Method_interface {

    TextView toolbar_title, cartCount;
    ImageButton backPress, cartBtn;
    Intent intent;
    String address_id, Billing_id;
    RadioButton radioButton1, radioButton2;
    SharedPreferences sharedPreferences;
    String payment_id;
    Button orderBtn;
    RadioGroup radioGroup;
    RecyclerView method_recycler;
    String selectedRadioButtonText;
    ArrayList<PaymentMethodData> paymentMethodData;
    ArrayList<String> payment;
    String Amount;
    String accessToken;
    private String currentEnv = null;
    private ProgressDialog dialog;
    TextView itemCount, totalPrice, discounted_price;
    String item;
    String coupon_id, coupon_code, coupon_amount;
    SharedPreferences.Editor editor;
    View progress_dialog;
    OrderModel addCartModel;
    OrderOnlineModel orderOnlineModel;
    PaymentMethodAdapter paymentMethodAdapter;
    String amount_new, Phone;
    int discount, amount, total_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_screen);
        ;

        sharedPreferences = getSharedPreferences(MyConstants.MyPref, MODE_PRIVATE);

        intent = getIntent();
        item = intent.getStringExtra("Item");
        address_id = intent.getStringExtra("Address_id");
        Amount = intent.getStringExtra("Amount");
        Phone = intent.getStringExtra("Phone");
        Log.e(MyConstants.TAG, "ADddress " + Amount);
        Billing_id = intent.getStringExtra("Billing_id");
        amount_new = Amount.replace(",", "");
        itemCount = findViewById(R.id.itemCount);
        totalPrice = findViewById(R.id.totalPrice);
        cartCount = findViewById(R.id.cartCount);
        discounted_price = findViewById(R.id.discounted_price);
        itemCount.setText(item);
        if (!sharedPreferences.getString("Code", "").equalsIgnoreCase("")) {
            coupon_id = sharedPreferences.getString("coupon_id", "");
            coupon_code = sharedPreferences.getString("Code", "");
            coupon_amount = sharedPreferences.getString("discount", "");
            amount_new = Amount.replace(",", "");
            String discount_new = coupon_amount.replace(",", "");
            discount = Integer.parseInt(discount_new);
            amount = Math.round(Float.parseFloat(amount_new));
            total_price = amount - discount;
            totalPrice.setText(String.valueOf(total_price));
            discounted_price.setText(Amount);
            discounted_price.setVisibility(View.VISIBLE);
        } else {
            amount_new = Amount.replace(",", "");
            amount = Math.round(Float.parseFloat(amount_new));
            total_price = amount;
            totalPrice.setText(String.valueOf(total_price));
        }
        toolbar_title = findViewById(R.id.toolbar_title);
        method_recycler = findViewById(R.id.method_recycler);
        backPress = findViewById(R.id.backPress);
        cartBtn = findViewById(R.id.cartBtn);
        radioButton1 = findViewById(R.id.radioButton1);
        radioButton2 = findViewById(R.id.radioButton2);
        radioGroup = findViewById(R.id.radioGroup);
        orderBtn = findViewById(R.id.orderBtn);
        toolbar_title.setText("Payment");
        progress_dialog = findViewById(R.id.progress_dialog);
        getmethods();
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        backPress.setOnClickListener(this);
        cartBtn.setOnClickListener(this);
        orderBtn.setOnClickListener(this);
        Instamojo.setLogLevel(Log.DEBUG);

        if (sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
            getCartCount();
        } else {
            getGuestCartCount();
        }
    }

    public void getCartCount() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Call<CartCountResponse> call = api.get_cart_count(getAuthToken());
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success " + response.body().getMessage().getSuccess());
                    } else {
                        cartCount.setText("0");
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }

    public void getGuestCartCount() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        GuestUserModel guestUserModel = new GuestUserModel(sharedPreferences.getString(MyConstants.AccessToken, ""));
        Call<CartCountResponse> call = api.get_guest_count(guestUserModel);
        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        cartCount.setText(response.body().getMessage().getSuccess());
                        Log.e(MyConstants.TAG, "Cart success " + response.body().getMessage().getSuccess());
                    } else {
                        cartCount.setText("0");
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
        finish();
        editor = sharedPreferences.edit();
        editor.putString("coupon_id", "");
        editor.putString("Code", "");
        editor.putString("discount", "");
        editor.apply();

    }

    @Override
    public void onClick(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.backPress:
                onBackPressed();
                break;
            case R.id.cartBtn:
                i = new Intent(PaymentScreen.this, CartProduct.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.apply_coupon:
                if (!sharedPreferences.getString(MyConstants.AccessToken, "").equalsIgnoreCase("")) {
//                    Intent intent = new Intent(CartProduct.this, Register_Screen.class);
//                    startActivity(intent);
                } else {
                    i = new Intent(PaymentScreen.this, Coupons.class);
                    i.putExtra("Amount", amount_new);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                break;
            case R.id.orderBtn:
                int selectedRadioButtonID = radioGroup.getCheckedRadioButtonId();

                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID != -1) {

                    RadioButton selectedRadioButton = (RadioButton) findViewById(selectedRadioButtonID);
                    selectedRadioButtonText = selectedRadioButton.getText().toString();
                    Log.e(MyConstants.TAG, "Order" + selectedRadioButtonText);
                } else {
                    Log.e(MyConstants.TAG, "Nothing selected from Radio Group.");
                }

                int index = -1;
                for (int j = 0; j < payment.size(); j++) {
                    if (payment.get(j).equals(selectedRadioButtonText)) {
                        index = j;
                        break;
                    }
                }

//                if (selectedRadioButtonText.equalsIgnoreCase("online")) {
//                    callInstamojoPay(sharedPreferences.getString(MyConstants.MyPref, M));
//                    progress_dialog.setVisibility(View.VISIBLE);
//                } else {
//                    Log.e(MyConstants.TAG, "iNDEX " + index);
//                    payment_id = paymentMethodData.get(index).getId().toString();
//                    Log.e(MyConstants.TAG, "Order" + payment_id);
//                    if (payment_id != null) {
//                        order();
//                    }
//                }


                break;
        }
    }

    private HttpUrl.Builder getHttpURLBuilder() {
        return new HttpUrl.Builder()
                .scheme("https")
                .host("myfoot.thegirafe.com");
    }

    public static String getImageUUID() {
        return UUID.randomUUID().toString();
    }


    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }


    public void getmethods() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);

        Call<PaymentMethodResponse> call = api.get_payments(getAuthToken());
        call.enqueue(new Callback<PaymentMethodResponse>() {
            @Override
            public void onResponse(Call<PaymentMethodResponse> call, Response<PaymentMethodResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        paymentMethodData = new ArrayList<>(response.body().getMessage().getData());
                        payment = new ArrayList<>();
                        for (int p = 0; p < paymentMethodData.size(); p++) {
                            payment.add(paymentMethodData.get(p).getName());
                        }

                        paymentMethodAdapter = new PaymentMethodAdapter(paymentMethodData, PaymentScreen.this);
                        method_recycler.setAdapter(paymentMethodAdapter);
                        LinearLayoutManager lm_new = new LinearLayoutManager(PaymentScreen.this, LinearLayoutManager.VERTICAL, false);
                        method_recycler.setLayoutManager(lm_new);

                        radioButton1.setText(paymentMethodData.get(0).getName());
                        radioButton2.setText(paymentMethodData.get(1).getName());
                    } else {
                        Log.e(MyConstants.TAG, "error" + response.errorBody());
                    }
                } else {
                    //server error
                }

            }

            @Override
            public void onFailure(Call<PaymentMethodResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


    public void order() {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        if (sharedPreferences.getString(MyConstants.Coupon_ID, "").equalsIgnoreCase("")) {
            Log.e(MyConstants.TAG, "Coupon " + sharedPreferences.getString(MyConstants.Coupon_ID, ""));
            addCartModel = new OrderModel(address_id, null, payment_id, Billing_id, null);
        } else {
            Log.e(MyConstants.TAG, "Coupon " + sharedPreferences.getString(MyConstants.Coupon_ID, ""));
            addCartModel = new OrderModel(address_id, "1", payment_id, Billing_id, sharedPreferences.getString(MyConstants.Coupon_ID, ""));
        }


        Call<OrderResponse> call = api.order(getAuthToken(), addCartModel);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.Coupon_ID, "");
                        editor.putString("Code", "");
                        editor.putString("coupon_id", "");
                        editor.putString("discount", "");
                        editor.putString("cart_amount", "");
                        editor.apply();
                        Log.e(MyConstants.TAG, "SUCCESS " + response.body().getMessage().getSuccess());
                        Intent intent = new Intent(PaymentScreen.this, OrderSuccess.class);
                        intent.putExtra("order", response.body().getMessage().getSuccess());
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();

                    } else {
                        Toast.makeText(PaymentScreen.this, response.body().getMessage().getSuccess(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //server error
                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }

    public void getResultSuccess(String dec) {
        TextView description, dialogTitle, viewOrders;
        final Dialog successDialog = new Dialog(PaymentScreen.this);
        successDialog.setContentView(R.layout.dialog_for_success_order);
        successDialog.setCancelable(false);
        description = successDialog.findViewById(R.id.description);
        viewOrders = successDialog.findViewById(R.id.viewOrders);
        description.setText(dec);
        viewOrders.setText("Done");
        viewOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successDialog.dismiss();
                Intent intent = new Intent(PaymentScreen.this, MyOrdersList.class);
                startActivity(intent);
                finish();

            }
        });
        successDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        successDialog.show();

    }

    public void orderOnline(String order_date, String order_status, String transaction_id) {
        progress_dialog.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api = retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "color " + payment_id + "   " + address_id + " " + order_status + " " + transaction_id + " " + order_date);

        if (sharedPreferences.getString(MyConstants.Coupon_ID, "").equalsIgnoreCase("")) {
            Log.e(MyConstants.TAG, "Coupon " + sharedPreferences.getString(MyConstants.Coupon_ID, ""));
            orderOnlineModel = new OrderOnlineModel(address_id, null, "2", order_date, order_status, transaction_id, Billing_id, null);
        } else {
            Log.e(MyConstants.TAG, "Coupon " + sharedPreferences.getString(MyConstants.Coupon_ID, ""));
            orderOnlineModel = new OrderOnlineModel(address_id, "1", "2", order_date, order_status, transaction_id, Billing_id, sharedPreferences.getString(MyConstants.Coupon_ID, ""));
        }

//        OrderOnlineModel addCartModel = new OrderOnlineModel(address_id, null, "2", order_date, order_status, transaction_id, Billing_id);
        Call<OrderResponse> call = api.order_online(getAuthToken(), orderOnlineModel);
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                progress_dialog.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        editor = sharedPreferences.edit();
                        editor.putString(MyConstants.Coupon_ID, "");
                        editor.putString("Code", "");
                        editor.putString("coupon_id", "");
                        editor.putString("discount", "");
                        editor.putString("cart_amount", "");
                        editor.apply();
                        Intent intent = new Intent(PaymentScreen.this, OrderSuccess.class);
                        intent.putExtra("order", response.body().getMessage().getSuccess());
                        startActivity(intent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                    } else {
                        Toast.makeText(PaymentScreen.this, "" + response.errorBody() + response.body().getMessage().getErrors(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //server error
                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                progress_dialog.setVisibility(View.GONE);
                Log.e(MyConstants.TAG, t.toString());
            }
        });
    }

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = this;
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", sharedPreferences.getString(MyConstants.Email, ""));
            pay.put("phone", Phone);
            pay.put("purpose", "Shopping");
            pay.put("amount", amount);
            pay.put("name", sharedPreferences.getString(MyConstants.Username, ""));
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                progress_dialog.setVisibility(View.VISIBLE);
                int begginingIndex = response.indexOf("orderId=") + "orderId=".length();
                int endIndex = response.indexOf(":txnId");
                String order_id = response.substring(begginingIndex, endIndex);
                int begginingIndex1 = response.indexOf("status=") + "status=".length();
                int endIndex1 = response.indexOf(":orderId");
                String status = response.substring(begginingIndex1, endIndex1);
                int begginingIndex2 = response.indexOf("paymentId=") + "paymentId=".length();
                int endIndex2 = response.indexOf(":token");
                String paymentId = response.substring(begginingIndex2, endIndex2);
                Log.e("Payment", "Response " + paymentId);

                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
//                Log.e(MyConstants.TAG, "response "+responseObject);
                orderOnline(date, status, paymentId);
//                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG)
//                        .show();
            }

            @Override
            public void onFailure(int code, String reason) {
                progress_dialog.setVisibility(View.GONE);
                Intent intent = new Intent(PaymentScreen.this, OrderFailure.class);
                if (reason.equalsIgnoreCase("Invalid mobile")) {
                    intent.putExtra("order", "Update your mobile number for online transaction we need your mobile number");
                } else {
                    intent.putExtra("order", reason);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        };
    }

    @Override
    public void method(String s, String name) {
        payment_id = s;
        if (name.equalsIgnoreCase("online")) {
            progress_dialog.setVisibility(View.VISIBLE);
            callInstamojoPay(sharedPreferences.getString(MyConstants.Email, ""), sharedPreferences.getString(MyConstants.Phone, ""), String.valueOf(total_price), "shopping", sharedPreferences.getString(MyConstants.Username, ""));
        } else {
            progress_dialog.setVisibility(View.VISIBLE);
            order();
        }
    }


}