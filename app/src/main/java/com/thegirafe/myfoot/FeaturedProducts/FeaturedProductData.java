package com.thegirafe.myfoot.FeaturedProducts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thegirafe.myfoot.Products.ProductGallery;
import com.thegirafe.myfoot.Products.SubProduct;

import java.util.List;

public class FeaturedProductData {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("featured_image")
    @Expose
    private String featuredImage;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_original_price")
    @Expose
    private String productOriginalPrice;
    @SerializedName("product_sale_price")
    @Expose
    private String productSalePrice;
    @SerializedName("product_quantity")
    @Expose
    private String productQuantity;
    @SerializedName("product_handling_fee")
    @Expose
    private String productHandlingFee;
    @SerializedName("product_has_free_shipping")
    @Expose
    private String productHasFreeShipping;
    @SerializedName("product_shipping_amount")
    @Expose
    private String productShippingAmount;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("product_delivery_detail")
    @Expose
    private String productDeliveryDetail;
    @SerializedName("product_sold_by")
    @Expose
    private String productSoldBy;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("wishlist")
    @Expose
    private String wishlist;
    @SerializedName("cart")
    @Expose
    private String cart;
    @SerializedName("product_gallery")
    @Expose
    private List<ProductGallery> productGallery = null;
    @SerializedName("sub_products")
    @Expose
    private List<SubProduct> subProducts = null;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductOriginalPrice() {
        return productOriginalPrice;
    }

    public void setProductOriginalPrice(String productOriginalPrice) {
        this.productOriginalPrice = productOriginalPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductHandlingFee() {
        return productHandlingFee;
    }

    public void setProductHandlingFee(String productHandlingFee) {
        this.productHandlingFee = productHandlingFee;
    }

    public String getProductHasFreeShipping() {
        return productHasFreeShipping;
    }

    public void setProductHasFreeShipping(String productHasFreeShipping) {
        this.productHasFreeShipping = productHasFreeShipping;
    }

    public String getProductShippingAmount() {
        return productShippingAmount;
    }

    public void setProductShippingAmount(String productShippingAmount) {
        this.productShippingAmount = productShippingAmount;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getProductDeliveryDetail() {
        return productDeliveryDetail;
    }

    public void setProductDeliveryDetail(String productDeliveryDetail) {
        this.productDeliveryDetail = productDeliveryDetail;
    }

    public String getProductSoldBy() {
        return productSoldBy;
    }

    public void setProductSoldBy(String productSoldBy) {
        this.productSoldBy = productSoldBy;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public List<ProductGallery> getProductGallery() {
        return productGallery;
    }

    public void setProductGallery(List<ProductGallery> productGallery) {
        this.productGallery = productGallery;
    }

    public List<SubProduct> getSubProducts() {
        return subProducts;
    }

    public void setSubProducts(List<SubProduct> subProducts) {
        this.subProducts = subProducts;
    }

    public FeaturedProductData(String productId, String productName, String featuredImage) {
        this.productId = productId;
        this.productName = productName;
        this.featuredImage = featuredImage;
    }
}
