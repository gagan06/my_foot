package com.thegirafe.myfoot.FeaturedProducts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeaturedProductMessage {
    @SerializedName("data")
    @Expose
    private List<FeaturedProductData> data = null;

    public List<FeaturedProductData> getData() {
        return data;
    }

    public void setData(List<FeaturedProductData> data) {
        this.data = data;
    }

}
