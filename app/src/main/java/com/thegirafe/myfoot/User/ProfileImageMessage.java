package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileImageMessage {
    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @SerializedName("errors")
    @Expose
    private ProfileImageError errors;

    public ProfileImageError getErrors() {
        return errors;
    }

    public void setErrors(ProfileImageError errors) {
        this.errors = errors;
    }

}
