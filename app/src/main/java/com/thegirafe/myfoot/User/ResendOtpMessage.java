package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResendOtpMessage {
    @SerializedName("errors")
    @Expose
    private ResendOtpError errors;

    public ResendOtpError getErrors() {
        return errors;
    }

    public void setErrors(ResendOtpError errors) {
        this.errors = errors;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
