package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileMessage {
    @SerializedName("data")
    @Expose
    private UserProfileData data;

    public UserProfileData getData() {
        return data;
    }

    public void setData(UserProfileData data) {
        this.data = data;
    }

    @SerializedName("errors")
    @Expose
    private RegisterError errors;

    public RegisterError getErrors() {
        return errors;
    }

    public void setErrors(RegisterError errors) {
        this.errors = errors;
    }
}
