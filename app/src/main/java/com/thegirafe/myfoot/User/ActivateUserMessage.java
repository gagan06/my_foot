package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivateUserMessage {
    @SerializedName("errors")
    @Expose
    private ActivateUserError errors;

    public ActivateUserError getErrors() {
        return errors;
    }

    public void setErrors(ActivateUserError errors) {
        this.errors = errors;
    }

    @SerializedName("data")
    @Expose
    private ActivateUserData data;

    public ActivateUserData getData() {
        return data;
    }

    public void setData(ActivateUserData data) {
        this.data = data;
    }
}
