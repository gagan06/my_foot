package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterMessage {
    @SerializedName("errors")
    @Expose
    private RegisterError errors;

    public RegisterError getErrors() {
        return errors;
    }

    public void setErrors(RegisterError errors) {
        this.errors = errors;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
