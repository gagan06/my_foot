package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPasswordMessage {
    @SerializedName("errors")
    @Expose
    private ResetPasswordError errors;

    public ResetPasswordError getErrors() {
        return errors;
    }

    public void setErrors(ResetPasswordError errors) {
        this.errors = errors;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
