package com.thegirafe.myfoot.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetPasswordMessage {
    @SerializedName("errors")
    @Expose
    private ForgetPasswordError errors;

    public ForgetPasswordError getErrors() {
        return errors;
    }

    public void setErrors(ForgetPasswordError errors) {
        this.errors = errors;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
