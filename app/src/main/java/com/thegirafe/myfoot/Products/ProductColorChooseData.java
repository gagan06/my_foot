package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductColorChooseData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("attribute_value_id")
    @Expose
    private Integer attribute_value_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getAttribute_value_id() {
        return attribute_value_id;
    }

    public void setAttribute_value_id(Integer attribute_value_id) {
        this.attribute_value_id = attribute_value_id;
    }

    public ProductColorChooseData(String productImage, Integer attribute_value_id, String productId) {
        this.productImage = productImage;
        this.attribute_value_id = attribute_value_id;
        this.productId = productId;
    }
}
