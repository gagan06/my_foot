package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailData {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("user_liked")
    @Expose
    private int user_liked;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_original_price")
    @Expose
    private Object productOriginalPrice;
    @SerializedName("product_sale_price")
    @Expose
    private Object productSalePrice;
    @SerializedName("product_quantity")
    @Expose
    private String productQuantity;
    @SerializedName("product_handling_fee")
    @Expose
    private String productHandlingFee;
    @SerializedName("product_has_free_shipping")
    @Expose
    private String productHasFreeShipping;
    @SerializedName("product_shipping_amount")
    @Expose
    private String productShippingAmount;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("product_delivery_detail")
    @Expose
    private Object productDeliveryDetail;
    @SerializedName("product_sold_by")
    @Expose
    private Object productSoldBy;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("subproducts")
    @Expose
    private List<Subproduct1> subproducts = null;
    @SerializedName("color")
    @Expose
    private Color color;
    @SerializedName("size")
    @Expose
    private Size size;
    @SerializedName("wishlist")
    @Expose
    private String wishlist;
    @SerializedName("cart")
    @Expose
    private String cart;
    @SerializedName("reviews")
    @Expose
    private Integer reviews;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Object getProductOriginalPrice() {
        return productOriginalPrice;
    }

    public void setProductOriginalPrice(Object productOriginalPrice) {
        this.productOriginalPrice = productOriginalPrice;
    }

    public Object getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(Object productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductHandlingFee() {
        return productHandlingFee;
    }

    public void setProductHandlingFee(String productHandlingFee) {
        this.productHandlingFee = productHandlingFee;
    }

    public String getProductHasFreeShipping() {
        return productHasFreeShipping;
    }

    public void setProductHasFreeShipping(String productHasFreeShipping) {
        this.productHasFreeShipping = productHasFreeShipping;
    }

    public String getProductShippingAmount() {
        return productShippingAmount;
    }

    public void setProductShippingAmount(String productShippingAmount) {
        this.productShippingAmount = productShippingAmount;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public Object getProductDeliveryDetail() {
        return productDeliveryDetail;
    }

    public void setProductDeliveryDetail(Object productDeliveryDetail) {
        this.productDeliveryDetail = productDeliveryDetail;
    }

    public Object getProductSoldBy() {
        return productSoldBy;
    }

    public void setProductSoldBy(Object productSoldBy) {
        this.productSoldBy = productSoldBy;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Subproduct1> getSubproducts() {
        return subproducts;
    }

    public void setSubproducts(List<Subproduct1> subproducts) {
        this.subproducts = subproducts;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public Integer getReviews() {
        return reviews;
    }

    public void setReviews(Integer reviews) {
        this.reviews = reviews;
    }

    public int getUser_liked() {
        return user_liked;
    }

    public void setUser_liked(int user_liked) {
        this.user_liked = user_liked;
    }
}
