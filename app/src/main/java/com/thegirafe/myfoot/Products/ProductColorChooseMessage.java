package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductColorChooseMessage {
    @SerializedName("errors")
    @Expose
    private ProductColorChooseError errors;

    public ProductColorChooseError getErrors() {
        return errors;
    }

    public void setErrors(ProductColorChooseError errors) {
        this.errors = errors;
    }


    @SerializedName("data")
    @Expose
    private List<ProductColorChooseData> data = null;

    public List<ProductColorChooseData> getData() {
        return data;
    }

    public void setData(List<ProductColorChooseData> data) {
        this.data = data;
    }

}
