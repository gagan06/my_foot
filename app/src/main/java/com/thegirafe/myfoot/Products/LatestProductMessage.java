package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LatestProductMessage {
    @SerializedName("data")
    @Expose
    private List<LatestProductData> data = null;

    public List<LatestProductData> getData() {
        return data;
    }

    public void setData(List<LatestProductData> data) {
        this.data = data;
    }

}
