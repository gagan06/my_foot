package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryProductMessage {
    @SerializedName("data")
    @Expose
    private List<CategoryProductData> data = null;

    public List<CategoryProductData> getData() {
        return data;
    }

    public void setData(List<CategoryProductData> data) {
        this.data = data;
    }

}
