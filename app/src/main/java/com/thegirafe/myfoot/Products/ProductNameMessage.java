package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductNameMessage {
    @SerializedName("data")
    @Expose
    private List<ProductNameData> data = null;

    public List<ProductNameData> getData() {
        return data;
    }

    public void setData(List<ProductNameData> data) {
        this.data = data;
    }
}
