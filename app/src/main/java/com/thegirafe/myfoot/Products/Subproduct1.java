package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subproduct1 {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_original_price")
    @Expose
    private String productOriginalPrice;
    @SerializedName("product_sale_price")
    @Expose
    private String productSalePrice;
    @SerializedName("product_quantity")
    @Expose
    private String productQuantity;
    @SerializedName("product_handling_fee")
    @Expose
    private String productHandlingFee;
    @SerializedName("product_has_free_shipping")
    @Expose
    private String productHasFreeShipping;
    @SerializedName("product_shipping_amount")
    @Expose
    private String productShippingAmount;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("product_delivery_detail")
    @Expose
    private String productDeliveryDetail;
    @SerializedName("product_sold_by")
    @Expose
    private String productSoldBy;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("color")
    @Expose
    private Color color;
    @SerializedName("size")
    @Expose
    private Size size;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductOriginalPrice() {
        return productOriginalPrice;
    }

    public void setProductOriginalPrice(String productOriginalPrice) {
        this.productOriginalPrice = productOriginalPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductHandlingFee() {
        return productHandlingFee;
    }

    public void setProductHandlingFee(String productHandlingFee) {
        this.productHandlingFee = productHandlingFee;
    }

    public String getProductHasFreeShipping() {
        return productHasFreeShipping;
    }

    public void setProductHasFreeShipping(String productHasFreeShipping) {
        this.productHasFreeShipping = productHasFreeShipping;
    }

    public String getProductShippingAmount() {
        return productShippingAmount;
    }

    public void setProductShippingAmount(String productShippingAmount) {
        this.productShippingAmount = productShippingAmount;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getProductDeliveryDetail() {
        return productDeliveryDetail;
    }

    public void setProductDeliveryDetail(String productDeliveryDetail) {
        this.productDeliveryDetail = productDeliveryDetail;
    }

    public String getProductSoldBy() {
        return productSoldBy;
    }

    public void setProductSoldBy(String productSoldBy) {
        this.productSoldBy = productSoldBy;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
