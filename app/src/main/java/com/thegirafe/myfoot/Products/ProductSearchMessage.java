package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductSearchMessage {

    @SerializedName("data")
    @Expose
    private List<ProductSearchData> data = null;

    public List<ProductSearchData> getData() {
        return data;
    }

    public void setData(List<ProductSearchData> data) {
        this.data = data;
    }
    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
