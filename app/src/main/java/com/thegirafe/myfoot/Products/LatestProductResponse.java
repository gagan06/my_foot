package com.thegirafe.myfoot.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LatestProductResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private LatestProductMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LatestProductMessage getMessage() {
        return message;
    }

    public void setMessage(LatestProductMessage message) {
        this.message = message;
    }
}
