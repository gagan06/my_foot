package com.thegirafe.myfoot.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyCartMessage {
    @SerializedName("data")
    @Expose
    private MyCartData data;

    public MyCartData getData() {
        return data;
    }

    public void setData(MyCartData data) {
        this.data = data;
    }
    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
