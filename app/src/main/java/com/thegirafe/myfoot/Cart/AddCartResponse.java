package com.thegirafe.myfoot.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCartResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private AddCartMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public AddCartMessage getMessage() {
        return message;
    }

    public void setMessage(AddCartMessage message) {
        this.message = message;
    }

}
