package com.thegirafe.myfoot.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thegirafe.myfoot.Products.ProductGallery;

import java.util.List;

public class ProductList {
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("featured_image")
    @Expose
    private String featuredImage;
    @SerializedName("total_quantity")
    @Expose
    private Integer totalQuantity;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("list_price")
    @Expose
    private Integer listPrice;
    @SerializedName("shipping_amount")
    @Expose
    private Integer shippingAmount;
    @SerializedName("product_price")
    @Expose
    private Integer productPrice;
    @SerializedName("wishlist")
    @Expose
    private Integer wishlist;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("cart")
    @Expose
    private Integer cart;
    @SerializedName("product_delivery_detail")
    @Expose
    private String productDeliveryDetail;
    @SerializedName("product_sold_by")
    @Expose
    private String productSoldBy;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("brand_name")
    @Expose
    private String brandName;
    @SerializedName("color")
    @Expose
    private Integer color;
    @SerializedName("size")
    @Expose
    private List<String> size = null;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("color_name")
    @Expose
    private List<String> colorName = null;
    @SerializedName("product_gallery")
    @Expose
    private List<ProductGallery> productGallery = null;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Integer getListPrice() {
        return listPrice;
    }

    public void setListPrice(Integer listPrice) {
        this.listPrice = listPrice;
    }

    public Integer getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(Integer shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getWishlist() {
        return wishlist;
    }

    public void setWishlist(Integer wishlist) {
        this.wishlist = wishlist;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getCart() {
        return cart;
    }

    public void setCart(Integer cart) {
        this.cart = cart;
    }

    public String getProductDeliveryDetail() {
        return productDeliveryDetail;
    }

    public void setProductDeliveryDetail(String productDeliveryDetail) {
        this.productDeliveryDetail = productDeliveryDetail;
    }

    public String getProductSoldBy() {
        return productSoldBy;
    }

    public void setProductSoldBy(String productSoldBy) {
        this.productSoldBy = productSoldBy;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public List<String> getSize() {
        return size;
    }

    public void setSize(List<String> size) {
        this.size = size;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public List<String> getColorName() {
        return colorName;
    }

    public void setColorName(List<String> colorName) {
        this.colorName = colorName;
    }

    public List<ProductGallery> getProductGallery() {
        return productGallery;
    }

    public void setProductGallery(List<ProductGallery> productGallery) {
        this.productGallery = productGallery;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }
}
