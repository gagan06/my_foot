package com.thegirafe.myfoot.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCartMessage {
    @SerializedName("errors")
    @Expose
    private AddCartError errors;

    public AddCartError getErrors() {
        return errors;
    }

    public void setErrors(AddCartError errors) {
        this.errors = errors;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
