package com.thegirafe.myfoot.Categories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrandsResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private BrandsMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BrandsMessage getMessage() {
        return message;
    }

    public void setMessage(BrandsMessage message) {
        this.message = message;
    }

}
