package com.thegirafe.myfoot.Categories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CatgeoryMessage {
    @SerializedName("data")
    @Expose
    private List<CatgeoryList> data = null;

    public List<CatgeoryList> getData() {
        return data;
    }

    public void setData(List<CatgeoryList> data) {
        this.data = data;
    }
}
