package com.thegirafe.myfoot.Categories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrandsMessage {
    @SerializedName("data")
    @Expose
    private List<BrandsList> data = null;

    public List<BrandsList> getData() {
        return data;
    }

    public void setData(List<BrandsList> data) {
        this.data = data;
    }
}
