package com.thegirafe.myfoot.coupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponsResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private CouponsMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CouponsMessage getMessage() {
        return message;
    }

    public void setMessage(CouponsMessage message) {
        this.message = message;
    }

}
