package com.thegirafe.myfoot.coupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CouponsMessage {
    @SerializedName("data")
    @Expose
    private List<CouponsData> data = null;

    public List<CouponsData> getData() {
        return data;
    }

    public void setData(List<CouponsData> data) {
        this.data = data;
    }


}
