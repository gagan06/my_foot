package com.thegirafe.myfoot.coupons;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckCouponResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private CheckCouponMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CheckCouponMessage getMessage() {
        return message;
    }

    public void setMessage(CheckCouponMessage message) {
        this.message = message;
    }
}
