package com.thegirafe.myfoot.Interfaces;

import com.thegirafe.myfoot.Address.DeleteAddressResponse;
import com.thegirafe.myfoot.Address.ShowShippingAddressResponse;
import com.thegirafe.myfoot.Address.StoreShippingAddressResponse;
import com.thegirafe.myfoot.Attributes.ColorResponse;
import com.thegirafe.myfoot.Attributes.SimpleColorResponse;
import com.thegirafe.myfoot.Attributes.SizeResponse;
import com.thegirafe.myfoot.Cart.AddCartResponse;
import com.thegirafe.myfoot.Cart.CartCountResponse;
import com.thegirafe.myfoot.Cart.MyCartResponse;
import com.thegirafe.myfoot.Cart.RemoveCartResponse;
import com.thegirafe.myfoot.Cart.UpdateCartResponse;
import com.thegirafe.myfoot.Categories.BrandsResponse;
import com.thegirafe.myfoot.Categories.CategoryResponse;
import com.thegirafe.myfoot.FeaturedProducts.FeaturedProductResponse;
import com.thegirafe.myfoot.Filters.FilterColorResponse;
import com.thegirafe.myfoot.Filters.FiltersResponse;
import com.thegirafe.myfoot.Guest.AddGuetsCart;
import com.thegirafe.myfoot.Guest.GuestUserModel;
import com.thegirafe.myfoot.Guest.GuestUserModelCategory;
import com.thegirafe.myfoot.Guest.GuestUserProductDetail;
import com.thegirafe.myfoot.Guest.GuestUserResponse;
import com.thegirafe.myfoot.Models.AddCartModel;
import com.thegirafe.myfoot.Offers.OfferPostData;
import com.thegirafe.myfoot.Offers.OfferProductResponse;
import com.thegirafe.myfoot.Offers.OffersSliderResponse;
import com.thegirafe.myfoot.Order.CancelOrderResponse;
import com.thegirafe.myfoot.Order.MyOrderResponse;
import com.thegirafe.myfoot.Order.OrderDetailResponse;
import com.thegirafe.myfoot.Order.OrderResponse;
import com.thegirafe.myfoot.Order.OrderStatusResponse;
import com.thegirafe.myfoot.OtherClasses.OtherResponse;
import com.thegirafe.myfoot.PaymentMethods.PaymentMethodResponse;
import com.thegirafe.myfoot.PostModel.ActivateUserModel;
import com.thegirafe.myfoot.PostModel.AddAddressModel;
import com.thegirafe.myfoot.PostModel.AddReviewModel;
import com.thegirafe.myfoot.PostModel.AddressModel;
import com.thegirafe.myfoot.PostModel.BrandModel;
import com.thegirafe.myfoot.PostModel.CancelOrderPost;
import com.thegirafe.myfoot.PostModel.CityModel;
import com.thegirafe.myfoot.PostModel.FacebookModel;
import com.thegirafe.myfoot.PostModel.FilterModel;
import com.thegirafe.myfoot.PostModel.FilterModelGuest;
import com.thegirafe.myfoot.PostModel.ForgetPasswordModel;
import com.thegirafe.myfoot.PostModel.GuestCartModel;
import com.thegirafe.myfoot.PostModel.LoginModel;
import com.thegirafe.myfoot.PostModel.OrderModel;
import com.thegirafe.myfoot.PostModel.OrderOnlineModel;
import com.thegirafe.myfoot.PostModel.OrderStatusModel;
import com.thegirafe.myfoot.PostModel.PopularityModelGuest;
import com.thegirafe.myfoot.PostModel.PostCatgeory;
import com.thegirafe.myfoot.PostModel.PriceModel;
import com.thegirafe.myfoot.PostModel.PriceModelGuest;
import com.thegirafe.myfoot.PostModel.ProductColorChooseModel;
import com.thegirafe.myfoot.PostModel.ProductDearchPostGuest;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.PostModel.ProductSearchPost;
import com.thegirafe.myfoot.PostModel.RegisterModel;
import com.thegirafe.myfoot.PostModel.ResendOtpModel;
import com.thegirafe.myfoot.PostModel.ResetPassModel;
import com.thegirafe.myfoot.PostModel.ResetPasswordModel;
import com.thegirafe.myfoot.PostModel.StateModel;
import com.thegirafe.myfoot.PostModel.UpdateCartModel;
import com.thegirafe.myfoot.PostModel.UpdateGuestCart;
import com.thegirafe.myfoot.PostModel.UpdateProfile;
import com.thegirafe.myfoot.Products.CategoryProductResponse;
import com.thegirafe.myfoot.Products.LatestProductResponse;
import com.thegirafe.myfoot.Products.LikeProductResponse;
import com.thegirafe.myfoot.Products.ProductColorChooseResponse;
import com.thegirafe.myfoot.Products.ProductDetailResponse;
import com.thegirafe.myfoot.Products.ProductNameResponse;
import com.thegirafe.myfoot.Products.ProductSearchResponse;
import com.thegirafe.myfoot.Reviews.AddReviewResponse;
import com.thegirafe.myfoot.Reviews.ReviewResponse;
import com.thegirafe.myfoot.SATECITY.CityResponse;
import com.thegirafe.myfoot.SATECITY.StateResponse;
import com.thegirafe.myfoot.SocialResponse.FbResponse;
import com.thegirafe.myfoot.User.ActivateUserResponse;
import com.thegirafe.myfoot.User.ForgetPasswordResponse;
import com.thegirafe.myfoot.User.LoginResponse;
import com.thegirafe.myfoot.User.ProfileImageResponse;
import com.thegirafe.myfoot.User.RegsiterResponse;
import com.thegirafe.myfoot.User.ResendOtpResponse;
import com.thegirafe.myfoot.User.ResetPassResponse;
import com.thegirafe.myfoot.User.ResetPasswordResponse;
import com.thegirafe.myfoot.User.UserProfileData;
import com.thegirafe.myfoot.User.UserProfileResponse;
import com.thegirafe.myfoot.Wishlist.CheckWishlistResponse;
import com.thegirafe.myfoot.Wishlist.MyWishListResponse;
import com.thegirafe.myfoot.Wishlist.WishlistResponse;
import com.thegirafe.myfoot.coupons.CheckCouponMessage;
import com.thegirafe.myfoot.coupons.CheckCouponPost;
import com.thegirafe.myfoot.coupons.CheckCouponResponse;
import com.thegirafe.myfoot.coupons.CheckPincodePost;
import com.thegirafe.myfoot.coupons.CouponsResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @POST("login")
    Call<LoginResponse> login(@Header("Authorization") String authkey, @Body LoginModel data);

    @POST("register")
    Call<RegsiterResponse> register(@Body RegisterModel body);

    @POST("resend-sms-code")
    Call<ResendOtpResponse> resendotp(@Body ResendOtpModel body);

    @POST("activate-user")
    Call<ActivateUserResponse> activateUser(@Body ActivateUserModel body);

    @POST("login-fb")
    Call<FbResponse> fblogin(@Body FacebookModel data);

    @POST("login-google")
    Call<FbResponse> googlelogin(@Body FacebookModel data);

    @POST("user-profile")
    Call<UserProfileResponse> user_profile(@Header("Authorization") String authkey);


    @POST("forgot-password")
    Call<ForgetPasswordResponse> forget_password(@Body ForgetPasswordModel data);
//
    @POST("reset-password")
    Call<ResetPasswordResponse> reset_password(@Body ResetPasswordModel data);

    @POST("get-all-products")
    Call<LatestProductResponse> get_all_products(@Header("Authorization") String authkey);
//
    @POST("get-categories")
    Call<CategoryResponse> get_categories(@Header("Authorization") String authkey);

    @POST("colors")
    Call<FilterColorResponse> get_colors(@Header("Authorization") String authkey);

    @POST("get-brands")
    Call<BrandsResponse> get_brands(@Header("Authorization") String authkey);

    @POST("brands")
    Call<BrandsResponse> get_brandscategory(@Header("Authorization") String authkey, @Body BrandModel data);

    @POST("colors-categorized")
    Call<FilterColorResponse> get_colorscategory(@Header("Authorization") String authkey, @Body BrandModel data);

    @POST("add-to-wishlist")
    Call<WishlistResponse> wishlist(@Header("Authorization") String authkey, @Body ProductDetailModel data);

    @POST("my-favourites")
    Call<MyWishListResponse> mywishlist(@Header("Authorization") String authkey);


    @POST("get-all-featured-products")
    Call<FeaturedProductResponse> getFeaturedProducts(@Header("Authorization") String authkey);


    @POST("get-all-featured-products-guest")
    Call<FeaturedProductResponse> getFeaturedProductsGuest(@Body GuestUserModel data);

    @POST("get-all-products-category")
    Call<CategoryProductResponse> getcategoryproduct(@Header("Authorization") String authkey, @Body PostCatgeory data);

    @POST("get-all-products-category-guest")
    Call<CategoryProductResponse> getcategoryproductguest(@Body GuestUserModelCategory data);
//
    @POST("get-product-detail")
    Call<ProductDetailResponse> get_product_detail(@Header("Authorization") String authkey, @Body ProductDetailModel data);

    @POST("get-product-detail-guest")
    Call<ProductDetailResponse> get_product_detailguest(@Body GuestUserProductDetail data);

    @POST("get_images")
    Call<ProductColorChooseResponse> get_images(@Header("Authorization") String authkey, @Body ProductColorChooseModel data);

    @POST("price")
    Call<CategoryProductResponse> price(@Header("Authorization") String authkey, @Body PriceModel data);

    @POST("price-guest")
    Call<CategoryProductResponse> priceGuest(@Body PriceModelGuest data);

    @POST("popularity")
    Call<CategoryProductResponse> popularity(@Header("Authorization") String authkey, @Body PostCatgeory data);

    @POST("popularity-guest")
    Call<CategoryProductResponse> popularity_guest(@Body PopularityModelGuest data);
    //
    @POST("get-all-review")
    Call<ReviewResponse> get_reviews(@Header("Authorization") String authkey, @Body ProductDetailModel data);
//

    @POST("add-to-cart")
    Call<AddCartResponse> add_to_cart(@Header("Authorization") String authkey, @Body AddCartModel body);

    @POST("add-to-guest-cart")
    Call<AddCartResponse> add_to_cartguest(@Body AddGuetsCart body);

    @POST("remove-from-cart")
    Call<RemoveCartResponse> remove_from_cart(@Header("Authorization") String authkey, @Body ProductDetailModel body);

    @POST("remove-from-guest-cart")
    Call<RemoveCartResponse> remove_from_cartguest(@Header("Authorization") String authkey, @Body UpdateGuestCart body);

    @POST("update-to-cart")
    Call<UpdateCartResponse> update_to_cart(@Header("Authorization") String authkey, @Body UpdateCartModel body);

    @POST("update-to-guestcart")
    Call<UpdateCartResponse> update_to_cartguest(@Header("Authorization") String authkey, @Body UpdateGuestCart body);


    @POST("get-cart")
    Call<MyCartResponse> get_cart(@Header("Authorization") String authkey);

    @POST("get-cart-count")
    Call<CartCountResponse> get_cart_count(@Header("Authorization") String authkey);

    @POST("get-guest-count")
    Call<CartCountResponse> get_guest_count(@Body GuestUserModel body);

    @POST("check-wishlist")
    Call<CheckWishlistResponse> check_wish(@Header("Authorization") String authkey, @Body ProductDetailModel body);

    @POST("check-cart")
    Call<CheckWishlistResponse> check_cart(@Header("Authorization") String authkey, @Body ProductDetailModel body);

    @POST("check-guest-cart")
    Call<CheckWishlistResponse> check_cart_guest(@Body GuestCartModel body);

    @POST("get-guest-cart")
    Call<MyCartResponse> get_guest_cart(@Body GuestUserModel body);

    @POST("add-review")
    Call<AddReviewResponse> add_review(@Header("Authorization") String authkey, @Body AddReviewModel body);
//
    @POST("states")
    Call<StateResponse> get_states(@Header("Authorization") String authkey, @Body StateModel data);

    @POST("city")
    Call<CityResponse> get_city(@Header("Authorization") String authkey, @Body CityModel data);
//
    @POST("store_shippingaddress")
    Call<StoreShippingAddressResponse> store_shippingaddress(@Header("Authorization") String authkey, @Body AddAddressModel data);

    @POST("store_billingaddress")
    Call<StoreShippingAddressResponse> store_billingaddress(@Header("Authorization") String authkey, @Body AddAddressModel data);

    @POST("order")
    Call<OrderResponse> order(@Header("Authorization") String authkey, @Body OrderModel data);

    @POST("order-online")
    Call<OrderResponse> order_online(@Header("Authorization") String authkey, @Body OrderOnlineModel data);

    @POST("my-orders")
    Call<MyOrderResponse> my_orders(@Header("Authorization") String authkey);

    @POST("order-status")
    Call<OrderStatusResponse> order_status(@Header("Authorization") String authkey, @Body OrderStatusModel data);

//
    @POST("get-shipping")
    Call<ShowShippingAddressResponse> get_address(@Header("Authorization") String authkey);

    @POST("get-billing")
    Call<ShowShippingAddressResponse> get_billingaddress(@Header("Authorization") String authkey);

    @POST("payment-methods")
    Call<PaymentMethodResponse> get_payments(@Header("Authorization") String authkey);

    @POST("delete_shippingaddress")
    Call<DeleteAddressResponse> delete_address(@Header("Authorization") String authkey, @Body AddressModel body);

    @POST("delete_billingaddress")
    Call<DeleteAddressResponse> delete_billingaddress(@Header("Authorization") String authkey, @Body AddressModel body);

    @POST("like-product")
    Call<LikeProductResponse> like(@Header("Authorization") String authkey, @Body ProductDetailModel body);

    @POST("filter")
    Call<FiltersResponse> filters(@Header("Authorization") String authkey, @Body FilterModel body);

    @POST("filter-guest")
    Call<FiltersResponse> filters_guest(@Header("Authorization") String authkey, @Body FilterModelGuest body);

    @POST("register-guest")
    Call<GuestUserResponse> register_guest(@Body GuestUserModel body);

    @POST("update-user-profile")
    Call<UserProfileResponse> update_user_profile(@Header("Authorization") String authkey, @Body UpdateProfile body);

    @POST("change-password")
    Call<ResetPassResponse> change(@Header("Authorization") String authkey, @Body ResetPassModel body);

    @POST("size")
    Call<SizeResponse> size(@Header("Authorization") String authkey, @Body ProductDetailModel data);

    @POST("color")
    Call<ColorResponse> color(@Header("Authorization") String authkey, @Body ProductDetailModel data);

    @POST("simple-color")
    Call<SimpleColorResponse> simple_color(@Header("Authorization") String authkey, @Body ProductDetailModel data);

    @Multipart
    @POST("update-user-profile-image")
    Call<ProfileImageResponse> update_profile_image(@Header("Authorization") String authkey, @Part MultipartBody.Part profile_image);


    @POST("get-coupons")
    Call<CouponsResponse> get_coupons(@Header("Authorization") String authkey);

    @POST("get-offers")
    Call<OffersSliderResponse> get_offers();

    @POST("get-offers-products-guest")
    Call<OfferProductResponse> get_offersproducts(@Body OfferPostData data);

    @POST("get-offers-products")
    Call<OfferProductResponse> get_offersproducts(@Header("Authorization") String authkey, @Body OfferPostData data);

    @POST("check-coupon")
    Call<CheckCouponResponse> check_coupon(@Header("Authorization") String authkey, @Body CheckCouponPost data);

    @POST("check-pincode")
    Call<CheckCouponResponse> check_pincode(@Header("Authorization") String authkey, @Body CheckPincodePost data);

    @POST("my-orders-detail")
    Call<OrderDetailResponse> order_detail(@Header("Authorization") String authkey, @Body OrderStatusModel data);

    @POST("get-all-products-names")
    Call<ProductNameResponse> get_names();

    @POST("search-products-names-guest")
    Call<ProductSearchResponse> get_search(@Body ProductDearchPostGuest data);

    @POST("search-products-names")
    Call<ProductSearchResponse> get_search(@Header("Authorization") String authkey, @Body ProductSearchPost data);

    @POST("cancel-order")
    Call<CancelOrderResponse> cancel_order(@Header("Authorization") String authkey, @Body CancelOrderPost data);


    @POST("about-us")
    Call<OtherResponse> about_us();

    @POST("terms")
    Call<OtherResponse> terms();

}
