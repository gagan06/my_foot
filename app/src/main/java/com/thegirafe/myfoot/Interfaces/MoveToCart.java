package com.thegirafe.myfoot.Interfaces;

public interface MoveToCart {
    void addToCart(String s);
    void removeFromWishlist(String s);
    void gotocart();
}
