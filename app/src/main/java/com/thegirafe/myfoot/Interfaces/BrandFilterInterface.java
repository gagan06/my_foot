package com.thegirafe.myfoot.Interfaces;

import java.util.ArrayList;

public interface BrandFilterInterface {
    void getBrandsList(ArrayList<String> s);
    void getColorsList(ArrayList<String> s);
}
