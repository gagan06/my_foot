package com.thegirafe.myfoot.Interfaces;

public interface BillingInterface {
    void deleteBillingAddress(String s);
    void selectBillingAddress(String s, String zip, String mobile);
}
