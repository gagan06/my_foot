package com.thegirafe.myfoot.Interfaces;

public interface MoveToWishlist {
    void movetowish(String s);
    void removerfromcart(String s);
    void addToCart(String s, String q);
    void sendError(String s);
    void gotowishlist();
}
