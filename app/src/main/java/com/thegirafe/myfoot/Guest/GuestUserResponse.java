package com.thegirafe.myfoot.Guest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuestUserResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private GuestUserMesssage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public GuestUserMesssage getMessage() {
        return message;
    }

    public void setMessage(GuestUserMesssage message) {
        this.message = message;
    }
}
