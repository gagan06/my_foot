package com.thegirafe.myfoot.Guest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuestUserMesssage {

    @SerializedName("data")
    @Expose
    private GuestUserData data;

    public GuestUserData getData() {
        return data;
    }

    public void setData(GuestUserData data) {
        this.data = data;
    }
}
