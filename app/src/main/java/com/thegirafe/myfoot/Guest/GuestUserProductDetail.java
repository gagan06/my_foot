package com.thegirafe.myfoot.Guest;

public class GuestUserProductDetail {
    String access_id, category_id, product_id;

    public GuestUserProductDetail(String access_id, String product_id) {
        this.access_id = access_id;
        this.product_id = product_id;
    }
}
