package com.thegirafe.myfoot.Guest;

public class AddGuetsCart {
    String quantity;
    String product_id;
    String color;
    String size;
    String access_id;

    public AddGuetsCart(String quantity, String product_id, String color, String size, String access_id) {
        this.quantity = quantity;
        this.product_id = product_id;
        this.color = color;
        this.size = size;
        this.access_id = access_id;
    }
}
