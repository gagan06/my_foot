package com.thegirafe.myfoot.PostModel;

public class FacebookModel {
    String name, email, profile_image, access_token;

    public FacebookModel(String name, String email, String profile_image, String access_token) {
        this.name = name;
        this.email = email;
        this.profile_image = profile_image;
        this.access_token = access_token;
    }
}
