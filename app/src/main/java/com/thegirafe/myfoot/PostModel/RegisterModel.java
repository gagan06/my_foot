package com.thegirafe.myfoot.PostModel;

public class RegisterModel {
    String name, email, mobile_no, password;

    public RegisterModel(String name, String email, String mobile_no, String password) {
        this.name = name;
        this.email = email;
        this.mobile_no = mobile_no;
        this.password = password;
    }
}
