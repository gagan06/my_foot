package com.thegirafe.myfoot.PostModel;

public class PriceModel {
    String category_id;
    String price;

    public PriceModel(String category_id, String price) {
        this.category_id = category_id;
        this.price = price;
    }
}
