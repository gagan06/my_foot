package com.thegirafe.myfoot.PostModel;

public class LoginModel {
    String device_type;
    String device_token;
    String access_id;

    public LoginModel(String device_type, String device_token, String access_id) {
        this.device_type = device_type;
        this.device_token = device_token;
        this.access_id = access_id;
    }
}
