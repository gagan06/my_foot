package com.thegirafe.myfoot.PostModel;

public class UpdateProfile {
    String name, email, mobile_no;

    public UpdateProfile(String name, String email, String mobile_no) {
        this.name = name;
        this.email = email;
        this.mobile_no = mobile_no;
    }
}
