package com.thegirafe.myfoot.PostModel;

public class AddReviewModel {
    String product_id, review, rating;

    public AddReviewModel(String product_id, String review, String rating) {
        this.product_id = product_id;
        this.review = review;
        this.rating = rating;
    }
}
