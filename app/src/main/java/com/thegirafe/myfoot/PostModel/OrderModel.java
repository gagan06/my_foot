package com.thegirafe.myfoot.PostModel;

public class OrderModel {
    String shipping_id, coupon_applied, payment_method, billing_id, coupon_id;

    public OrderModel(String shipping_id, String coupon_applied, String payment_method, String billing_id, String coupon_id) {
        this.shipping_id = shipping_id;
        this.coupon_applied = coupon_applied;
        this.payment_method = payment_method;
        this.billing_id = billing_id;
        this.coupon_id = coupon_id;
    }
}
