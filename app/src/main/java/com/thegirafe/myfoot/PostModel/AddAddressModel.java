package com.thegirafe.myfoot.PostModel;

public class AddAddressModel {
    String address, state, city, zip, name, phone, landmark;

    public AddAddressModel(String address, String state, String city, String zip, String name, String phone, String landmark) {
        this.address = address;
        this.state = state;
        this.city = city;
        this.zip = zip;
        this.name = name;
        this.phone = phone;
        this.landmark = landmark;
    }
}
