package com.thegirafe.myfoot.PostModel;

public class ProductColorChooseModel {
    String product_id;
    String attribute_value_id;

    public ProductColorChooseModel(String product_id, String attribute_value_id) {
        this.product_id = product_id;
        this.attribute_value_id = attribute_value_id;
    }
}
