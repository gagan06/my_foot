package com.thegirafe.myfoot.PostModel;

public class OrderOnlineModel {
    String shipping_id, coupon_applied, payment_method, billing_id;
    String order_date, order_status, transaction_id, coupon_id;

    public OrderOnlineModel(String shipping_id, String coupon_applied, String payment_method, String order_date, String order_status, String transaction_id, String billing_id, String coupon_id) {
        this.shipping_id = shipping_id;
        this.coupon_applied = coupon_applied;
        this.payment_method = payment_method;
        this.order_date = order_date;
        this.order_status = order_status;
        this.transaction_id = transaction_id;
        this.billing_id = billing_id;
        this.coupon_id = coupon_id;
    }
}
