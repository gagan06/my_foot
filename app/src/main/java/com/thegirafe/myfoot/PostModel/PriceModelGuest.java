package com.thegirafe.myfoot.PostModel;

public class PriceModelGuest {
    String category_id;
    String price;
    String access_id;

    public PriceModelGuest(String category_id, String price, String access_id) {
        this.category_id = category_id;
        this.price = price;
        this.access_id = access_id;
    }
}
