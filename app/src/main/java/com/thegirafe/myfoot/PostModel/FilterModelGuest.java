package com.thegirafe.myfoot.PostModel;

import java.util.List;

public class FilterModelGuest {
    String category_id;
    List<String> brand_id;
    List<String> color_id;
    String access_id;

    public FilterModelGuest(String category_id, List<String> brand_id, List<String> color_id, String access_id) {
        this.category_id = category_id;
        this.brand_id = brand_id;
        this.color_id = color_id;
        this.access_id = access_id;
    }
}
