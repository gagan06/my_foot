package com.thegirafe.myfoot.PostModel;

public class UpdateCartModel {
    String product_id, quantity;

    public UpdateCartModel(String product_id, String quantity) {
        this.product_id = product_id;
        this.quantity = quantity;
    }
}
