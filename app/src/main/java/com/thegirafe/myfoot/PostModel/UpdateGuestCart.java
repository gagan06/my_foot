package com.thegirafe.myfoot.PostModel;

public class UpdateGuestCart {
    String product_id, quantity, access_id;

    public UpdateGuestCart(String product_id, String quantity, String access_id) {
        this.product_id = product_id;
        this.quantity = quantity;
        this.access_id = access_id;
    }
}
