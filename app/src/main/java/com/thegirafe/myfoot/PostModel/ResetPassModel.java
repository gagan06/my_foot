package com.thegirafe.myfoot.PostModel;

public class ResetPassModel {
    String password, new_password;

    public ResetPassModel(String password, String new_password) {
        this.password = password;
        this.new_password = new_password;
    }
}
