package com.thegirafe.myfoot.PostModel;

import java.util.List;

public class FilterModel {
    String category_id;
    List<String> brand_id;
    List<String> color_id;

    public FilterModel(String category_id, List<String> brand_id, List<String> color_id) {
        this.category_id = category_id;
        this.brand_id = brand_id;
        this.color_id = color_id;
    }
}