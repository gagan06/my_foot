package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 7/2/2018.
 */
public class ProductReviewsModel {
    String id;
    String userName;
    String imageUrl;
    String date;
    String revew;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRevew() {
        return revew;
    }

    public void setRevew(String revew) {
        this.revew = revew;
    }
}
