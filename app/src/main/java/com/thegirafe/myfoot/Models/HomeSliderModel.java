package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 6/21/2018.
 */
public class HomeSliderModel {

    String imageUrl;
    String id;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
