package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class ColorFilterModel {
    String colorNmae;
    boolean status;
    String colorShade;
    String id;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getColorNmae() {
        return colorNmae;
    }

    public void setColorNmae(String colorNmae) {
        this.colorNmae = colorNmae;
    }


    public String getColorShade() {
        return colorShade;
    }

    public void setColorShade(String colorShade) {
        this.colorShade = colorShade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
