package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class SizeFilterModel {
    String sizeNmae;
    boolean status;
    String id;

    public String getSizeNmae() {
        return sizeNmae;
    }

    public void setSizeNmae(String sizeNmae) {
        this.sizeNmae = sizeNmae;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
