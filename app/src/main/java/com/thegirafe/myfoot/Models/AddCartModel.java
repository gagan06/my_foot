package com.thegirafe.myfoot.Models;

public class AddCartModel {
    String quantity;
    String product_id;
    String color;
    String size;

    public AddCartModel(String quantity, String product_id, String color, String size) {
        this.quantity = quantity;
        this.product_id = product_id;
        this.color = color;
        this.size = size;
    }
}
