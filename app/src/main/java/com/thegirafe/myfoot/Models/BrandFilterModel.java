package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class BrandFilterModel {
    String brandName;
    boolean status;
    String brandIcon;
    String id;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandIcon() {
        return brandIcon;
    }

    public void setBrandIcon(String brandIcon) {
        this.brandIcon = brandIcon;
    }
}
