package com.thegirafe.myfoot.Models;

/**
 * Created by ${Gagandeep} on 6/22/2018.
 */
public class HomeRecentViewModel {
    String id;
    String imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
