package com.thegirafe.myfoot.SATECITY;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityMessage {
    @SerializedName("data")
    @Expose
    private List<CityData> data;

    public CityMessage() {
        data = null;
    }

    public List<CityData> getData() {
        return data;
    }

    public void setData(List<CityData> data) {
        this.data = data;
    }
}
