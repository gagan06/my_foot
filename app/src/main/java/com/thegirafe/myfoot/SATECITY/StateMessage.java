package com.thegirafe.myfoot.SATECITY;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateMessage {

    @SerializedName("data")
    @Expose
    private List<StateData> data = null;

    public List<StateData> getData() {
        return data;
    }

    public void setData(List<StateData> data) {
        this.data = data;
    }
}
