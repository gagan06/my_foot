package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thegirafe.myfoot.Activities.CartProduct;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.ItemClickListener;
import com.thegirafe.myfoot.Interfaces.MoveToCart;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Models.WishlistModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Wishlist.CheckWishlistResponse;
import com.thegirafe.myfoot.Wishlist.MyWishListData;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ${Gagandeep} on 6/23/2018.
 */
public class WishlistAdapter extends RecyclerView.Adapter {
    private ArrayList<MyWishListData> data = new ArrayList<>();
    private Context context;
    private LayoutInflater li;
    RequestOptions requestOptions;
    private ItemClickListener clickListener;
    MoveToCart moveToCart;
    SharedPreferences sharedPreferences;
    TextView stock, move_to_cart;


    public WishlistAdapter(ArrayList<MyWishListData> data, Context context) {
        this.data=data;
        this.context=context;
        li=LayoutInflater.from(context);
        this.moveToCart = ((MoveToCart) context);
        sharedPreferences = context.getSharedPreferences(MyConstants.MyPref, Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v=li.inflate(R.layout.card_for_wishlist_item,parent,false);
        listHolder holder=new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final MyWishListData current=data.get(position);
        final listHolder myHolder=(listHolder)holder;

        if (current.getSubProducts().size() == 0){
            myHolder.discountPriseTV.setText(current.getSalePrice());
            myHolder.productActualPriseTV.setText(current.getListPrice());
            if (current.getSalePrice() == null){
                myHolder.discountPriseTV.setText(current.getListPrice());
                myHolder.price.setVisibility(View.GONE);
            }
            myHolder.product_name.setText(current.getProductName());
            myHolder.delivery_detail.setText(current.getProductDeliveryDetail());
            myHolder.sold_by.setText(current.getProductSoldBy());
            checkCart(current.getProductId());
            myHolder.moveToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (current.getQuantity().equalsIgnoreCase("0")){
                        stock.setVisibility(View.VISIBLE);
                    }else{
                        if (move_to_cart.getText().toString().equalsIgnoreCase("Move to cart")){
                            moveToCart.addToCart(current.getProductId());
                        }else if(move_to_cart.getText().toString().equalsIgnoreCase("Go to cart")){
                            moveToCart.gotocart();
                        }
                        stock.setVisibility(View.GONE);
                    }
                }
            });
        }else{
            myHolder.discountPriseTV.setText(current.getSubProducts().get(0).getProductSalePrice());
            myHolder.productActualPriseTV.setText(current.getSubProducts().get(0).getProductOriginalPrice());
            myHolder.product_name.setText(current.getSubProducts().get(0).getProductName());
            myHolder.delivery_detail.setText(current.getSubProducts().get(0).getProductDeliveryDetail());
            myHolder.sold_by.setText(current.getSubProducts().get(0).getProductSoldBy());
            checkCart(current.getProductId());

            myHolder.moveToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (current.getSubProducts().get(0).getProductQuantity().equalsIgnoreCase("0")){
                        stock.setVisibility(View.VISIBLE);
                    }else{
                        if (move_to_cart.getText().toString().equalsIgnoreCase("Move to cart")){
                            moveToCart.addToCart(current.getProductId());
                        }else if(move_to_cart.getText().toString().equalsIgnoreCase("Go to cart")){
                            moveToCart.gotocart();
                        }
                        stock.setVisibility(View.GONE);
                    }
                }
            });
        }



        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + current.getFeaturedImage()))
                .into(myHolder.product_image);



        myHolder.removeFromWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToCart.removeFromWishlist(current.getProductId());
            }
        });




    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class listHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView product_image;
        TextView productActualPriseTV,discountPriseTV, product_name, sold_by, delivery_detail;
        LinearLayout moveToCart,removeFromWishlist, price;

        public listHolder(View v) {
            super(v);
            productActualPriseTV=v.findViewById(R.id.productActualPriseTV);
            sold_by=v.findViewById(R.id.sold_by);
            product_image=v.findViewById(R.id.product_image);
            delivery_detail=v.findViewById(R.id.delivery_detail);
            product_name=v.findViewById(R.id.product_name);
            discountPriseTV=v.findViewById(R.id.discountPriseTV);
            moveToCart=v.findViewById(R.id.moveToCart);
            removeFromWishlist=v.findViewById(R.id.removeFromWishlist);
            stock=v.findViewById(R.id.stock);
            move_to_cart = v.findViewById(R.id.move_to_cart);
            price = v.findViewById(R.id.price);
        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


    public void checkCart(String productid){

        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);
        Log.e(MyConstants.TAG, "PRODUCT CART CHECK "+productid);
        ProductDetailModel productDetailModel =  new ProductDetailModel(productid);
        Call<CheckWishlistResponse> call = api.check_cart(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<CheckWishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<CheckWishlistResponse> call, Response<CheckWishlistResponse> response) {
                if (response.isSuccessful()){
                    Log.e(MyConstants.TAG, "Cart "+response.body().getMessage().getSuccess());
                    if (response.body().getSuccess()){
                        move_to_cart.setText("Go to cart");
                    }else {
                        move_to_cart.setText("Move to cart");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CheckWishlistResponse> call, Throwable t) {

            }
        });
    }

}
