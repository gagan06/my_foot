package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.thegirafe.myfoot.Activities.ProductDetail;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.CategoryProductData;
import com.thegirafe.myfoot.Products.ProductSearchData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class SearchProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<ProductSearchData> productData = new ArrayList<>();
    Context context;
    LayoutInflater li;
    RequestOptions requestOptions;

    public SearchProductListAdapter(ArrayList<ProductSearchData> productData, Context context) {
        this.context=context;
        this.productData=productData;
        li=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_product_item, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final listHolder myHolder = (listHolder) holder;
        final ProductSearchData current = productData.get(position);

        if(current.getSalePrice() == null){
            myHolder.productActualPrise.setText(current.getSalePrice());
            myHolder.price.setVisibility(View.GONE);
        }else{
            myHolder.productActualPrise.setText(current.getPrice());
            myHolder.productDiscountPrice.setText(current.getSalePrice());
        }

            myHolder.productName.setText(current.getName());
            requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

            int like_user = current.getUser_liked();
            if (like_user == 1){
                myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            }else{
                myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
            }
            Glide.with(context)
                    .load(Uri.parse(MyConstants.IMAGE_PATH + current.getImage()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(((listHolder) holder).product_image);

        myHolder.product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(MyConstants.TAG, "id "+current.getId());
                Intent intent = new Intent(context, ProductDetail.class);
                intent.putExtra(MyConstants.Product_id, ""+current.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView productName, productDiscountPrice,productActualPrise;
        RelativeLayout product;
        LinearLayout price;
        ImageButton addToFav;
        ProgressBar progress;

        public listHolder(View v) {
            super(v);
            product_image = v.findViewById(R.id.product_image);
            productName = v.findViewById(R.id.productName);
            productDiscountPrice = v.findViewById(R.id.productDiscountPrice);
            productActualPrise = v.findViewById(R.id.productActualPrise);
            product = v.findViewById(R.id.product);
            addToFav = v.findViewById(R.id.addToFav);
            price = v.findViewById(R.id.price);
            progress = v.findViewById(R.id.progress);
        }
    }
}
