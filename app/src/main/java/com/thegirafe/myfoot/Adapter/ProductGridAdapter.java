package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.CategoryProductData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 7/2/2018.
 */
public class ProductGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<CategoryProductData> productData = new ArrayList<>();
    Context context;
    LayoutInflater li;
    RequestOptions requestOptions;

    public ProductGridAdapter(ArrayList<CategoryProductData> productData, Context context) {
        this.context = context;
        this.productData = productData;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_product_item, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        listHolder myHolder = (listHolder) holder;
        final CategoryProductData current = productData.get(position);
        myHolder.productActualPrise.setText(current.getProductOriginalPrice().toString());
        myHolder.productDiscountPrice.setText(current.getProductSalePrice().toString());
        myHolder.productName.setText(current.getProductName());
        requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + current.getFeaturedImage()))
                .apply(requestOptions
                        .skipMemoryCache(true)
                        .placeholder(R.color.Black))
                .into(((listHolder) holder).product_image);
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView productName, productDiscountPrice, productActualPrise;

        public listHolder(View v) {
            super(v);
            product_image = v.findViewById(R.id.product_image);
            productName = v.findViewById(R.id.productName);
            productDiscountPrice = v.findViewById(R.id.productDiscountPrice);
            productActualPrise = v.findViewById(R.id.productActualPrise);

        }
    }
}
