package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thegirafe.myfoot.Activities.OrderDetail;
import com.thegirafe.myfoot.Order.MyOrderData;
import com.thegirafe.myfoot.Order.OrderDetailProduct;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class OrderProductRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<OrderDetailProduct> data = new ArrayList<>();
    LayoutInflater li;

    public OrderProductRecyclerAdapter(ArrayList<OrderDetailProduct> data, Context context) {
        this.context = context;
        this.data = data;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.order_product_view, parent, false);
        listHolder holder = new listHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final OrderDetailProduct current = data.get(position);
        listHolder myHolder = (listHolder) holder;
        myHolder.quantity.setText("Qty: "+current.getQuantity());
        myHolder.product_name.setText(current.getProduct().getProductName());
        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + current.getProduct().getProduct_image()))
                .into(myHolder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        TextView  quantity, product_name;
        ImageView image;

        public listHolder(View v) {
            super(v);
            image = v.findViewById(R.id.image);
            quantity = v.findViewById(R.id.quantity);
            product_name = v.findViewById(R.id.product_name);
        }
    }

}