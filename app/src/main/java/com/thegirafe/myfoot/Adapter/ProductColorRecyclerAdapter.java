package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thegirafe.myfoot.Interfaces.ColorInterface;
import com.thegirafe.myfoot.Interfaces.SizeColorInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.ProductColorChooseData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class ProductColorRecyclerAdapter extends RecyclerView.Adapter<ProductColorRecyclerAdapter.ViewHolder> {

    private ArrayList<ProductColorChooseData> productColors;
    private Context context;
    private ViewHolder viewHolder;
    private onRecyclerViewItemClickListener mItemClickListener;
    ColorInterface colorInterface;
    int selectedPosition=-1;

    public ProductColorRecyclerAdapter(Context context, ArrayList<ProductColorChooseData> productColors, String product_id) {
        super();
        this.context = context;
        this.productColors = productColors;
        this.colorInterface = ((ColorInterface) context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_for_color, viewGroup, false);
        viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        final ProductColorChooseData product = productColors.get(i);
        final RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
        Log.e(MyConstants.TAG, "product image "+product.getAttribute_value_id()+product.getProductId());
        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + product.getProductImage()))
                .into(viewHolder.imageView);

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               colorInterface.colorid(product.getAttribute_value_id().toString(), product.getProductId());
                selectedPosition = i;
                notifyDataSetChanged();
            }
        });

        if (selectedPosition == i){
            viewHolder.main.setBackground(context.getResources().getDrawable(R.drawable.dummy_image_border_slected));
        }
        else {
            viewHolder.main.setBackground(context.getResources().getDrawable(R.drawable.dummy_image_border));
        }



    }

    @Override
    public int getItemCount() {
        return productColors.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        RelativeLayout main;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView =  itemView.findViewById(R.id.coloredImage);
            main =  itemView.findViewById(R.id.main);
        }


    }

    public interface onRecyclerViewItemClickListener {
        void onItemClickListener(View view, int position);
    }

    public void setOnItemClickListener(onRecyclerViewItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
