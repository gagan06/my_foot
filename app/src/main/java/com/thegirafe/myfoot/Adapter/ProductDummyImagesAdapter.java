package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.thegirafe.myfoot.Interfaces.ColorInterface;
import com.thegirafe.myfoot.Interfaces.SizeColorInterface;
import com.thegirafe.myfoot.Interfaces.onItemClickListener;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.ProductColorChooseData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class ProductDummyImagesAdapter extends RecyclerView.Adapter<ProductDummyImagesAdapter.ViewHolder> {

    ArrayList<ProductColorChooseData> alImage;
    Context context;
    onItemClickListener mItemClickListener;

    public ProductDummyImagesAdapter(Context context, ArrayList<ProductColorChooseData> alImage) {
        super();
        this.context = context;
        this.alImage = alImage;
        this.mItemClickListener = ((onItemClickListener) context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dummy_image_view, viewGroup, false);
        ProductDummyImagesAdapter.ViewHolder viewHolder = new ProductDummyImagesAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProductDummyImagesAdapter.ViewHolder viewHolder, final int i) {
        final RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Log.e(MyConstants.TAG, "image "+alImage.get(i).getProductImage());

        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + alImage.get(i).getProductImage()))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        viewHolder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(viewHolder.imageView);

       viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.scroll(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return alImage.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        ProgressBar progress_bar;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            progress_bar = itemView.findViewById(R.id.progress_bar);


        }


    }


}
