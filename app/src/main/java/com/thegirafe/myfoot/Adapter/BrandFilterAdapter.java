package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thegirafe.myfoot.Categories.BrandsList;
import com.thegirafe.myfoot.Interfaces.BrandFilterInterface;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Models.BrandFilterModel;
import com.thegirafe.myfoot.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class BrandFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<BrandsList> data = new ArrayList<>();
    LayoutInflater li;
    ArrayList<String> stringArray = new ArrayList<>();
    BrandFilterInterface brandFilterInterface;

    public BrandFilterAdapter(ArrayList<BrandsList> data, Context context) {
        this.data = data;
        this.context = context;
        li = LayoutInflater.from(context);
        this.brandFilterInterface = ((BrandFilterInterface) context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_brand_filter, parent, false);
        ListHolder holder = new ListHolder(v);
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        final BrandsList current = data.get(position);
        final ListHolder myHolder = (ListHolder) holder;

        myHolder.brandName.setText(current.getBrand_name());

        myHolder.brandCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myHolder.brandCheckBox.isChecked()) {
                    myHolder.brandCheckBox.setChecked(true);
                    data.get(position).setStatus_new(true);
                    stringArray.add(data.get(position).getId().toString());
                    brandFilterInterface.getBrandsList(stringArray);
                } else {
                    myHolder.brandCheckBox.setChecked(false);
                    data.get(position).setStatus_new(false);
                    stringArray.remove(data.get(position).getId().toString());
                    brandFilterInterface.getBrandsList(stringArray);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListHolder extends RecyclerView.ViewHolder {

        TextView brandName;
        CheckBox brandCheckBox;

        public ListHolder(View v) {
            super(v);

            brandName = v.findViewById(R.id.brandName);
            brandCheckBox = v.findViewById(R.id.brandCheckBox);
        }
    }
}
