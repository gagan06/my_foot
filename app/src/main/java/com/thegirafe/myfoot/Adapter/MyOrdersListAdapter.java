package com.thegirafe.myfoot.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.thegirafe.myfoot.Activities.Home_Screen;
import com.thegirafe.myfoot.Activities.OrderDetail;
import com.thegirafe.myfoot.Activities.OrderStatus;
import com.thegirafe.myfoot.Interfaces.OrderInterface;
import com.thegirafe.myfoot.Models.MyOrdersListModel;
import com.thegirafe.myfoot.Order.MyOrderData;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/23/2018.
 */
public class MyOrdersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<MyOrderData> data = new ArrayList<>();
    LayoutInflater li;

    public MyOrdersListAdapter(ArrayList<MyOrderData> data, Context context) {
        this.context = context;
        this.data = data;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_myorders, parent, false);
        listHolder holder = new listHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final MyOrderData current = data.get(position);
        listHolder myHolder = (listHolder) holder;

        myHolder.orderId.setText(current.getOrderId());
        final Activity activity = (Activity) context;
        myHolder.DateTV.setText("Order Date: "+current.getOrderDate());
        myHolder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetail.class);
                intent.putExtra("Order", current.getId().toString());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        myHolder.product_name.setText(current.getProduct().get(0).getProduct().getName());
        Glide.with(context)
                   .load(Uri.parse(MyConstants.IMAGE_PATH + current.getProduct().get(0).getProduct().getImage()))
                .into(myHolder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        TextView orderId, DateTV, product_name;
        TextView orderStatus;
        ImageView image;
        LinearLayout mainView;

        public listHolder(View v) {
            super(v);
            orderId = v.findViewById(R.id.orderId);
            orderStatus = v.findViewById(R.id.orderStatus);
            DateTV = v.findViewById(R.id.DateTV);
            image = v.findViewById(R.id.image);
            product_name = v.findViewById(R.id.product_name);
            mainView = v.findViewById(R.id.mainView);
        }
    }
}
