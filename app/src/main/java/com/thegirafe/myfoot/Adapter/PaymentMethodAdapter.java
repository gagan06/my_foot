package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thegirafe.myfoot.Interfaces.Method_interface;
import com.thegirafe.myfoot.Order.OrderDetailProduct;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PaymentMethods.PaymentMethodData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class PaymentMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<PaymentMethodData> data = new ArrayList<>();
    LayoutInflater li;
    int selectedPosition=-1;
    Method_interface method_interface;

    public PaymentMethodAdapter(ArrayList<PaymentMethodData> data, Context context) {
        this.context = context;
        this.data = data;
        li = LayoutInflater.from(context);
        method_interface = ((Method_interface) context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.mehtod_view, parent, false);
        listHolder holder = new listHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final PaymentMethodData current = data.get(position);
        listHolder myHolder = (listHolder) holder;
        myHolder.mehtod_name.setText(current.getName());
        myHolder.mehtod_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
                method_interface.method(current.getId().toString(), current.getName());
            }
        });

        if (selectedPosition == position){
            myHolder.mehtod_call.setBackgroundColor(context.getResources().getColor(R.color.card_wish));
        }
        else {
            myHolder.mehtod_call.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        TextView mehtod_name;
        LinearLayout mehtod_call;

        public listHolder(View v) {
            super(v);
            mehtod_name = v.findViewById(R.id.mehtod_name);
            mehtod_call = v.findViewById(R.id.mehtod_call);
        }
    }

}
