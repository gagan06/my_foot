package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thegirafe.myfoot.Attributes.SizeData;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Interfaces.SizeColorInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.Size;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class ProductSizeRecyclerAdapter extends RecyclerView.Adapter<ProductSizeRecyclerAdapter.ViewHolder> {

    ArrayList<SizeData> productSizes;
    Context context;
    SizeColorInterface sizeColorInterface;
    int selectedPosition=-1;

    public ProductSizeRecyclerAdapter(Context context, ArrayList<SizeData> productSizes1) {
        super();
        this.context = context;
        this.productSizes = productSizes1;
        this.sizeColorInterface = ((SizeColorInterface) context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_for_size, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.e(MyConstants.TAG, "product size " + position);
        final SizeData product = productSizes.get(position);
        viewHolder.size_text.setText(product.getName());
        viewHolder.size_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                sizeColorInterface.select_size(product.getAttributeValueId());
                notifyDataSetChanged();
            }
        }); // bind the listener

        if (selectedPosition == position){
            viewHolder.circle.setBackground(context.getResources().getDrawable(R.drawable.circle_selected));
            viewHolder.size_text.setTextColor(context.getResources().getColor(R.color.colorSec));
        }
        else {
            viewHolder.circle.setBackground(context.getResources().getDrawable(R.drawable.circle_black));
            viewHolder.size_text.setTextColor(context.getResources().getColor(R.color.Black));
        }

    }

    @Override
    public int getItemCount() {
        return productSizes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView size_text;
        RelativeLayout circle;

        public ViewHolder(View itemView) {
            super(itemView);
            size_text =  itemView.findViewById(R.id.size);
            circle =  itemView.findViewById(R.id.circle);
        }


    }

}
