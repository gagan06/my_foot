package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.thegirafe.myfoot.Models.HomeProductListModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/22/2018.
 */
public class HomeMainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<HomeProductListModel> productData = new ArrayList<>();
    Context context;
    LayoutInflater li;

    public HomeMainListAdapter(ArrayList<HomeProductListModel> productData, Context context) {
        this.productData = productData;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        switch (viewType) {
            case 0:
                view=LayoutInflater.from(parent.getContext()).inflate(R.layout.card_for_home_advertisment, parent, false);
                return new advertismentViewType(view);

            case 1:
                view=LayoutInflater.from(parent.getContext()).inflate(R.layout.card_for_home_grid, parent, false);
                return new productViewType(view);
        }
       return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (productData.get(position).getViewType()) {
            case 0:
                return 0;
            case 1:
                return 1;
            default:
                return -1;
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //listHolder myHolder = (listHolder) holder;
        final HomeProductListModel current = productData.get(position);

        if (current != null) {
            switch (holder.getItemViewType()) {
                case 0:
                    advertismentViewType addHolder=(advertismentViewType)holder;
                    Picasso.with(context)
                            .load(current.getAddUrl())
                            .into((addHolder.addImage));
                            break;

                case 1:
                    productViewType myHolder=(productViewType)holder;

                    Picasso.with(context)
                            .load(current.getImageUrl1())
                            .into((myHolder.image1));
                    Picasso.with(context)
                            .load(current.getImageUrl2())
                            .into((myHolder.image2));
                    Picasso.with(context)
                            .load(current.getImageUrl3())
                            .into((myHolder.image3));
                    Picasso.with(context)
                            .load(current.getImageUrl4())
                            .into((myHolder.image4));
                    break;

            }
        }



    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    public static class advertismentViewType extends RecyclerView.ViewHolder {

        ImageView addImage;

        public advertismentViewType(View itemView) {
            super(itemView);

            this.addImage = itemView.findViewById(R.id.addImage);
        }
    }


    public static class productViewType extends RecyclerView.ViewHolder {

        ImageView image1,image2,image3,image4;

        public productViewType(View itemView) {
            super(itemView);

            this.image1 = itemView.findViewById(R.id.image1);
            this.image2 = itemView.findViewById(R.id.image2);
            this.image3 = itemView.findViewById(R.id.image3);
            this.image4 = itemView.findViewById(R.id.image4);
        }
    }
}
