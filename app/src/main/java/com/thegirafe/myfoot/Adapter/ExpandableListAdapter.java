package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thegirafe.myfoot.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ${Gagandeep} on 6/30/2018.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    ImageView imageView, drop;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.nav_items, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.nav_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        imageView = (ImageView) convertView.findViewById(R.id.ic_txt);
        drop = (ImageView) convertView.findViewById(R.id.plus_txt);

        setIcon(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setIcon(String title) {
        switch (title) {
            case "Home":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.home));
                drop.setVisibility(View.GONE);
                break;

            case "Category":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.category));
                drop.setVisibility(View.VISIBLE);
                break;

            case "Brand":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.brand));
                drop.setVisibility(View.VISIBLE);
                break;


            case "Offers":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.offer));
                drop.setVisibility(View.GONE);
                break;

            case "Notifications":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.profile_notification));
                drop.setVisibility(View.GONE);
                break;

            case "My Account":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.privacy));
                drop.setVisibility(View.GONE);
                break;

            case "My Cart":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.cart));
                drop.setVisibility(View.GONE);
                break;

            case "My Wishlist":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
                drop.setVisibility(View.GONE);
                break;

            case "My Orders":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.order_id_shopping_bag));
                drop.setVisibility(View.GONE);
                break;

            case "Login":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.logout));
                drop.setVisibility(View.GONE);
                break;

            case "Logout":
                imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.logout));
                drop.setVisibility(View.GONE);
                break;

        }
    }
}
