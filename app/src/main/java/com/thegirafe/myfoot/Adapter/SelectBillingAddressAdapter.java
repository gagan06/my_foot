package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.thegirafe.myfoot.Address.ShowShippingAddressData;
import com.thegirafe.myfoot.Interfaces.AddressInterface;
import com.thegirafe.myfoot.Interfaces.BillingInterface;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class SelectBillingAddressAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ShowShippingAddressData> data=new ArrayList<>();
    private LayoutInflater li;
    BillingInterface addressInterface;
    private int lastCheckedPosition = -1;

    public SelectBillingAddressAdapter(ArrayList<ShowShippingAddressData> data, Context context) {
        this.context=context;
        this.data=data;
        li=LayoutInflater.from(context);
        this.addressInterface = ((BillingInterface) context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=li.inflate(R.layout.card_for_saved_address,parent,false);
        listHolder holder=new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final listHolder myHolder=(listHolder)holder;
        final ShowShippingAddressData current=data.get(position);
        if (!current.getPhone().equalsIgnoreCase("")){
            myHolder.name.setText(String.format("%s( %s )", current.getName(), current.getPhone()));
        }else{
            myHolder.name.setText(current.getName());
        }
        if (current.getLandmark() != null){
            myHolder.addressTV.setText(String.format("%s( %s ), %s, %s, %s", current.getAddress(), current.getLandmark(), current.getCityName(), current.getStateName(), current.getZip()));
        }else{
            myHolder.addressTV.setText(String.format("%s, %s, %s, %s", current.getAddress(), current.getCityName(), current.getStateName(), current.getZip()));
        }

        myHolder.removeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressInterface.deleteBillingAddress(current.getAddress_id());
            }
        });
        myHolder.addressRadioBtn.setOnCheckedChangeListener(null);
        myHolder.addressRadioBtn.setChecked(position == lastCheckedPosition);
        myHolder.addressRadioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressInterface.selectBillingAddress(current.getAddress_id(), current.getZip(), current.getPhone());
                if (position == lastCheckedPosition) {
                    myHolder.addressRadioBtn.setChecked(false);
                    lastCheckedPosition = -1;
                } else {
                    lastCheckedPosition = position;
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class listHolder extends RecyclerView.ViewHolder {
        TextView name, addressTV;
        LinearLayout removeLayout;
        RadioButton addressRadioBtn;

        public listHolder(View v) {
            super(v);
            name=v.findViewById(R.id.name);
            addressTV=v.findViewById(R.id.addressTV);
            removeLayout=v.findViewById(R.id.removeLayout);
            addressRadioBtn=v.findViewById(R.id.addressRadioBtn);

        }
    }
}
