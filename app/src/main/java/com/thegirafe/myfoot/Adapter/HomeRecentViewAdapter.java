package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thegirafe.myfoot.Models.HomeRecentViewModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/22/2018.
 */
public class HomeRecentViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<HomeRecentViewModel> recentData = new ArrayList<>();
    private Context context;
    private LayoutInflater li;

    public HomeRecentViewAdapter(ArrayList<HomeRecentViewModel> recentData, Context context) {
        this.context=context;
        this.recentData=recentData;
        li=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=li.inflate(R.layout.card_for_home_recent_view,parent,false);
        listHolder holder=new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HomeRecentViewModel currunt=recentData.get(position);
        listHolder myHolder=(listHolder)holder;
        if (position==0){
            myHolder.firstText.setVisibility(View.VISIBLE);
            myHolder.imageView.setVisibility(View.GONE);
        }
        else {
            myHolder.firstText.setVisibility(View.GONE);
            myHolder.imageView.setVisibility(View.VISIBLE);

            Picasso.with(context)
                    .load(currunt.getImageUrl())
                    .into(myHolder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return recentData.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
           TextView firstText;

        public listHolder(View v) {
            super(v);
            imageView=v.findViewById(R.id.imageView);
            firstText=v.findViewById(R.id.firstText);

        }
    }
}
