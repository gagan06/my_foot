package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.thegirafe.myfoot.Cart.ProductList;
import com.thegirafe.myfoot.Interfaces.CouponInterface;
import com.thegirafe.myfoot.Interfaces.ItemClickListener;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.coupons.CouponsData;

import java.util.ArrayList;

public class CouponAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<CouponsData> cartData = new ArrayList<>();
    Context context;
    LayoutInflater li;
    int selectedPosition=-1;
    String amount;
    CouponInterface couponInterface;
    int valid = 1;

    public CouponAdapter(ArrayList<CouponsData> cartData, Context context, String amount) {
        this.cartData = cartData;
        this.context = context;
        li = LayoutInflater.from(context);
        this.amount = amount;
        this.couponInterface = ((CouponInterface) context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.coupon_view, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final CouponsData current = cartData.get(position);
        final listHolder myHolder = (listHolder) holder;

        Log.e(MyConstants.TAG, "Coupon "+current.getCode());
//
        myHolder.coupon.setText(current.getCode());
        myHolder.amount.setText("Rs. "+current.getDiscount());
        myHolder.description.setText("Rs. "+current.getDiscount()+" off on minimum purchase of  Rs. "+current.getCartAmount()+"\nValid till "+current.getValidTo());


        myHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount_new = amount.replace(",", "");
                int amt = Math.round(Float.parseFloat(amount_new));
                final String s = current.getCartAmount().replace(",", "");
                int cartamount = Integer.parseInt(s);
                Log.e(MyConstants.TAG, "COUPON "+(cartamount <= amt));
                if (cartamount <= amt){
                    couponInterface.couponselectd(current.getId().toString(), current.getCode(), current.getDiscount(), current.getCartAmount());
                    valid = 1;
                    selectedPosition = position;
                    notifyDataSetChanged();
                }else{
                    valid = 0;
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            }
        });

        if (selectedPosition == position){
            if (valid == 0){
                myHolder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.curved_invalid_coupon));
                myHolder.coupon.setBackground(context.getResources().getDrawable(R.drawable.white_rounder_corner));
                myHolder.amount.setTextColor(context.getResources().getColor(R.color.White));
                myHolder.coupon.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }else if(valid == 1){
                myHolder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.curved_coupon_drawable_selected));
                myHolder.coupon.setBackground(context.getResources().getDrawable(R.drawable.white_rounder_corner));
                myHolder.amount.setTextColor(context.getResources().getColor(R.color.Black));
                myHolder.coupon.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        }
        else {
            myHolder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.curved_coupon_drawable));
            myHolder.coupon.setBackground(context.getResources().getDrawable(R.drawable.coupon_drawable));
            myHolder.amount.setTextColor(context.getResources().getColor(R.color.colorSec));
            myHolder.coupon.setTextColor(context.getResources().getColor(R.color.greymedium));
        }
    }

    @Override
    public int getItemCount() {
        return cartData.size();
    }

    class listHolder extends RecyclerView.ViewHolder{
        LinearLayout mainLayout;
        TextView coupon, amount, description;

        public listHolder(View v) {
            super(v);
            mainLayout = v.findViewById(R.id.mainLayout);
            coupon = v.findViewById(R.id.coupon);
            amount = v.findViewById(R.id.amount);
            description = v.findViewById(R.id.description);

        }

    }
}
