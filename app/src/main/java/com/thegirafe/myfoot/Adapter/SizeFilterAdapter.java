package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.thegirafe.myfoot.Models.SizeFilterModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class SizeFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<SizeFilterModel> data = new ArrayList<>();
    LayoutInflater li;

    public SizeFilterAdapter(ArrayList<SizeFilterModel> data, Context context) {
        this.data = data;
        this.context = context;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_size_filter, parent, false);
        ListHolder holder = new ListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final SizeFilterModel current = data.get(position);
        final ListHolder myHolder = (ListHolder) holder;

        myHolder.sizeTV.setText(current.getSizeNmae());

        myHolder.sizeCheckBox.setChecked(data.get(position).getStatus());

        myHolder.sizeCheckBox.setTag(position);

        myHolder.sizeCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myHolder.sizeCheckBox.isChecked()) {
                    myHolder.sizeCheckBox.setChecked(true);
                    data.get(position).setStatus(true);
                } else {
                    myHolder.sizeCheckBox.setChecked(false);
                    data.get(position).setStatus(false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListHolder extends RecyclerView.ViewHolder {

        TextView sizeTV;
        CheckBox sizeCheckBox;

        public ListHolder(View v) {
            super(v);

            sizeTV = v.findViewById(R.id.sizeTV);
            sizeCheckBox = v.findViewById(R.id.sizeCheckBox);
        }
    }
}
