package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thegirafe.myfoot.Models.NotificationModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/23/2018.
 */
public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NotificationModel> data = new ArrayList<>();
    private Context context;
    private LayoutInflater li;

    public NotificationAdapter(ArrayList<NotificationModel> data, Context context) {
        this.context = context;
        this.data = data;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=li.inflate(R.layout.card_for_notifications,parent,false);
        listHolder holder=new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NotificationModel current=data.get(position);
        listHolder myHolder=(listHolder)holder;

        myHolder.titleTV.setText(current.getTitle());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class listHolder extends RecyclerView.ViewHolder {
        TextView titleTV,decTV,timeTV;
        ImageView bellIcon,notificationImage;

        public listHolder(View v) {
            super(v);
            titleTV=v.findViewById(R.id.titleTV);
        }
    }
}
