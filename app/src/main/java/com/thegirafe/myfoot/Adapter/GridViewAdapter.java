package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.thegirafe.myfoot.FeaturedProducts.FeaturedProductData;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 4/3/2018.
 */
public class GridViewAdapter extends ArrayAdapter<FeaturedProductData> {
    Context context;
    int layoutResourceId;
    RequestOptions requestOptions;
    ArrayList<FeaturedProductData> data = new ArrayList<FeaturedProductData>();

    public GridViewAdapter(Context context, int layoutResourceId,
                           ArrayList<FeaturedProductData> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordHolder holder = null;

        if (row == null) {
            LayoutInflater inflater =  LayoutInflater.from(getContext());
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RecordHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
            holder.imageItem = (ImageView) row.findViewById(R.id.item_image);

            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        Log.e(MyConstants.TAG, "Product sizw "+data.size());

        FeaturedProductData item = data.get(position);
        holder.txtTitle.setText(item.getProductName());

        requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + item.getProductGallery().get(0).getProductImage()))
                .apply(requestOptions
                        .skipMemoryCache(true)
                        .placeholder(R.color.Black))
                .into(holder.imageItem);
//        holder.imageItem.setImageDrawable(item.getProductGallery().get(0));
        return row;

    }

    static class RecordHolder {
        TextView txtTitle;
        ImageView imageItem;
    }
}