package com.thegirafe.myfoot.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thegirafe.myfoot.Activities.ProductDetail;
import com.thegirafe.myfoot.Models.MyOffersModel;
import com.thegirafe.myfoot.Offers.OfferProductData;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class MyOffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<OfferProductData> data = new ArrayList<>();
    LayoutInflater li;
    String discount;

    public MyOffersAdapter(ArrayList<OfferProductData> data, Context context, String discount) {
        this.context = context;
        this.data = data;
        this.discount =discount;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_my_offers, parent, false);
        ListHolder holder = new ListHolder(v);
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final OfferProductData current=data.get(position);
        ListHolder myHolder=(ListHolder)holder;

        myHolder.productActualPrise.setText(current.getProductOriginalPrice());
        myHolder.productName.setText(current.getProductName());

        if (current.getProductSalePrice() == null){
            int price = Integer.parseInt(current.getProductOriginalPrice()) - Integer.parseInt(discount);
            myHolder.productDiscountPrice.setText(price+"");
        }else{
            int price = Integer.parseInt(current.getProductSalePrice()) - Integer.parseInt(discount);
            myHolder.productDiscountPrice.setText(price+"");
        }
        Picasso.with(context)
                .load(MyConstants.IMAGE_PATH+current.getProductImage())
                .skipMemoryCache()
                .into(myHolder.product_image);

        if (current.getWishlist().equalsIgnoreCase("0")){
            myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
        }else{
            myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_filled));
        }

        myHolder.priseOff.setVisibility(View.GONE);

        final Activity activity = (Activity) context;


        myHolder.product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetail.class);
                intent.putExtra(MyConstants.Product_id, current.getProductId());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListHolder extends RecyclerView.ViewHolder {
        TextView productName, productDiscountPrice, productActualPrise, priseOff;
        ImageView product_image;
        ImageButton addToFav;

        public ListHolder(View v) {
            super(v);

            productName = v.findViewById(R.id.productName);
            productDiscountPrice = v.findViewById(R.id.productDiscountPrice);
            productActualPrise = v.findViewById(R.id.productActualPrise);
            priseOff = v.findViewById(R.id.priseOff);
            product_image = v.findViewById(R.id.product_image);
            addToFav = v.findViewById(R.id.addToFav);

        }
    }
}
