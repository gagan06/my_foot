package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thegirafe.myfoot.Filters.FilterColorData;
import com.thegirafe.myfoot.Interfaces.BrandFilterInterface;
import com.thegirafe.myfoot.Models.ColorFilterModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ${Gagandeep} on 6/29/2018.
 */
public class ColorFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<FilterColorData> data = new ArrayList<>();
    LayoutInflater li;
    ArrayList<String> main = new ArrayList<>();
    BrandFilterInterface brandFilterInterface;

    public ColorFilterAdapter(ArrayList<FilterColorData> data, Context context) {
        this.data = data;
        this.context = context;
        li = LayoutInflater.from(context);
        this.brandFilterInterface = ((BrandFilterInterface) context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_color_filter, parent, false);
        ListHolder holder = new ListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final FilterColorData current = data.get(position);
        final ListHolder myHolder = (ListHolder) holder;

        myHolder.colorName.setText(current.getColorName());

        myHolder.colorCheckBox.setChecked(data.get(position).getStatus());

        myHolder.colorCheckBox.setTag(position);

        myHolder.colorCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myHolder.colorCheckBox.isChecked()) {
                    myHolder.colorCheckBox.setChecked(true);
                    data.get(position).setStatus(true);
                    main.add(data.get(position).getColorId());
                    brandFilterInterface.getColorsList(main);
                    notifyDataSetChanged();
                } else {
                    myHolder.colorCheckBox.setChecked(false);
                    data.get(position).setStatus(false);
                    main.remove(data.get(position).getColorId());
                    brandFilterInterface.getColorsList(main);
                    notifyDataSetChanged();
                }
            }
        });

        Gson gson = new Gson();
        String hashMapString = gson.toJson(main);
        Log.e(MyConstants.TAG, "Dara "+hashMapString);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListHolder extends RecyclerView.ViewHolder {

        TextView colorName;
        CheckBox colorCheckBox;

        public ListHolder(View v) {
            super(v);

            colorName = v.findViewById(R.id.colorName);
            colorCheckBox = v.findViewById(R.id.colorCheckBox);
        }
    }
}
