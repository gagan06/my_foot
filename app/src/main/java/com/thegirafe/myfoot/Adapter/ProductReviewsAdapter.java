package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thegirafe.myfoot.Models.ProductReviewsModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Reviews.ReviewData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ${Gagandeep} on 7/2/2018.
 */
public class ProductReviewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<ReviewData> data = new ArrayList<>();
    Context context;
    LayoutInflater li;

    public ProductReviewsAdapter(ArrayList<ReviewData> reviewData, Context context) {
        this.data = reviewData;
        this.context = context;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_reviews, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ReviewData current = data.get(position);
        listHolder myHolder = (listHolder) holder;
//
        myHolder.userName.setText(current.getUserName());
        if (current.getProfile_image() != null){
            if (current.getProvider().equals("normal")){
                Glide.with(context)
                        .load(Uri.parse(MyConstants.Profile_Image + current.getProfile_image()))
                        .into(myHolder.userImage);
            }else{
                Glide.with(context)
                        .load(Uri.parse(current.getProfile_image()))
                        .into(myHolder.userImage);
            }
        }
        myHolder.dateTV.setText(getFormatedDate(current.getDate().substring(0, 10)));
        myHolder.smallRatingBar.setRating(Float.parseFloat(current.getRating()));
        myHolder.smallRatingBar.setEnabled(false);
        myHolder.reviewTV.setText(current.getReviews());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class listHolder extends RecyclerView.ViewHolder {
        ImageView userImage;
        TextView userName, dateTV,reviewTV;
        RatingBar smallRatingBar;

        public listHolder(View v) {
            super(v);
            userImage = v.findViewById(R.id.userImage);
            userName = v.findViewById(R.id.userName);
            reviewTV = v.findViewById(R.id.reviewTV);
            dateTV = v.findViewById(R.id.dateTV);
            smallRatingBar = v.findViewById(R.id.smallRatingBar);

        }
    }

    public String getFormatedDate(String dateStr){
        try {
            // SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
            // Date dateValue = input.parse(dateStr);

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
            Date date = format1.parse(dateStr);
            dateStr=format2.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(dateStr);
    }

}
