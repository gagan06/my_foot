package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thegirafe.myfoot.Order.OrderStatusData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/30/2018.
 */
public class OrderStatusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<OrderStatusData> data = new ArrayList<>();
    LayoutInflater li;

    public OrderStatusAdapter(ArrayList<OrderStatusData> data, Context context) {
        this.context = context;
        this.data = data;
        li = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_order_status, parent, false);
        ListHolder holder = new ListHolder(v);
        return holder;
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OrderStatusData current=data.get(position);
        ListHolder myHolder=(ListHolder)holder;
            myHolder.statusTitle.setText(current.getStatus());
            myHolder.statusDate.setText(current.getOrder_date().toString());
            myHolder.statusIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.order_status_tick));

//        if (current.isCheckStatus()){
//            myHolder.statusIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.order_status_tick));
//        }
//        else {
//            myHolder.statusTitle.setText(current.getStatusTitle());
//            myHolder.statusDate.setText("Sheduled");
//            myHolder.statusIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.order_status_circle));
//        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ListHolder extends RecyclerView.ViewHolder {
        TextView statusTitle, statusDate;
        ImageView statusIcon;

        public ListHolder(View v) {
            super(v);

            statusTitle = v.findViewById(R.id.statusTitle);
            statusDate = v.findViewById(R.id.statusDate);
            statusIcon = v.findViewById(R.id.statusIcon);
        }
    }
}
