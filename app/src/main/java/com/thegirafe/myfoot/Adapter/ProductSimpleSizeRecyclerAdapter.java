package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thegirafe.myfoot.Attributes.SimpleData;
import com.thegirafe.myfoot.Interfaces.SizeColorInterface;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.Size;
import com.thegirafe.myfoot.R;

public class ProductSimpleSizeRecyclerAdapter  extends RecyclerView.Adapter<ProductSizeRecyclerAdapter.ViewHolder> {

    SimpleData productSizes;
    Context context;
    SizeColorInterface sizeColorInterface;
    int selectedPosition=-1;

    public ProductSimpleSizeRecyclerAdapter(Context context, SimpleData productSizes1) {
        super();
        this.context = context;
        this.productSizes = productSizes1;
        this.sizeColorInterface = ((SizeColorInterface) context);
    }

    @Override
    public ProductSizeRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_for_size, viewGroup, false);
        ProductSizeRecyclerAdapter.ViewHolder viewHolder = new ProductSizeRecyclerAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProductSizeRecyclerAdapter.ViewHolder viewHolder, final int position) {
        Log.e(MyConstants.TAG, "product size " + position);
        final SimpleData product = productSizes;
        viewHolder.size_text.setText(product.getName());
        viewHolder.size_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                sizeColorInterface.select_size(product.getAttributeValueId());
                notifyDataSetChanged();
            }
        }); // bind the listener

        if (selectedPosition == position){
            viewHolder.circle.setBackground(context.getResources().getDrawable(R.drawable.circle_selected));
            viewHolder.size_text.setTextColor(context.getResources().getColor(R.color.colorSec));
        }
        else {
            viewHolder.circle.setBackground(context.getResources().getDrawable(R.drawable.circle_black));
            viewHolder.size_text.setTextColor(context.getResources().getColor(R.color.Black));
        }

    }

    @Override
    public int getItemCount() {
        return selectedPosition;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView size_text;
        RelativeLayout circle;

        public ViewHolder(View itemView) {
            super(itemView);
            size_text =  itemView.findViewById(R.id.size);
            circle =  itemView.findViewById(R.id.circle);
        }


    }

}
