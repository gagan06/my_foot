package com.thegirafe.myfoot.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.thegirafe.myfoot.Activities.MyOffers;
import com.thegirafe.myfoot.Models.HomeSliderModel;
import com.thegirafe.myfoot.Offers.OffersSliderData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/21/2018.
 */

public class ImagePagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<OffersSliderData> data = new ArrayList<>();
    String preLink="http://myfoot.thegirafe.com/uploads/offer/";

    public ImagePagerAdapter(Context contex, ArrayList<OffersSliderData> data) {
        this.context = contex;
        layoutInflater = (LayoutInflater) contex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data=data;
    }

    @Override
    public int getCount() {
        if(data != null){
            return data.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.home_pager_item, container, false);

        final OffersSliderData currunt=data.get(position);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Picasso.with(context)
                .load(preLink+currunt.getImage())
                .into(imageView);

        container.addView(itemView);
        final Activity activity = (Activity) context;

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOffers.class);
                intent.putExtra("Offer_id", currunt.getId().toString());
                intent.putExtra("discount", currunt.getDiscount());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}