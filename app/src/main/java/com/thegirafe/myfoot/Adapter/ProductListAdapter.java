package com.thegirafe.myfoot.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;
import com.thegirafe.myfoot.Activities.CartProduct;
import com.thegirafe.myfoot.Activities.ProductDetail;
import com.thegirafe.myfoot.Models.ProductListModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.Products.CategoryProductData;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

/**
 * Created by ${Gagandeep} on 6/21/2018.
 */
public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<CategoryProductData> productData = new ArrayList<>();
    Context context;
    LayoutInflater li;
    RequestOptions requestOptions;
    ArrayList<CategoryProductData> contactListFiltered = new ArrayList<>();

    public ProductListAdapter(ArrayList<CategoryProductData> productData, Context context) {
        this.context=context;
        this.productData=productData;
        this.contactListFiltered = productData;
        li=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_product_item, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final listHolder myHolder = (listHolder) holder;
        final CategoryProductData current = productData.get(position);
        Log.e(MyConstants.TAG, "currre");
        if (current.getSubProducts().size() == 0){
            myHolder.productActualPrise.setText(current.getProductOriginalPrice());
            myHolder.productDiscountPrice.setText(current.getProductSalePrice());
            myHolder.productName.setText(current.getProductName());
            requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

            if (current.getProductSalePrice() == null){
                myHolder.productDiscountPrice.setText(current.getProductOriginalPrice());
                myHolder.price.setVisibility(View.GONE);
            }

            Glide.with(context)
                    .load(Uri.parse(MyConstants.IMAGE_PATH + current.getFeaturedImage()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(((listHolder) holder).product_image);
        }else{
            myHolder.productActualPrise.setText(current.getSubProducts().get(0).getProductOriginalPrice());
            myHolder.productDiscountPrice.setText(current.getSubProducts().get(0).getProductSalePrice());
            myHolder.productName.setText(current.getProductName());
            requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

            String like_user = current.getLikes();
            if (like_user != null){
                myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
            }else{
                myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
            }
            Glide.with(context)
                    .load(Uri.parse(MyConstants.IMAGE_PATH + current.getSubProducts().get(0).getProductImage()))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            myHolder.progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(((listHolder) holder).product_image);

            if (current.getSubProducts().get(0).getProductSalePrice() == null){
                myHolder.productDiscountPrice.setText(current.getSubProducts().get(0).getProductOriginalPrice());
                myHolder.price.setVisibility(View.GONE);
            }
        }
        int like_user = current.getUser_liked();
        if (like_user != 0){
            myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
        }else{
            myHolder.addToFav.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
        }
        final Activity activity = (Activity) context;

        myHolder.product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetail.class);
                intent.putExtra(MyConstants.Product_id, current.getProductId());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.e(MyConstants.TAG, "TRUE"+charString);

                if (charString.isEmpty()) {
                    Log.e(MyConstants.TAG, "Product Data "+productData.size());
                    productData = contactListFiltered;
                } else {
                    ArrayList<CategoryProductData> filteredList = new ArrayList<>();
                    for (CategoryProductData row : productData) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            Log.e(MyConstants.TAG, "TRUE    "+row.getProductName());
                            filteredList.add(row);
                        }
                    }

                    productData = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = productData;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productData = (ArrayList<CategoryProductData>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    class listHolder extends RecyclerView.ViewHolder {
        ImageView product_image;
        TextView productName, productDiscountPrice,productActualPrise;
        RelativeLayout product;
        ImageButton addToFav;
        ProgressBar progress;
        LinearLayout price;

        public listHolder(View v) {
            super(v);
            product_image = v.findViewById(R.id.product_image);
            productName = v.findViewById(R.id.productName);
            productDiscountPrice = v.findViewById(R.id.productDiscountPrice);
            productActualPrise = v.findViewById(R.id.productActualPrise);
            product = v.findViewById(R.id.product);
            addToFav = v.findViewById(R.id.addToFav);
            price = v.findViewById(R.id.price);
            progress = v.findViewById(R.id.progress);
        }
    }
}
