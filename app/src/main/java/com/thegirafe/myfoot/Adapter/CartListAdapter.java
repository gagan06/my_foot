package com.thegirafe.myfoot.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.thegirafe.myfoot.Activities.Wishlist;
import com.thegirafe.myfoot.Cart.MyCartData;
import com.thegirafe.myfoot.Cart.ProductList;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Interfaces.ItemClickListener;
import com.thegirafe.myfoot.Interfaces.MoveToWishlist;
import com.thegirafe.myfoot.Models.CartListModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.PostModel.ProductDetailModel;
import com.thegirafe.myfoot.R;
import com.thegirafe.myfoot.Wishlist.CheckWishlistResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ${Gagandeep} on 6/22/2018.
 */
public class CartListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<ProductList> cartData = new ArrayList<>();
    Context context;
    private ItemClickListener clickListener;
    LayoutInflater li;
    MoveToWishlist moveToWishlistListener;
    RequestOptions requestOptions;
    SharedPreferences sharedPreferences;
    TextView move_wishlist;

    public CartListAdapter(ArrayList<ProductList> cartData, Context context) {
        this.cartData = cartData;
        this.context = context;
        li = LayoutInflater.from(context);
        this.moveToWishlistListener = ((MoveToWishlist) context);
        sharedPreferences = context.getSharedPreferences(MyConstants.MyPref, Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = li.inflate(R.layout.card_for_cart_item, parent, false);
        listHolder holder = new listHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ProductList current = cartData.get(position);
        listHolder myHolder = (listHolder) holder;
//
        if (current.getProductPrice() == null){
            myHolder.price.setVisibility(View.GONE);
            myHolder.discountPriseTV.setText(current.getListPrice().toString());
        }else{
            myHolder.discountPriseTV.setText(current.getProductPrice().toString());
            myHolder.productActualPriseTV.setText(current.getListPrice().toString());
        }
        myHolder.product_name.setText(current.getProductName());
        myHolder.sold_by.setText("Seller: "+current.getProductSoldBy());

        if (current.getTotalQuantity() == 0){
            myHolder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.card_wish));
            moveToWishlistListener.sendError(current.getProductId().toString());
            myHolder.stock.setVisibility(View.VISIBLE);
            myHolder.number_button.setVisibility(View.GONE);
        }

        checkWishlist(current.getProductId().toString());

        Glide.with(context)
                .load(Uri.parse(MyConstants.IMAGE_PATH + current.getFeaturedImage()))
                .into(myHolder.product_image);
        myHolder.delivery_detail.setText("Size: "+current.getSize().get(0));
        myHolder.number_button.setNumber(String.valueOf(current.getQuantity()));
        myHolder.number_button.setRange(1, current.getTotalQuantity());
        myHolder.moveToWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (move_wishlist.getText().toString().equalsIgnoreCase("Move to Wishlist")){
                    moveToWishlistListener.movetowish(current.getProductId().toString());
                }else if(move_wishlist.getText().toString().equalsIgnoreCase("Go to Wishlist")){
                    moveToWishlistListener.gotowishlist();
                }

            }
        }); // bind the listener
        myHolder.removeFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToWishlistListener.removerfromcart(current.getProductId().toString());

            }
        });

        myHolder.number_button.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                moveToWishlistListener.addToCart(current.getProductId().toString(), String.valueOf(newValue));
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartData.size();
    }

    class listHolder extends RecyclerView.ViewHolder{
        ImageView product_image;
        TextView productActualPriseTV, discountPriseTV, product_name, sold_by, delivery_detail, stock;
        LinearLayout removeFromCart, moveToWishlist, price;
        ElegantNumberButton number_button;
        CardView card_view;

        public listHolder(View v) {
            super(v);
            productActualPriseTV = v.findViewById(R.id.productActualPriseTV);
            discountPriseTV = v.findViewById(R.id.discountPriseTV);
            removeFromCart = v.findViewById(R.id.removeFromCart);
            moveToWishlist = v.findViewById(R.id.moveToWishlist);
            product_name = v.findViewById(R.id.product_name);
            sold_by = v.findViewById(R.id.sold_by);
            delivery_detail = v.findViewById(R.id.delivery_detail);
            product_image = v.findViewById(R.id.product_image);
            number_button = v.findViewById(R.id.number_button);
            card_view = v.findViewById(R.id.card_view);
            stock = v.findViewById(R.id.stock);
            price = v.findViewById(R.id.price);
            move_wishlist = v.findViewById(R.id.move_wishlist);
        }

    }

    public void checkWishlist(String productid){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        ProductDetailModel productDetailModel =  new ProductDetailModel(productid);
        Call<CheckWishlistResponse> call = api.check_wish(getAuthToken(), productDetailModel);
        call.enqueue(new Callback<CheckWishlistResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResponse(Call<CheckWishlistResponse> call, Response<CheckWishlistResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()){
                        move_wishlist.setText("Go to Wishlist");
                    }else{
                        move_wishlist.setText("Move to Wishlist");
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<CheckWishlistResponse> call, Throwable t) {

            }
        });
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


}
