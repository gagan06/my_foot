package com.thegirafe.myfoot.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.thegirafe.myfoot.R;

import java.util.ArrayList;
import java.util.List;

public class PriceFilter extends Fragment implements AdapterView.OnItemSelectedListener {

    ArrayAdapter<String> spinnerAdapterMinPrice;
    ArrayAdapter<String> spinnerAdapterMaxPrice;
    Spinner minPriceSpinner, maxPriceSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_price_filter, container, false);

        minPriceSpinner = v.findViewById(R.id.minPriceSpinner);
        maxPriceSpinner = v.findViewById(R.id.maxPriceSpinner);

        minPrice();
        maxPrice();

        minPriceSpinner.setOnItemSelectedListener(this);
        maxPriceSpinner.setOnItemSelectedListener(this);

        return v;
    }

    public void minPrice() {
        // Spinner Drop down elements
        List<String> Type = new ArrayList<String>();
        Type.add("Experience");
        Type.add("0");
        Type.add("100");
        Type.add("200");
        Type.add("300");
        Type.add("400");
        Type.add("500");
        Type.add("600");
        Type.add("700");
        Type.add("800");
        Type.add("900");
        Type.add("1000");

        // Creating adapter for spinner
        spinnerAdapterMinPrice = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, Type) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
        };

        // Drop down layout style - list view with radio button
        spinnerAdapterMinPrice.setDropDownViewResource(R.layout.spinner_item_black_text);


        // attaching data adapter to spinner
        minPriceSpinner.setAdapter(spinnerAdapterMinPrice);
    }

    public void maxPrice() {
        // Spinner Drop down elements
        List<String> Type = new ArrayList<String>();
        Type.add("Experience");
        Type.add("100");
        Type.add("200");
        Type.add("300");
        Type.add("400");
        Type.add("500");
        Type.add("600");
        Type.add("700");
        Type.add("800");
        Type.add("900");
        Type.add("1000");
        Type.add("2000");

        // Creating adapter for spinner
        spinnerAdapterMaxPrice = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, Type) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }
        };

        // Drop down layout style - list view with radio button
        spinnerAdapterMaxPrice.setDropDownViewResource(R.layout.spinner_item_black_text);


        // attaching data adapter to spinner
        maxPriceSpinner.setAdapter(spinnerAdapterMaxPrice);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        Spinner spinner = (Spinner) parent;
        switch (spinner.getId()) {
            case R.id.minPriceSpinner:
                String minPriceStr = parent.getItemAtPosition(position).toString();
                //stateTV.setText(state);
                break;

            case R.id.maxPriceSpinner:
                String maxPriceStr = parent.getItemAtPosition(position).toString();
                //stateTV.setText(state);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
