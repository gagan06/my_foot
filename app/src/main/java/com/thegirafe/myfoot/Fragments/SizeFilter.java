package com.thegirafe.myfoot.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thegirafe.myfoot.Adapter.SizeFilterAdapter;
import com.thegirafe.myfoot.Models.SizeFilterModel;
import com.thegirafe.myfoot.R;

import java.util.ArrayList;

public class SizeFilter extends Fragment {
RecyclerView sizeRecycler;
    ArrayList<SizeFilterModel> data=new ArrayList<>();
    SizeFilterAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_size_filter, container, false);

        sizeRecycler= v.findViewById(R.id.sizeRecycler);

        getDemoColor();

        return v;
    }

    public void getDemoColor(){
        for (int i=1;i<12;i++){
            SizeFilterModel model=new SizeFilterModel();
            model.setSizeNmae(String.valueOf(i));
            model.setStatus(true);
            data.add(model);
            adapter=new SizeFilterAdapter(data,getContext());
            sizeRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
            sizeRecycler.setAdapter(adapter);
        }
    }

}
