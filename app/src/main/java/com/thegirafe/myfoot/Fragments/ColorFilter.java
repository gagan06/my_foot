package com.thegirafe.myfoot.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.thegirafe.myfoot.Adapter.BrandFilterAdapter;
import com.thegirafe.myfoot.Adapter.ColorFilterAdapter;
import com.thegirafe.myfoot.Categories.BrandsResponse;
import com.thegirafe.myfoot.Filters.FilterColorData;
import com.thegirafe.myfoot.Filters.FilterColorResponse;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.ColorFilterModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ColorFilter extends Fragment {
    RecyclerView colorRecycler;
    ArrayList<FilterColorData> data=new ArrayList<>();
    ColorFilterAdapter adapter;
    SharedPreferences sharedPreferences;
    Button applyColor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_color_filter, container, false);

        sharedPreferences = getActivity().getSharedPreferences(MyConstants.MyPref, Context.MODE_PRIVATE);
        colorRecycler = v.findViewById(R.id.colorRecycler);
        applyColor = v.findViewById(R.id.applyColor);

        getColors();


        applyColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return v;
    }

    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getColors(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<FilterColorResponse> call = api.get_colors(getAuthToken());
        call.enqueue(new Callback<FilterColorResponse>() {
            @Override
            public void onResponse(Call<FilterColorResponse> call, Response<FilterColorResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new ColorFilterAdapter(data, getActivity());
                        colorRecycler.setAdapter(adapter);
                        colorRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<FilterColorResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }
}
