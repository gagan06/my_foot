package com.thegirafe.myfoot.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thegirafe.myfoot.Activities.Products_List;
import com.thegirafe.myfoot.Adapter.BrandFilterAdapter;
import com.thegirafe.myfoot.Categories.BrandsList;
import com.thegirafe.myfoot.Categories.BrandsResponse;
import com.thegirafe.myfoot.Interfaces.ApiInterface;
import com.thegirafe.myfoot.Models.BrandFilterModel;
import com.thegirafe.myfoot.Others.MyConstants;
import com.thegirafe.myfoot.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BrandFilter extends Fragment {
    RecyclerView brandRecycler;
    ArrayList<BrandsList> data = new ArrayList<>();
    BrandFilterAdapter adapter;
    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_brand_filter, container, false);

        sharedPreferences = getContext().getSharedPreferences(MyConstants.MyPref, Context.MODE_PRIVATE);
        brandRecycler = v.findViewById(R.id.brandRecycler);

        getBrands();

        return v;
    }


    public String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = (sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Email, "") + ":" + sharedPreferences.getString(com.thegirafe.myfoot.Others.MyConstants.Password, "")).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public void getBrands(){
        Retrofit retrofit =new Retrofit.Builder().baseUrl(com.thegirafe.myfoot.Others.MyConstants.MyFootUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface api= retrofit.create(ApiInterface.class);

        Call<BrandsResponse> call = api.get_brands(getAuthToken());
        call.enqueue(new Callback<BrandsResponse>() {
            @Override
            public void onResponse(Call<BrandsResponse> call, Response<BrandsResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getSuccess()) {
                        data = new ArrayList<>(response.body().getMessage().getData());
                        adapter = new BrandFilterAdapter(data, getActivity());
                        brandRecycler.setAdapter(adapter);
                        brandRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
                    }else
                    {
                        Log.e(MyConstants.TAG, "error"+response.errorBody());
                    }
                }else{
                    //server error
                }

            }

            @Override
            public void onFailure(Call<BrandsResponse> call, Throwable t) {
                Log.e(com.thegirafe.myfoot.Others.MyConstants.TAG, t.toString());
            }
        });
    }

}
