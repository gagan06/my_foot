package com.thegirafe.myfoot.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderStatusMessage {

    @SerializedName("data")
    @Expose
    private List<OrderStatusData> data = null;

    public List<OrderStatusData> getData() {
        return data;
    }

    public void setData(List<OrderStatusData> data) {
        this.data = data;
    }
}
