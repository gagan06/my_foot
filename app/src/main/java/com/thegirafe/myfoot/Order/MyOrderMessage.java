package com.thegirafe.myfoot.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrderMessage {

    @SerializedName("data")
    @Expose
    private List<MyOrderData> data = null;

    public List<MyOrderData> getData() {
        return data;
    }

    public void setData(List<MyOrderData> data) {
        this.data = data;
    }
}
