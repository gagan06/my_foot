package com.thegirafe.myfoot.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thegirafe.myfoot.coupons.CheckCouponMessage;

public class CancelOrderResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private CancelOrderMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CancelOrderMessage getMessage() {
        return message;
    }

    public void setMessage(CancelOrderMessage message) {
        this.message = message;
    }
}
