package com.thegirafe.myfoot.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatusData {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("remark")
    @Expose
    private Object remark;
    @SerializedName("order_date")
    @Expose
    private Object order_date;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getRemark() {
        return remark;
    }

    public void setRemark(Object remark) {
        this.remark = remark;
    }

    public Object getOrder_date() {
        return order_date;
    }

    public void setOrder_date(Object order_date) {
        this.order_date = order_date;
    }
}
