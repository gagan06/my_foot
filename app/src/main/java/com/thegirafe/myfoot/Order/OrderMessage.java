package com.thegirafe.myfoot.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderMessage {
    @SerializedName("errors")
    @Expose
    private OrderError errors;

    public OrderError getErrors() {
        return errors;
    }

    public void setErrors(OrderError errors) {
        this.errors = errors;
    }
    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
