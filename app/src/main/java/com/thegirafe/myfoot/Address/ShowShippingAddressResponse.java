package com.thegirafe.myfoot.Address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShowShippingAddressResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private ShowShippingAddressMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ShowShippingAddressMessage getMessage() {
        return message;
    }

    public void setMessage(ShowShippingAddressMessage message) {
        this.message = message;
    }
}
