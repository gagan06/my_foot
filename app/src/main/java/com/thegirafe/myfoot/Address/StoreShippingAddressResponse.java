package com.thegirafe.myfoot.Address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thegirafe.myfoot.Cart.MyCartMessage;

public class StoreShippingAddressResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private StoreShippingAddressMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public StoreShippingAddressMessage getMessage() {
        return message;
    }

    public void setMessage(StoreShippingAddressMessage message) {
        this.message = message;
    }
}
