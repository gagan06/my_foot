package com.thegirafe.myfoot.Address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShowShippingAddressMessage {
    @SerializedName("data")
    @Expose
    private List<ShowShippingAddressData> data = null;

    public List<ShowShippingAddressData> getData() {
        return data;
    }

    public void setData(List<ShowShippingAddressData> data) {
        this.data = data;
    }

}
