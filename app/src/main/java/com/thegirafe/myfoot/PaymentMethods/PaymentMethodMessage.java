package com.thegirafe.myfoot.PaymentMethods;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethodMessage {
    @SerializedName("data")
    @Expose
    private List<PaymentMethodData> data = null;

    public List<PaymentMethodData> getData() {
        return data;
    }

    public void setData(List<PaymentMethodData> data) {
        this.data = data;
    }

}
