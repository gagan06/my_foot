package com.thegirafe.myfoot.OtherClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private OtherMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OtherMessage getMessage() {
        return message;
    }

    public void setMessage(OtherMessage message) {
        this.message = message;
    }

}
