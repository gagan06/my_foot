package com.thegirafe.myfoot.OtherClasses;

/**
 * Created by ${Gagandeep} on 6/30/2018.
 */
public class MyConstants {
    public static final String GroupPosition = "0";
    public static final String Category_Id = "Category_Id";
    public static final String TAG = "MyFoot";
    public static final String MyFootUrl = "http://my-foot.thegirafe.com/api/v1/";
}
