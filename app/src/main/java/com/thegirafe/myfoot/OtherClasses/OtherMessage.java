package com.thegirafe.myfoot.OtherClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherMessage {
    @SerializedName("data")
    @Expose
    private OtherData data;

    public OtherData getData() {
        return data;
    }

    public void setData(OtherData data) {
        this.data = data;
    }

}
