package com.thegirafe.myfoot.Others;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.thegirafe.myfoot.service.MyFirebaseMessagingService;

/**
 * Created by HP on 3/30/2018.
 */

public class FirebaseDataReceiver extends BroadcastReceiver {

    public FirebaseDataReceiver() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        //Run your service here
        Intent startServiceIntent = new Intent(context, MyFirebaseMessagingService.class);
        context.startService(startServiceIntent);
    }
}