package com.thegirafe.myfoot.Others;

/**
 * Created by The Girafe on 1/31/2018.
 */
//http://myfoot.thegirafe.com/api/v1/
public class MyConstants {
    public static final String MyFootUrl = "http://myfoot.thegirafe.com/api/v1/";
    public static final String TAG = "MyFoot";
    public static final String MyPref = "SalonKeyPreferences";
    public static final String GuestPref = "Guest";
    public static final String USERNAME_PATTERN = "^[a-zA-Z0-9_ ]*$";
    public static final String City_Pattern = "[a-zA-Z]+(?:[ '-][a-zA-Z]+)*";
    public static final String Address_Pattern = "^[#.0-9a-zA-Z\\s,-]+$";
    public static final String Email_Pattern = "[_a-zA-Z_]+[a-zA-Z0-9._]*[a-zA-Z0-9]+@[a-z]+[.]+[a-z0-9A-Z]+[.]*+[a-zA-Z0-9]*";
    public static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
    public static final String Email = "";
    public static final String Provider = "normal";
    public static final String Password = "Pass";
    public static final String GroupPosition = "0";
    public static final String Id = "id";
    public static final String ProfileImage = "ProfileImage";
    public static final String Phone = "Phone";
    public static final String Gender = "Gender";
    public static final String Username = "Username";
    public static final String Salon_id = "salon_id";
    public static final String Salon_name = "Salon_name";
    public static final String Category_Id = "Category_Id";
    public static final String CityName = "CityName";
    public static final String Blog_Id = "Blog_Id";
    public static final String Address = "Address";
    public static final String BLOG_IMAGE_PATH = "http://saloon.thegirafe.com/blog_images/";
    public static final String IMAGE_PATH = "http://myfoot.thegirafe.com/product_images/";
    public static final String COVER_IMAGE_PATH = "http://saloon.thegirafe.com/cover_images/";
    public static final String COUPON_IMAGE_PATH ="http://saloon.thegirafe.com/coupon_images/";
    public static final String Profile_Image ="http://my-foot.thegirafe.com/profile_images/";
    public static final String Slider_Image ="http://saloon.thegirafe.com/slider_images/";
    public static final String PlaceApi = "AIzaSyDDIAUXqadWrakGt6rNEUwHPmBONPdNZ7c";
    public static final String SMS_ORIGIN = "BW-VSBJAJ";
    public static final String SMS_ORIGIN1 = "ID-VSBJAJ";
    public static final String OTP_DELIMITER = ":";
    public static final String PermissionStatus = "permissionStatus";
    public static final String Product_id = "Product_id";

    public static final String AccessToken="AccessToken";

    public static final String ProductImage = "ProductImage";
    public static final String ProductName = "ProductName";
    public static final String Coupon_ID = "Coupon_ID";
}
