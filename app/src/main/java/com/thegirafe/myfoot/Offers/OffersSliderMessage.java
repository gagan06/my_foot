package com.thegirafe.myfoot.Offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersSliderMessage {
    @SerializedName("data")
    @Expose
    private List<OffersSliderData> data = null;

    public List<OffersSliderData> getData() {
        return data;
    }

    public void setData(List<OffersSliderData> data) {
        this.data = data;
    }

}
