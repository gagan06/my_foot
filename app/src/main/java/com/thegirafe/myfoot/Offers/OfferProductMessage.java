package com.thegirafe.myfoot.Offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferProductMessage {
    @SerializedName("data")
    @Expose
    private List<OfferProductData> data = null;

    public List<OfferProductData> getData() {
        return data;
    }

    public void setData(List<OfferProductData> data) {
        this.data = data;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
