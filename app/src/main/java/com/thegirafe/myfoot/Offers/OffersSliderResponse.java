package com.thegirafe.myfoot.Offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OffersSliderResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private OffersSliderMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OffersSliderMessage getMessage() {
        return message;
    }

    public void setMessage(OffersSliderMessage message) {
        this.message = message;
    }

}
