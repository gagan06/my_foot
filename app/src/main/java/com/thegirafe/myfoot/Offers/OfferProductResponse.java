package com.thegirafe.myfoot.Offers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferProductResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private OfferProductMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OfferProductMessage getMessage() {
        return message;
    }

    public void setMessage(OfferProductMessage message) {
        this.message = message;
    }
}
