package com.thegirafe.myfoot.Wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckWishlistResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private CheckWishlistMessag message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public CheckWishlistMessag getMessage() {
        return message;
    }

    public void setMessage(CheckWishlistMessag message) {
        this.message = message;
    }
}
