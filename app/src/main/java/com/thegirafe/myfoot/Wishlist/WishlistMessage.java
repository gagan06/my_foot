package com.thegirafe.myfoot.Wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishlistMessage {

    @SerializedName("data")
    @Expose
    private WishlistData data;

    public WishlistData getData() {
        return data;
    }

    public void setData(WishlistData data) {
        this.data = data;
    }

    @SerializedName("success")
    @Expose
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
