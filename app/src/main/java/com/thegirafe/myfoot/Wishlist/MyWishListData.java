package com.thegirafe.myfoot.Wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thegirafe.myfoot.Products.ProductGallery;
import com.thegirafe.myfoot.Products.SubProduct;

import java.util.List;

public class MyWishListData {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("featured_image")
    @Expose
    private String featuredImage;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("list_price")
    @Expose
    private String listPrice;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("wishlist")
    @Expose
    private String wishlist;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("cart")
    @Expose
    private String cart;
    @SerializedName("delivery_id")
    @Expose
    private Object deliveryId;
    @SerializedName("sold_id")
    @Expose
    private Object soldId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("product_delivery_detail")
    @Expose
    private String productDeliveryDetail;
    @SerializedName("product_sold_by")
    @Expose
    private String productSoldBy;
    @SerializedName("sub_products")
    @Expose
    private List<SubProduct> subProducts = null;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public Object getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Object deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Object getSoldId() {
        return soldId;
    }

    public void setSoldId(Object soldId) {
        this.soldId = soldId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductDeliveryDetail() {
        return productDeliveryDetail;
    }

    public void setProductDeliveryDetail(String productDeliveryDetail) {
        this.productDeliveryDetail = productDeliveryDetail;
    }

    public String getProductSoldBy() {
        return productSoldBy;
    }

    public void setProductSoldBy(String productSoldBy) {
        this.productSoldBy = productSoldBy;
    }

    public List<SubProduct> getSubProducts() {
        return subProducts;
    }

    public void setSubProducts(List<SubProduct> subProducts) {
        this.subProducts = subProducts;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
