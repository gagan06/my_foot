package com.thegirafe.myfoot.Wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WishlistResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private WishlistMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public WishlistMessage getMessage() {
        return message;
    }

    public void setMessage(WishlistMessage message) {
        this.message = message;
    }

}
