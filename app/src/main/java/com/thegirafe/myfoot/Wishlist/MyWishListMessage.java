package com.thegirafe.myfoot.Wishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyWishListMessage {
    @SerializedName("data")
    @Expose
    private List<MyWishListData> data = null;

    public List<MyWishListData> getData() {
        return data;
    }

    public void setData(List<MyWishListData> data) {
        this.data = data;
    }

}
