package com.thegirafe.myfoot.Filters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterColorData {
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("color_name")
    @Expose
    private String colorName;

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
