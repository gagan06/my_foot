package com.thegirafe.myfoot.Filters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FiltersResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private FiltersMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public FiltersMessage getMessage() {
        return message;
    }

    public void setMessage(FiltersMessage message) {
        this.message = message;


    }
}