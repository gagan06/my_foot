package com.thegirafe.myfoot.Filters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterColorMessage {
    @SerializedName("data")
    @Expose
    private List<FilterColorData> data = null;

    public List<FilterColorData> getData() {
        return data;
    }

    public void setData(List<FilterColorData> data) {
        this.data = data;
    }
}
