package com.thegirafe.myfoot.Filters;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FiltersMessage {

    @SerializedName("data")
    @Expose
    private List<FiltersData> data = null;

    public List<FiltersData> getData() {
        return data;
    }

    public void setData(List<FiltersData> data) {
        this.data = data;
    }



}
