package com.thegirafe.myfoot.Filters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterColorResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private FilterColorMessage message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public FilterColorMessage getMessage() {
        return message;
    }

    public void setMessage(FilterColorMessage message) {
        this.message = message;
    }

}
